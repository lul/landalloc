require(randtoolbox)
source(file.path(r_script_root, "CommonFunctions.R"))






MIN_SHOCK = .3
MAX_SHOCK = .6

cotterellRegions =fread(file.path(plum_data_dir,"cotterellRegions.csv"), header=TRUE)
cotterellRegions[Region %like% 'Africa' & Region != "North Africa", Region := "Sub-Saharan Africa"]
cotterellRegions[Region %like% 'Europe' & Region != "Eastern Europe", Region := "West Central Europe"]

regions = unique(cotterellRegions$Region)

crop_prod = readCropProd()
dt = merge(crop_prod[Year == 2011], cotterellRegions, by="Country")[, list(prod=sum(prod)), by=c("Region", "Item")]
dt[, pc:=100 * prod/sum(prod), by=Item]
breadBaskets = dt[Item %in% c('Wheat', 'Maize', 'Rice paddy') & pc>5][order(prod)]
breadBaskets[, sum(pc), by=Item]

breadBaskets[Item=="Maize", Item:="MaizeMilletSorghum"]
breadBaskets[Item=="Wheat", Item:="WheatBarleyOats"]
breadBaskets[Item=="Rice paddy", Item:="Rice (Paddy Equivalent)"]

#crops = unique(fread(file.path(plum_data_dir,"historicShocks.csv"), header=TRUE)$Sector)


for (scenarioNum in 1:5) {
	shocks = data.table()

	for (year in 2030:2050) {
		numShocks = round (runif(1, 2.5,7.5))
		print (numShocks)
		regionShocks = breadBaskets[sample(1:nrow(breadBaskets), numShocks)][, list(year=year, Region, crop=Item, shockFactor=runif(numShocks, MIN_SHOCK, MAX_SHOCK))]
		print (regionShocks)
		shocks = rbind(shocks, merge(regionShocks, cotterellRegions, by="Region"))
	}
	
	shocks[,mapFilename := paste0(Country,'.asc')]
	
	subDir = file.path("~/Downloads", paste0("s", scenarioNum))
	if (!file.exists(subDir)){
		dir.create(subDir)
	}
	
	write.csv(shocks[, list(year,mapFilename,crop,shockFactor,region=Region)],file.path(subDir, "yieldshocks.csv"), row.names=FALSE, quote=FALSE)
}