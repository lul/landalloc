# TODO: Add comment
# 
# Author: rhenry2
###############################################################################

library(data.table)
library(reshape2)
library(ggplot2)
library(gridExtra)
library(plyr) # manipulation of data formatting
library(dplyr) # maniuplation of data formatting
library(raster)

#frequency ramps up over time with climate change 
#countries have % chance of being shocked, need to check frequency distribution is the same with this methods

#Africa
N_Africa <- c("Algeria", "Egypt", "Libya", "Morocco", "Sudan", "Tunisia")
E_Africa <- c("Burundi", "Comoros", "Djibouti", "Eritrea", "Ethiopia", "Former Ethiopia", "Kenya", "Madagascar", "Malawi", "Mauritius", "Mozambique", "Rwanda", "Seychelles", "Somalia", "South Sudan", "Uganda","Tanzania", "Zambia", "Zimbabwe","United Republic of Tanzania" , "Tanzania, United Rep. of", "Zanzibar","Former Sudan")
M_Africa <- c("Angola", "Cameroon", "Central African Republic", "Chad", "Congo", "Dem. Rep. Congo", "Equatorial Guinea", "Gabon", "Sao Tome and Principe", "St Helena", "Congo, Dem. Rep. of the", "Democratic Republic of the Congo" )
S_Africa <- c("Botswana", "Lesotho", "Namibia", "South Africa", "Eswatini")
W_Africa <- c("Benin", "Burkina Faso", "Cabo Verde", "Cote d'Ivoire","Côte d'Ivoire", "Gambia", "Ghana", "Guinea", "Guinea-Bissau", "Liberia", "Mali", "Mauritania", "Niger", "Nigeria", "Senegal", "Sierra Leone", "Togo")

#Asia

E_Asia <- c("China, mainland", "China, Hong Kong SAR", "China, Macao SAR", "China",  "N. Korea", "Japan", "Mongolia", "Republic of Korea","Democratic People's Republic of Korea", "Korea, Dem. People's Rep", "Northern Mariana Is.")
SE_Asia <- c("Brunei Darussalam", "Cambodia", "Indonesia", "Lao People's Democratic Republic", "Malaysia", "Myanmar", "Philippines", "Singapore", "Thailand", "Timor-Leste", "Viet Nam")
S_Asia <- c("Afghanistan", "Bangladesh", "Bhutan", "India", "Iran (Islamic Republic of)", "Maldives", "Nepal", "Pakistan", "Sri Lanka")
W_Asia <- c("Armenia", "Bahrain", "Cyprus", "Iraq", "Israel", "Jordan", "Kuwait", "Lebanon", "Oman", "Palestine", "Qatar", "Saudi Arabia","Syrian Arab Republic", "United Arab Emirates" , "Yemen","Türkiye")

#Latin America/Caribbean

Caribbean <- c("Antigua and Barbuda", "Anguilla", "Aruba", "Bahamas", "Barbados", "British Virgin Islands",  "Cuba", "Cayman Islands", "Dominica", "Dominican Republic", "Grenada", "Guadeloupe", "Haiti", "Jamaica", "Martinique", "Mayotte", "Montserrat", "Netherlands Antilles","Netherlands Antilles (former)", "Puerto Rico", "Saint Kitts and Nevis", "Saint Lucia", "St. Vin.&Gren.", "Trinidad and Tobago", "Turks and Caicos Is.", "US Virgin Islands", "Saint Vincent and the Grenadines", "Saint Vincent/Grenadines")

C_America <- c("Belize", "Costa Rica", "El Salvador", "Guatemala", "Honduras", "Mexico", "Nicaragua", "Panama")
S_America <- c("Argentina", "Bolivia (Plurinational State of)", "Brazil", "Chile", "Colombia", "Ecuador", "French Guiana", "Guyana", "Paraguay", "Peru", "Suriname", "Uruguay", "Venezuela","Venezuela (Bolivarian Republic of)", "Venezuela, Boliv Rep of", "Falkland Islands (Malvinas)")

#Oceania
Aus_NZ <- c("Australia", "New Zealand")
Melanesia <- c("Fiji",  "New Caledonia", "Papua New Guinea", "Solomon Islands", "Vanuatu", "Fiji, Republic of" )
Micronesia <- c("Guam", "Kiribati", "Marshall Islands", "Micronesia (Federated States of)", "Nauru", "Palau")
Polynesia <- c("American Samoa", "Cook Islands", "French Polynesia", "Niue", "Samoa", "Tokelau", "Tonga", "Tuvalu", "Wallis and Futuna Islands", "Pitcairn Islands")

#North AMerica
N_America <- c("Bermuda", "United States of America", "Canada", "Greenland", "St. Pierre and Miquelon")

#Europe
E_Europe <- c("Bulgaria", "Former Czechoslovakia", "Hungary", "Poland", "Romania", "Ukraine", "Former USSR")
N_Europe <- c("Denmark", "Finland", "Iceland", "Ireland", "Norway", "Sweden", "United Kingdom", "Faroe Islands", "Isle of Man" )
S_Europe <- c("Albania", "Andorra", "Croatia", "Greece", "Italy", "Malta", "Portugal", "Spain",  "Former Yugoslav SFR", "San Marino")
W_Europe <- c("Austria", "Belgium-Luxembourg", "France", "Germany", "Netherlands", "Switzerland","United Kingdom of Great Britain and Northern Ireland" )

# Combine all regions into a named list
regions <- list(
  "North Africa" = N_Africa,
  "East Africa" = E_Africa,
  "Middle Africa" = M_Africa,
  "South Africa" = S_Africa,
  "West Africa" = W_Africa,
  "East Asia" = E_Asia,
  "Southeast Asia" = SE_Asia,
  "South Asia" = S_Asia,
  "West Asia" = W_Asia,
  "Caribbean" = Caribbean,
  "Central America" = C_America,
  "South America" = S_America,
  "Australia/New Zealand" = Aus_NZ,
  "Melanesia" = Melanesia,
  "Micronesia" = Micronesia,
  "Polynesia" = Polynesia,
  "North America" = N_America,
  "East Europe" = E_Europe,
  "North Europe" = N_Europe,
  "South Europe" = S_Europe,
  "West Europe" = W_Europe
)

# Create a data frame with country and corresponding region
country_region_table <- do.call(rbind, lapply(names(regions), function(region) {
  data.frame(
    Country = regions[[region]],
    Region = region,
    stringsAsFactors = FALSE
  )
}))

# View the resulting table
#write.table(data.table(country_region_table),file=file.path(plum_data_dir,"cotterellRegions.csv"),row.names=F)

aggregateStates = function(dt, countriesToExclude){
  #INclude Soviet, Yugoslav, Sudanese as former states
  dt[Country %in% c("Armenia", "Azerbaijan", "Belarus", "Estonia", "Georgia", "Kazakhstan", "Kyrgyzstan", "Latvia", 
                    "Lithuania", "Republic of Moldova", "Russian Federation", "Tajikistan", "Turkmenistan", "Ukraine", 
                    "Uzbekistan","USSR","Russian Fed","Moldova, Republic of","Un. Sov. Soc. Rep.","Macedonia, Fmr Yug Rp of"),
     Country := "Former USSR"]
  dt[Country %in% c("Bosnia and Herzegovina", "Slovenia", "Croatia", "Serbia","Montenegro",
                    "The former Yugoslav Republic of Macedonia","Yugoslav SFR","Serbia and Montenegro","Bosnia Herzg",
                    "Yugoslavia","Macedonia, Fmr Yug Rp of","Yugoslavia SFR"),
     Country := "Former Yugoslav SFR"]
  dt[Country %in% c("Czech Republic", "Slovakia","Czechoslovakia","Czechia","North Macedonia"),
     Country := "Former Czechoslovakia"]
  dt[Country %in% c('Sudan', 'South Sudan', 'Somalia', "Sudan (former)"),
     Country := "Former Sudan"]
  dt[Country %in% c("Ethiopia", "Eritrea","Ethiopia PDR"),
     Country := "Former Ethiopia"]
  dt[Country %in% c("Belgium","Luxembourg"),
     Country := "Belgium-Luxembourg"]
  dt[Country %in% c("China","China Taiwan Province of"),
     Country := "China"]
  
  
  #remove partial data stes 
  dt = dt[!Country %in% countriesToExclude]
  
  dt1 = dt[,list(prod = sum(prod), import=sum(import),export=sum(export), food=sum(food), feed=sum(feed)), by=c('Country','Year','plumCropItem')]
  
  dt1
}

production =fread(file.path("C:\\Users\\rosly\\eclipse-workspace\\plumv2\\data","production_1961-2021.csv"), header=TRUE)
production = aggregateStates(production, c('Africa','Americas'))

# Summing the `prod` column by `Country` and `Year`, split our livestock and crops 
# Define crop and livestock categories
# crops <- c("FruitVeg", "MaizeMilletSorghum", "Oilcrops N-fixing", "Oilcrops Other", 
#            "Pulses", "Rice (Paddy Equivalent)", "Starchy Roots", "Sugar", "WheatBarleyOats")
# livestock <- c("Monogastrics", "Ruminants")

# # Aggregate data
# summed_production <- production[, .(
#   CropProd = sum(prod[plumCropItem %in% crops], na.rm = TRUE),
#   LivestockProd = sum(prod[plumCropItem %in% livestock], na.rm = TRUE)
# ), by = .(Country, Year)]


# Extract unique countries from the production dataset
production_countries <- unique(production$Country)

# Filter the country-region table to only include countries present in the production dataset
filtered_country_region_table <- country_region_table %>%
  filter(Country %in% production_countries)


# Function to find the region
find_region <- function(country) {
  region <- names(regions)[sapply(regions, function(x) country %in% x)]
  if (length(region) == 0) {
    return(NA)  # Return NA if no region matches
  } else {
    return(region[1])  # Return the first matching region
  }
}



# Add the Region column to production
production <- production %>%
  mutate(Region = sapply(Country, find_region))

# Filter rows where Region is NA
#missing_regions <- production %>% filter(is.na(Region))
#unique(missing_regions$Country)


# Generate a complete sequence of years for each Country and plumCropItem
complete_years <- production %>%
  group_by(Country, plumCropItem) %>%
  summarise(ExpectedYears = list(1961:2021), .groups = "drop") %>%
  tidyr::unnest(ExpectedYears) %>%
  rename(Year = ExpectedYears)

# Find missing years by performing an anti-join with the production dataset
missing_years <- complete_years %>%
  anti_join(production, by = c("Country", "plumCropItem", "Year"))

# Identify incomplete combinations (those with missing years)
incomplete_combinations <- missing_years %>%
  distinct(Country, plumCropItem)

# Exclude incomplete combinations from the original dataset
filtered_production <- production %>%
  anti_join(incomplete_combinations, by = c("Country", "plumCropItem"))

# # Generate a complete sequence of years for each Country
# complete_years <- summed_production %>%
#   group_by(Country) %>%
#   summarise(ExpectedYears = list(1961:2021), .groups = "drop") %>%
#   tidyr::unnest(ExpectedYears) %>%
#   rename(Year = ExpectedYears)
# 
# # Find missing years by performing an anti-join with the production dataset
# missing_years <- complete_years %>%
#   anti_join(summed_production, by = c("Country", "Year"))
# 
# # Identify incomplete combinations (those with missing years)
# incomplete_combinations <- missing_years %>%
#   distinct(Country)
# 
# # Exclude incomplete combinations from the original dataset
# filtered_production <- summed_production %>%
#   anti_join(incomplete_combinations, by = c("Country"))



# Merge the filtered_production with countryWregions to add the WBRegion column

countryWregions = fread(file.path(plum_data_dir,"countriesWRegions.csv"))
filtered_production <- filtered_production %>%
  left_join(countryWregions %>%
              select(Area, Region), 
            by = c("Country" = "Area")) %>%
  rename(WBRegion = Region.y)

# Filter the rows where WBRegion is NA
countries_with_na_WBRegion <- filtered_production %>%
  filter(is.na(WBRegion)) %>%
  select(Country)

# View the countries with missing WBRegion
#unique(countries_with_na_WBRegion)

#unique(filtered_production$WBRegion)


#CROP SHOCKS
filtered_production[Country=='Former USSR', WBRegion := "Europe & Central Asia"]
filtered_production[Country=='Belgium-Luxembourg', WBRegion := "Europe & Central Asia"]
filtered_production[Country=='Former Yugoslav SFR', WBRegion := "Europe & Central Asia"]
filtered_production[Country=='Former Czechoslovakia', WBRegion := "Europe & Central Asia"]
filtered_production[Country=='United Kingdom of Great Britain and Northern Ireland', WBRegion := "Europe & Central Asia"]
filtered_production[Country=='Türkiye', WBRegion := "Europe & Central Asia"]
filtered_production[Country=='Antigua and Barbuda', WBRegion := "Latin America & Caribbean"]
filtered_production[Country=='Saint Kitts and Nevis', WBRegion := "Latin America & Caribbean"]
filtered_production[Country=='Dominica', WBRegion := "Latin America & Caribbean"]
filtered_production[Country=='Kiribati', WBRegion := "East Asia & Pacific"]
filtered_production[Country=='Former Sudan', WBRegion := "Middle East & North Africa"]
filtered_production[Country=='Former Ethiopia', WBRegion := "Middle East & North Africa"]
filtered_production[Country=="Côte d'Ivoire", WBRegion := "Middle East & North Africa"]

# Reshape and rename sector values
# filtered_production <- data.table(filtered_production %>%
#   pivot_longer(cols = c(CropProd, LivestockProd), 
#                names_to = "sector", 
#                values_to = "prod") %>%
#   mutate(sector = ifelse(sector == "CropProd", "crops", "livestock")))

filtered_production
# Rename the column
setnames(filtered_production, old = "plumCropItem", new = "sector")

# Count the number of unique entries by Region and Sector
unique_country_count <- filtered_production %>%
  group_by(WBRegion, sector) %>%

  rename(
    Region = WBRegion,
    Sector = sector
  )  %>%
  summarise(UniqueEntries = n_distinct(paste(Country,Year)), .groups = "drop")



# Count the number of unique countries per sector
sector_country_count <- filtered_production[, .(UniqueCountries = uniqueN(Country)), by = .(sector,WBRegion)]



calculatePlumShocks = function (dt,sectorP,noPreviousYears, Span, cooksDistance,FUN){
	shocks=NULL
	
		for (country in unique(dt$Country)){
			
			countryX=dt[Country == country & sector==sectorP]
			if(nrow(countryX)>0){
				fit=loess(countryX$prod  ~ countryX$Year, span=Span)
				#regress residuals against lag-1 residuals
				errors=lm(resid(fit)[2:length(countryX[,Year])]~ resid(fit)[1:(length(countryX[,Year])-1)])
				#Identify the Shock
				shockInd = which(as.vector(cooks.distance(errors)>cooksDistance))
				shockInd=shockInd[shockInd>noPreviousYears]
				
				if (length(shockInd)>0){
					for (shock in shockInd){
						year=countryX[shock+1,Year]
						fiveyearmean = FUN(countryX[Year >=(year-noPreviousYears) & Year <year,prod])
						AbsMagnitude = fiveyearmean-countryX[Year==year,prod]
						PropMagnitude = 1-(countryX[Year==year,prod]/fiveyearmean)
						RecoveryYear=countryX[Year >= year+1 & prod >=(fiveyearmean*0.9),Year][1]
						data=data.table(sectorP, Country=country, Region=countryX[,unique(WBRegion)], Year=year, PropMagnitude, 
								AbsMagnitude, RecoveryTime=RecoveryYear-year, Span,noPreviousYears,funct=as.character(substitute(FUN)),cooksDistance)
						shocks=rbind(shocks, data)
					}
				}
				
			}
		}
	shocks[AbsMagnitude >0]
}

calculateAllSectorShocks = function (dt,noPreviousYears, Span, cooksDistance,FUN){
	shocks=NULL
	
	for(sector in unique(dt$sector)){
		sectorTab = calculatePlumShocks(dt,sector, noPreviousYears, Span,cooksDistance, FUN)
		shocks=rbind(shocks,sectorTab)
	}
	shocks
}

#calculate historic shocks by sector 
##frequency is regional as regions differ in frequency of shocks, using Cotterells regions rather than WB regions
##magnitudes are world per crop
historicShocks = calculateAllSectorShocks(dt=filtered_production,noPreviousYears=6, Span=0.72,cooksDistance=0.3, FUN=mean) 
write.table(historicShocks[,list(Country,Region,Year,Sector=sectorP,PropMagnitude,AbsMagnitude)], file.path(plum_data_dir,"historicShocks.csv"), row.names=FALSE)


##CHECKS

#setwd("C:\\Users\\rhenry2\\Desktop\\RUGS\\Climate_shocks\\checks")

plotShockFits = function(dt, Span,cooksThreshold){
  
  
  for(sector in unique(dt$sector)){
    
    tiff(file=file.path("C:\\Users\\rosly\\Desktop\\forFinn\\shocks","LOESS Shock Analysis - ",sector,".tif"))
    for (country in unique(dt$Country)){
      op<-par(mfrow=c(3, 1))
      
      #Production plot with model fit
      countryX=dt[Country == country & sector==sector]
      if(nrow(countryX)>0){
        plot(countryX$Year, countryX$prod, xlab="Year", ylab=paste0("Total ",sector," Production (tonnes)"), main=country, pch=20, cex=0.75)
        fit<-loess(countryX$prod ~ countryX$Year, span=Span)
        lines(countryX$Year, predict(fit))
        
        #regress residuals against lag-1 residuals
        if(nrow(countryX[prod > 0])){
          errors<-lm(resid(fit)[2:length(countryX[,Year])]~ resid(fit)[1:(length(countryX[,Year])-1)])
          plot(resid(fit)[2:length(countryX[,Year])], resid(fit)[1:(length(countryX[,Year])-1)], pch=20, main="", xlab=expression(bold("Residuals")[(italic ("t"))]),ylab=expression(bold("Residuals")[(italic("t -1"))]))
          abline(errors)
          
          #plot cooks distance and identify outliers in residual regression
          plot(cooks.distance(errors), type="o", pch=20, ylab="Cook's Distance", xlab="Index")
          abline(h=cooksThreshold, lty=2, col="red", lwd=2)
        }
      }
      par(op)	
    }
    dev.off()
  }
}

plotShockFits(filtered_production, Span=1.0, cooksThreshold=0.35)

#checking cooks distance threshold. looking for close to asymptote point. Check duration and span. Can also check function used ie mean vs median 

plotCooksDistanceSensitivity = function(){
	
	
	plotSector = function(dtMaster,sector, cookDistances,spans, durations, FUN){
		sensitivityList = NULL
		dt = dtMaster[sector==sector]
		
		for(span in spans){
			for(duration in durations){
				for (cooksD in cookDistances){
					shocksList = calculatePlumShocks(dt,sector,duration, span, cooksD, FUN)
					sensitivityList=rbind(sensitivityList, data.table(sector,cooksD,shocks=nrow(shocksList),span,duration,funct=as.character(substitute(FUN))))
				}
			}
		}
		sensitivityList
		
		ggplot(data=sensitivityList, aes(x=cooksD, y=shocks))+geom_line(aes(colour=factor(span), linetype=factor(duration)), size=1)+
				labs(x="Cook's Distance threshold", y="No. of shocks detected", title = sector, colour="")+
				theme_classic()+
				theme(panel.grid = element_blank(),
						plot.title = element_text(hjust = 0.5, vjust = -0.75, face="bold"))+
				scale_y_continuous(limits=c(0,250))+
				geom_vline(xintercept = 0.35, lty=2, colour="red")
	}
	
	# wheatPlot = plotSector(dtMaster=filtered_production, sector='WheatBarleyOats', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	# 		durations=c(6,2), FUN=mean)
	# maizePlot = plotSector(dtMaster=filtered_production, sector='MaizeMilletSorghum', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	# 		durations=c(6,2), FUN=mean)
	# ricePlot = plotSector(dtMaster=filtered_production, sector='Rice (Paddy Equivalent)', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	# 		durations=c(6,2), FUN=mean)
	# fruitVegPlot = plotSector(dtMaster=filtered_production, sector='FruitVeg', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	# 		durations=c(6,2), FUN=mean)
	# oilcropsOtherPlot = plotSector(dtMaster=filtered_production, sector='Oilcrops Other', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	# 		durations=c(6,2), FUN=mean)
	# oilcropsNFixingPlot = plotSector(dtMaster=filtered_production, sector='Oilcrops N-fixing', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	#     durations=c(6,2), FUN=mean)
	# pulsesPlot = plotSector(dtMaster=filtered_production, sector='Pulses', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	# 		durations=c(6,2), FUN=mean)
	# sugarPlot = plotSector(dtMaster=filtered_production, sector='Sugar', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	# 		durations=c(6,2), FUN=mean)
	# starchyRootsPlot = plotSector(dtMaster=filtered_production, sector='Starchy Roots', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	# 		durations=c(6,2), FUN=mean)
	# ruminantsPlot = plotSector(dtMaster=filtered_production, sector='Ruminants', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	#     durations=c(6,2), FUN=mean)
	# monigastricsPlot = plotSector(dtMaster=filtered_production, sector='Monogastrics', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	#     durations=c(6,2), FUN=mean)
	
	cropsPlot = plotSector(dtMaster=filtered_production, sector='crops', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	                       durations=c(6,2), FUN=mean)
	livestockPlot = plotSector(dtMaster=filtered_production, sector='livestock', cookDistances=seq(0.05,1, by=0.05),spans=c(0.6,0.7), 
	                       durations=c(6,2), FUN=mean)
	
	# pdf(width=20, height=32,file =file.path("C:\\Users\\rosly\\Desktop\\forFinn\\shocks","cooksDistanceMean.pdf"))
	# grid.arrange(wheatPlot,maizePlot,ricePlot,fruitVegPlot, oilcropsOtherPlot, oilcropsNFixingPlot, pulsesPlot,sugarPlot, starchyRootsPlot, ruminantsPlot,monigastricsPlot, ncol=2)
	# dev.off()
	
	pdf(width=30, height=20,file =file.path("C:\\Users\\rosly\\Desktop\\forFinn\\shocks","cooksDistanceMeanSummedProd.pdf"))
	grid.arrange(cropsPlot,livestockPlot, ncol=2)
	dev.off()

	print('Cooks distance plot generated')
}

plotCooksDistanceSensitivity()


#check range of shock frequencies and for analysis choose the combination
#that minimised the sum of squared residuals with the median of this range through time.
#Parameters checked = 

#- mean or median time span for shock to drop from
#- duration of previous average (3,5,7,9 years)
#- LOESS span (0, 0.25, 0.33, 0.5, 0.66, 0.75, 1, 1.5, 2)

sensitivityAnalysis = function(dtMaster, cookDistance,spans, durations){
		sensitivityList = NULL
		
		for(sector in unique(dtMaster[,sector])){
			dt = dtMaster[sector==sector]
			
			for(span in spans){
				for(duration in durations){
					shocksListMedian = calculatePlumShocks(dt, sector,duration, span, cookDistance, FUN=median)
					shocksListMean = calculatePlumShocks(dt, sector,duration, span, cookDistance, FUN=mean)
					sensitivityList=rbind(sensitivityList, shocksListMedian,shocksListMean)
				}
			}
			
		}
		
		sensitivityList
	}

sensTab = sensitivityAnalysis(dtMaster=filtered_production, cookDistance=0.35,spans=c(0.25, 0.33, 0.5, 0.66, 0.75, 1, 1.5, 2), 
			durations=c(2,4,6,7,8))

sensTab = sensitivityAnalysis(dtMaster=filtered_production, cookDistance=0.3,spans=c(0.7,0.72, 0.75), 
                             durations=c(6,7))


sensTab = sensitivityAnalysis(dtMaster=filtered_production, cookDistance=0.35,spans=c(0.9, 1, 1.1), 
                              durations=c(2,4,6,7,8))

sensTab2 <-data.table(table(sensTab[,list(Year,noPreviousYears,funct,Span)]))



#median,minimum and maximum of all ensembles

sensTabRange = sensTab2[,list(Min=min(N), Max=max(N), Median=median(N)), by=c('Year')]

#which model minimises the Sum of squares

SSResids <- function(x){
	model <- lm(x~sensTabRange[,Median])
	SSR <-sum(residuals(model)^2)
	return(SSR)
}

modelChoice = sensTab2[,list(SSR=SSResids(N)), by=c('noPreviousYears','funct','Span')]
modelChoice[order(SSR)][1] #parameter values to use 

#> modelChoice[order(SSR)][1] #parameter values to use 
# noPreviousYears funct Span      SSR
# 1:               6  mean    1 2.860395

sensTabRange = merge(sensTabRange,sensTab2[noPreviousYears==6 & funct=='mean' & Span==0.72, list(Year,Actual=N)],by='Year') 

#all sectors model selection plot - FREQUENCY 
ggplot(data=sensTabRange, aes(x=Year, y=Median, group = 1))+
		geom_ribbon(aes(ymin=Min, ymax=Max), fill="grey")+
		geom_line(size=0.5)+
		geom_line(aes(y=Actual), colour="Red")+
		theme_bw()+
		theme(panel.background = element_blank(),
				panel.grid = element_blank())+
		labs(x=bquote(bold(Year)), y=bquote(bold(Shock~frequency)))


# 10-Year Event:
#   1°C: ~2 times more frequent
# 1.5°C: ~3 times more frequent
# 2°C: ~4 times more frequent
# 3°C: ~7 times more frequent
# 4°C: ~10 times more frequent
# 50-Year Event:
#   1°C: ~3 times more frequent
# 1.5°C: ~5 times more frequent
# 2°C: ~10 times more frequent
# 3°C: ~20 times more frequent
# 4°C: ~35 times more frequent

##Cotterell proportion of shocks that are weather/climate related

h = fread(file.path("C:\\Users\\rosly\\Desktop\\forFinn\\shocks","41893_2018_210_MOESM2_ESM.csv"))
unique(h[,Category])
h = h[Sector %in% c("Crops","Livestock"),list(Sector,Country,Year,Category,Region)]

# Calculate proportions
proportion_shocks <- h %>%
  group_by(Sector, Region) %>%
  summarise(
    TotalShocks = n(),
    ClimateWeatherShocks = sum(Category %in% c("Climate/weather events","Climate/weather events & mismanagement","Climate/weather & geopolitical/economic events")),
    ProportionClimateWeather = ClimateWeatherShocks / TotalShocks,
    .groups = "drop"
  )

# Summarise the average proportion of Climate/Weather events per sector
average_proportion_climate_weather <- proportion_shocks %>%
  group_by(Sector) %>%
  summarise(
    AvgProportionClimateWeather = mean(ProportionClimateWeather, na.rm = TRUE),
    .groups = "drop"
  )
#  Sector    AvgProportionClimateWeather
#<chr>                           <dbl>
#  1 Crops                           0.522
#2 Livestock                       0.230

# Calculate the number of shocks per year for each sector
sector_shock_trend <- h %>%
  group_by(Sector, Year) %>%
  summarise(
    ShockFrequency = n(),
    .groups = "drop"
  ) %>%
  arrange(Sector, Year) %>%
  group_by(Sector) %>%
  mutate(
    RateOfChange = (ShockFrequency - lag(ShockFrequency)) / lag(ShockFrequency)
  ) %>%
  ungroup()

# View the resulting data
print(sector_shock_trend)



# Calculate the number of shocks per year for each sector
sector_shock_trend <- h[Category %in% c("Climate/weather events","Climate/weather events & mismanagement","Climate/weather & geopolitical/economic events")] %>%
  group_by(Sector, Year) %>%
  summarise(ShockFrequency = n(), .groups = "drop") %>%
  arrange(Sector, Year)

# Plot the number of shocks per year by sector
ggplot(sector_shock_trend, aes(x = Year, y = ShockFrequency, color = Sector)) +
  geom_line(size = 1) +
  geom_point(size = 2) +
  labs(
    title = "Number of Shocks Per Year by Sector",
    x = "Year",
    y = "Shock Frequency",
    color = "Sector"
  ) +
  theme_minimal() +
  theme(
    plot.title = element_text(hjust = 0.5, size = 14, face = "bold"),
    axis.title = element_text(size = 12),
    legend.position = "bottom"
  )

##Cotterell doesn't really show increase in frequency over time of weather/climate shocks (despite paper text saying it does) 


##checking distributions of future shocks are similar to historic 
newShocks=fread(file.path("C:\\Users\\rhenry2\\Desktop","yieldshocks.csv")) ##change to file of interest
historicShocks=fread(file.path(plum_data_dir,'historicShocks.csv'))
histFreq=data.table(table(historicShocks[,list(Region,Sector)]))
newFreq=data.table(table(data.table(shock_data)[,list(Region,Sector)]))
comp=merge(histFreq[,list(Region,Sector, histFreq=N)],newFreq[,list(Region,Sector, newFreq=N)], by=c('Region','Sector'))

##want below to all be above 0.05 so values are drawn from the same distribution aka the distributions between the historic shocks and new shocks are the same
results=NULL
for(sector in unique(data.table(shock_data)[,Sector])){
  
  print(sector)
  row= data.table(sector,magnitude=ks.test(historicShocks[Sector==sector, PropMagnitude],data.table(shock_data)[Sector==sector, Magnitude])$p.value,
                  frequency=ks.test(comp[Sector==sector, jitter(histFreq)],comp[Sector==sector, jitter(newFreq)])$p.value)
  results=rbind(results,row)
}


