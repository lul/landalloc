require(data.table)
require(raster)

historicShocks=fread(file.path(plum_data_dir,"historicShocks.csv")) ##generated from productionShocks.R

##jUSTIFY frequency increase, justify magnitude increase (developed vs undeveloped world)

##magnitude increases by 10 % points under 8.5, 3-11% to 10-20%, https://www.nature.com/articles/s41467-023-36129-4
##frequency of extreme precipitation/droughts increases according to IPCC chapter, https://www.ipcc.ch/report/ar6/wg1/chapter/chapter-11/, 
## use 10-year event as more in keeping with historic frequencies and more conservative increase
# 10-Year Event:
#   1°C: ~2 times more frequent
# 1.5°C: ~3 times more frequent
# 2°C: ~4 times more frequent
# 3°C: ~7 times more frequent
# 4°C: ~10 times more frequent
# 50-Year Event:
#   1°C: ~3 times more frequent
# 1.5°C: ~5 times more frequent
# 2°C: ~10 times more frequent
# 3°C: ~20 times more frequent
# 4°C: ~35 times more frequent

##warming associated with each rcp by 2080-2100
# 2.6	2.6 W/m 2	~2.0 °C
# 4.5	4.5 W/m 2	~2.4 °C
# 6.0	6.0 W/m 2	~2.8 °C
# 8.5	8.5 W/m 2	~4.3 °C


# Create the data.table
climate_data <- data.table(
  rcp = c(2.6, 4.5, 6.0, 7.0, 8.5),
  freqMultiplier = c(2, 4, 7, 8, 10),
  magnitudeIncrease = c(0.01, 0.025, 0.05, 0.075, 0.1)
)

# Function to create time series for each row
create_time_series <- function(start, end, adjusted, endpoint) {
  years <- 2020:2100
  # Linearly increase from adjusted to endpoint (2020 to 2080)
  increasing <- seq(from = adjusted, to = endpoint, length.out = 2080 - 2020 + 1)
  # Constant value from 2081 to 2100
  constant <- rep(endpoint, length(2099:2080 + 1))
  # Combine the two series
  shockfreq <- c(increasing, constant)
  return(data.table(Year = years, ShockFreq = shockfreq))
}

generateShocksTable = function(dt, startYear, endYear, clim){
  
  ampli = climate_data[rcp == clim]
  
  dt[,endPoint := AdjustedShockCount* ampli$freqMultiplier]
  
  # Expand dataset
  expanded_data <- dt[, create_time_series(2020, 2100, AdjustedShockCount, endPoint),
                      by = .(Region, Sector)]
  
  expanded_datab <- dt[,create_time_series(2020, 2100, 0, ampli$magnitudeIncrease),
                       by = .(Region, Sector)]
  
  merged_data <- merge(expanded_data,
    expanded_datab[, .(Region, Sector, Year, magnitudeAddition = ShockFreq)],
    by = c("Region", "Sector", "Year")
  )
  
  # Create an empty data frame to store the results
  shock_data <- data.frame(Country = character(),Sector= integer(), Year = integer(), Shock = integer())
  
  for(cropType in unique(dt$Sector)){
    #magnitudes of shocks take from global distribution per sector at a country level, median recovery time is 1 year
    sectorHist = hist(historicShocks[Sector==cropType,PropMagnitude],freq=FALSE)
    
    for(country in unique(countryWregions$PlumGroup)){	
      
      region = countryWregions[PlumGroup == country, unique(Region)]
      ###TODO, missing regions, fill with global average 
      
      for (year in startYear:endYear) {
        
        shock_probability =   
          merged_data %>%
          filter(Sector == cropType, Region == region, Year==year) %>% 
          select(ShockFreq)  %>%
          pull() 
        
        shock_mag_ad =   
          merged_data %>%
          filter(Sector == cropType, Region == region, Year==year) %>% 
          select(magnitudeAddition)  %>%
          pull() 
        
        
        shock_occurred <- rbinom(1, 1, shock_probability)  # Generate a shock (1) or no shock (0)
        
        if(shock_occurred == 1){
          bins=with(sectorHist,sample(length(mids),1,p=density,replace=TRUE))
          shockMagnitude=runif(length(bins),sectorHist$breaks[bins],sectorHist$breaks[bins+1]) + shock_mag_ad
          shock_data <- rbind(shock_data, data.frame(Country = country,Region=region, Sector=cropType, Year = year, Magnitude = shockMagnitude))
          print(paste(cropType,country,year, shock_probability))
        }
      }
      
    }
    
  }
  
  shock_data
}

# Count the number of shocks by sector and region
shock_counts <- historicShocks %>%
  group_by(Sector, Region) %>%
  summarise(ShockCount = n(), .groups = 'drop')

unique_country_count #from productionShocks.R

# Merge the shock_counts data with sector_country_count by WBRegion and sector
adjusted_shock_counts <- shock_counts %>%
  left_join(unique_country_count, by = c("Region", "Sector")) %>%
  mutate(AdjustedShockCount = ShockCount / UniqueEntries) %>%
  select(Region, Sector, AdjustedShockCount)


# Adjust based on Cotterell proportions attributable to weather
# chance of shock in a given year for each region and sector
adjusted_shock_counts <- adjusted_shock_counts %>%
  mutate(AdjustedShockCount = case_when(
    Sector %in% c("Ruminants", "Monogastrics") ~ (AdjustedShockCount * 0.23),
    TRUE ~ (AdjustedShockCount * 0.52)
  ))

##some regions and sectors are missing, no shocks, mostly North America, use sector averages

# Convert to data.table if not already
setDT(adjusted_shock_counts)

# Create a full grid of all Sector and Region combinations
all_combinations <- CJ(Sector = unique(adjusted_shock_counts$Sector), Region = unique(adjusted_shock_counts$Region))

# Identify missing combinations
missing_combinations <- all_combinations[!adjusted_shock_counts, on = .(Sector, Region)]

# Calculate the average AdjustedShockCount for each Sector
sector_averages <- adjusted_shock_counts[, .(AvgAdjustedShockCount = mean(AdjustedShockCount, na.rm = TRUE)), by = Sector]

# Add the missing combinations with the sector average
missing_combinations <- merge(missing_combinations, sector_averages, by = "Sector", all.x = TRUE)
missing_combinations[, AdjustedShockCount := AvgAdjustedShockCount]
missing_combinations[, AvgAdjustedShockCount := NULL]  # Clean up temporary column

# Combine original data with the missing rows
adjusted_shock_counts <- rbind(adjusted_shock_counts, missing_combinations, fill = TRUE)

rcp26Shocks = generateShocksTable(adjusted_shock_counts,2020,2100,2.6)
rcp45Shocks = generateShocksTable(adjusted_shock_counts,2020,2100,4.5)
rcp60Shocks = generateShocksTable(adjusted_shock_counts,2020,2100,6.0)
rcp70Shocks = generateShocksTable(adjusted_shock_counts,2020,2100,7.0)
rcp85Shocks = generateShocksTable(adjusted_shock_counts,2020,2100,8.5)

write.csv(data.table(rcp26Shocks)[,list(year=Year,mapFilename=Country, crop=Sector, shockFactor=Magnitude)], file.path(plum_data_dir,"rcp26Shocks.csv"), row.names=FALSE,quote=FALSE)
write.csv(data.table(rcp45Shocks)[,list(year=Year,mapFilename=Country, crop=Sector, shockFactor=Magnitude)], file.path(plum_data_dir,"rcp45Shocks.csv"), row.names=FALSE,quote=FALSE)
write.csv(data.table(rcp60Shocks)[,list(year=Year,mapFilename=Country, crop=Sector, shockFactor=Magnitude)], file.path(plum_data_dir,"rcp60Shocks.csv"), row.names=FALSE,quote=FALSE)
write.csv(data.table(rcp70Shocks)[,list(year=Year,mapFilename=Country, crop=Sector, shockFactor=Magnitude)], file.path(plum_data_dir,"rcp70Shocks.csv"), row.names=FALSE,quote=FALSE)
write.csv(data.table(rcp85Shocks)[,list(year=Year,mapFilename=Country, crop=Sector, shockFactor=Magnitude)], file.path(plum_data_dir,"rcp85Shocks.csv"), row.names=FALSE,quote=FALSE)


##checks

# count shocks per year, should increase in frequency to 2080 and level off 
shocks_per_year <- rcp85Shocks %>%
  group_by(Year) %>%
  summarise(num_shocks = n())

# Plot
ggplot(shocks_per_year, aes(x = Year, y = num_shocks)) +
  geom_line(color = "blue") +
  labs(title = "Frequency of Shocks Over Time",
       x = "Year",
       y = "Number of Shocks") +
  theme_minimal()

## average magnitude per year, should similarly increase to 2080 and level off 

average_magnitude <- rcp85Shocks %>%
  group_by(Year) %>%
  summarise(AverageMagnitude = mean(Magnitude, na.rm = TRUE))

# Plot the data
ggplot(average_magnitude, aes(x = Year, y = AverageMagnitude)) +
  geom_line(color = "blue", size = 1) +
  geom_point(color = "red", size = 2) +
  labs(title = "Change in Average Magnitude Through Time",
       x = "Year",
       y = "Average Magnitude") +
  theme_minimal()




##perhaps better to just keep a list of country maps rather than make them each time in this script
# makeMaps=function(countriesTab, outputDir){
#   
#   country_codes = fread(file.path(baseDataDir, "country_codes4.csv"), header=TRUE)
#   boundary = raster(file.path(baseDataDir, "halfdeg", "country_boundaries.asc"))
#   crs(boundary) = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs'
#   
#   for(mapFilename in unique(countriesTab[,mapFilename])){
#     b2 = boundary %in% country_codes[Country %in% mapFilename, numCode]
#     values(b2)[values(b2) == 0] = NA
#     fileN = filename=file.path(baseDataDir, "halfdeg","yieldshockmaps",paste0(mapFilename,'.asc'))
#     if (!file.exists(fileN)){
#       writeRaster(b2, filename=fileN, format='ascii', bylayer=TRUE, overwrite=TRUE)
#     }
#     
#   }
# }







