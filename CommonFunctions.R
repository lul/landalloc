require(data.table)

fao_data_dir = file.path(r_script_root, "InputData", "FAOStat-April2019")

getItemDetails = function() {
	itemDetails = fread(file.path(r_script_root, "itemsInputs.csv"))
	itemDetails[, allowedForVeggie:=TRUE]
	itemDetails[animal == TRUE & !Item %in% c("Eggs", "Milk - Excluding Butter"), allowedForVeggie:=FALSE]
	itemDetails
}

########## getFeedConvRatio
getFeedConvRatio  = function(generateRandom=FALSE, lowerB=-0.2, upperB=0.2) {
	fcrDt = data.table(
			productionItem=c("Milk Total + (Total)", "Eggs Primary + (Total)", "Meat Poultry + (Total)", "Beef and Buffalo Meat + (Total)", "Sheep and Goat Meat + (Total)", "Meat pig", "Meat Other", "Animal fats"),
			Item=c("Milk - Excluding Butter", "Eggs", "Poultry Meat", "Bovine Meat", "Mutton & Goat Meat", "Pigmeat", "Meat Other", "Animal fats"),
			fcr=c(0.7, 2.3, 3.3, 25, 15, 6.4, 15, 9.5),
			justFeed = c(FALSE, TRUE, TRUE, FALSE, FALSE, TRUE, FALSE, FALSE)) # animal fats from weighted average world consumption in 2010 of meats not including poultry meat
	
	if (generateRandom) {
		fcrDt[, fcr := fcr * (1 + runif(8, min=lowerB, max=upperB))]
	}
	fcrDt
}

getCountryCodes = function(includeWorld=TRUE) {
	dt = fread(file.path(plum_data_dir, "country_codes4.csv"), header=TRUE)[, list(Country, CountryCode, Region, IncomeGroup)]
	if (includeWorld == TRUE) dt = rbind(dt, data.table(Country = "World", CountryCode = "WLD", Region = "World", IncomeGroup="variable"))
	dt
}

##### Plot helper functions
rasterFromFile = function(lcType, fileName, offset=0.0) {
	plumDt=data.table(read.table(fileName, header=TRUE))
	plumDt[, c('Lon', 'Lat') := list(Lon-offset, Lat-offset)]
	rasterFromXYZ(plumDt[, list(Lon, Lat, get(lcType))], crs = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
}

########## Country data
readCountryData = function() {
	########## Land
	area_data = fread(file.path(fao_data_dir, "Inputs_Land_E_All_Data_Norm.csv"))
	
	if ("Country Code" %in% names(area_data)) {
		setnames(area_data, "Country Code", "FaoCountryCode")
	} else {
		setnames(area_data, c("Area Code", "Area"), c("FaoCountryCode", "Country"))
	}
	
	if (grepl("April2019", fao_data_dir)) {
		data_req = data.table(faoName = c("Land area equipped for irrigation", "Land area", "Agricultural land", "Arable land", "Forest land", "Land under permanent crops", "Land under perm. meadows and pastures", "Land under temporary crops", "Land under temp. meadows and pastures", "Other land"),
				ourColName = c("irrig", "land", "agri", "arable", "forest", "perm_crops", "perm_pasture", "temp_crops", "temp_pasture", "other"))
	} else { 
		data_req = data.table(faoName = c("Total area equipped for irrigation", "Land area", "Agricultural area", "Arable land", "Forest", "Permanent crops", "Permanent meadows and pastures", "Temporary crops", "Temporary meadows and pastures", "Other land"),
			ourColName = c("irrig", "land", "agri", "arable", "forest", "perm_crops", "perm_pasture", "temp_crops", "temp_pasture", "other"))
	}
	
	area_data[, Value := as.numeric(Value) / 1000]
	area_data[, Year := as.numeric(Year)]
	
	cd = data.table(dcast(area_data[Element == "Area" & Item %in% data_req$faoName], Country + Year + FaoCountryCode ~ Item, value.var = 'Value'))
	cd[is.na(cd)] = 0
	setnames(cd, data_req$faoName, data_req$ourColName)
	cd[Country == "Eswatini", Country:="Swaziland"] # some FAO dataset contain old name of Swaziland, and some new name of Eswatini
	
	########## Historical Population & GDP	
	wb_data = fread(file.path(data_dir_root, "WorldBank","wb_data3Mar17.csv"), header=TRUE, na.strings=c('..'))
	setnames(wb_data, gsub(' (\\[.*\\])*', '', (names(wb_data))))
	wb_data[CountryName=="Romania"]$CountryCode = "ROU"
	wb_data[CountryName=="South Sudan"]$CountryCode = "SSUF"
	wb_data[CountryName=="Congo, Dem. Rep."]$CountryCode = "COD"
	wb_melt = melt.data.table(wb_data, id.vars = c("CountryName", "CountryCode", "SeriesName", "SeriesCode"), variable.name = "Year", value.name = "Value")
	
	gdp_pop = wb_melt[SeriesName %in% c("Population, total", "GDP at market prices (constant 2010 US$)"), list(CountryCode, SeriesName, Year=as.numeric(as.character(Year)), Value=Value/1000000)]
	gdp_pop2 = data.table(dcast(gdp_pop, CountryCode + Year  ~ SeriesName, value.var = 'Value'))
	setnames(gdp_pop2, c("CountryCode", "Year", "gdp", "population"))
	gdp_pop3 = merge(gdp_pop2, getCountryCodes(), by='CountryCode')
	cd2 = merge(cd, gdp_pop3, by=c('Country', 'Year'), all.x=TRUE)
	cd3 = merge(cd2, cd2[, list(worldPop=sum(population, na.rm=TRUE), worldGdp=sum(gdp, na.rm=TRUE)), by=Year], by="Year")
	cd3[Country=="World", c('population', 'gdp') := list(worldPop, worldGdp)]
	cd3[, c('gdp_pc', 'worldPop', 'worldGdp') := list(gdp/population, NULL, NULL)]
	#cd2[,gdp:=as.numeric(na.approx(gdp,rule=2)), by=Country] # this interpolates and extrapolation (keeping last value for future missing values).
	cd3
}

########## Historic N production
getHistroicN = function() {
	livestockPIN = fread(file.path(fao_data_dir, "Livestock_Production_Index-27-2017.csv"))[, list(Year, livestock=Value)]
	manure = livestockPIN[, list(Year, manure = 43.277 * livestock / livestockPIN[Year==2000]$livestock)]
	fert = fread(file.path(data_dir_root, "IFA", "worldInorganicFert.csv"), header=TRUE)
	fert = melt.data.table(fert, id.vars="Country", variable.name="Year", value.name="fert", variable.factor=FALSE)
	fert[, Year := as.numeric(Year)]
	fert[, fert := fert / 1000]
	merge(fert, manure, by="Year")
}

getWorldLUHist = function() {
	nitrogen = getHistroicN()[, list(Year, fert=manure+fert)]
	faoDt=country_data[Country == "World", list(Year, cropland=arable+perm_crops, pasture=perm_pasture)]
	faoDt = merge(faoDt, nitrogen, by="Year")
	# add irrigation amount based on irrigated area, and 2700km3 in 2010
	faoDt = merge(faoDt, country_data[Country=="World", list(Year, irrig=2700 * irrig / country_data[Country=="World" & Year == 2010]$irrig)], by="Year")
	# add energy crop area
	energyCropArea = data.table(Year = seq(1961, 2013), 	# cite Alexander (2015), extracted as paste0(round(country_con_prod[,list(bioenergy_area=sum(other_area)), by=Year]$bioenergy_area, 2), collapse = ", ")
			energyCropA = c(10.43, 9.94, 11.26, 11.75, 17.44, 11.84, 11.96, 11.14, 11.15, 11.61, 11.61, 11.42, 11.13, 12.25, 11.54, 11.76, 11.68, 12.87, 13.54, 13.02, 14.57, 14.72, 14.87, 16.65, 18.81, 19.01, 20.38, 19.91, 21.92, 25.14, 26.17, 26.38, 25.42, 26.23, 29.57, 31.7, 31.34, 33.7, 35.3, 35.97, 37.09, 40.86, 47.12, 47.89, 55.38, 56.26, 62.89, 68.62, 73.78, 76.65, 80.21, 80.21, 80.21))
	faoDt = merge(faoDt, energyCropArea, by="Year")
	faoDt[, exEnergyCrop := cropland , by=Year]
	faoDt[, energyCropA:=NULL]
	faoDt
}

########## Crop production
readCropProd = function() {
	crop_prod_data = fread(file.path(fao_data_dir, "Production_Crops_E_All_Data_Norm.csv"))
	if (!"Country" %in% names(crop_prod_data)) setnames(crop_prod_data, "Area", "Country")
	crop_prod_data[, Value := as.numeric(Value) / 1000000]
	crop_prod_data[, Year := as.numeric(Year)]
	
	result = data.table(dcast(crop_prod_data, Country + Year + Item ~ Element, value.var = 'Value'))
	result[is.na(result)] = 0
	result[Country == "Eswatini", Country:= "Swaziland"] # some FAO dataset contain old name of Swaziland, and some new name of Eswatini
	
	rbind( result[, list(Country, Year, Item, area=get("Area harvested"), prod=get("Production"))],
			readForageCropProd())
}

readForageCropProd = function() {
	if (exists('forage_prod_data')) rm(forage_prod_data)
	
	for (forageFile in list.files(file.path(fao_data_dir), pattern='forage_.*\\.csv$', full.names=TRUE)) {
		if (exists('forage_prod_data'))
			forage_prod_data = rbind (forage_prod_data, fread(forageFile))
		else
			forage_prod_data = fread(forageFile)
	}
	
	forage_prod_data[, Value := as.numeric(Value)]	
	result = data.table(dcast(forage_prod_data, Country + Year + Item ~ Element, value.var = 'Value'))
	result[is.na(result)] = 0
	result[, list(Item = "Forage crops", area=sum(get("Area harvested"))/1000000,prod=sum(get("Production"))/1000000), by=list(Country, Year)]
}

########## Commodities balance
readCommoditiyBalance = function() {
	processCommoditiyBalance = function(fao_file) {
		fao_data =fread(fao_file)
		setnames(fao_data, gsub(' ', '', (names(fao_data))))  
		fao_data = fao_data[!ItemCode == 2948 & !ItemCode == 2949] # remove duplicate eggs and milk totals, as they have the same Item text name as the individual items
		fao_data[, Value := as.numeric(Value) / 1000000]
		fao_data[, Year := as.numeric(Year)]
		if ("Area" %in% names(fao_data)) setnames(fao_data, "Area", "Country")
		
		if (length(fao_data[Country =="China"]$Country)==0) {
			fao_data = rbind(fao_data, fao_data[Country %like% 'China', list(AreaCode=9999, Country="China", Value=sum(Value), Flag="X"), by=list(ItemCode, Item, ElementCode, Element, YearCode, Year, Unit)])
		}
		
		result = data.table(dcast(fao_data, Country + Year + Item ~ Element, value.var = 'Value'))
		result[is.na(result)] = 0
		setnames(result, c("Domestic supply quantity", "Seed","Processing","Food supply quantity (tonnes)","Feed","Production","Stock Variation","Import Quantity","Export Quantity","Other uses"),
				c("supply", "seed", "processing", "food", "feed", "prod", "stockvar", "import", "export", "other"))
		
		
		if ("Waste" %in% names(result)) setnames(result, "Waste", "waste") else setnames(result, "Losses", "waste")
		result[, Item := gsub(' and products', '', (Item))]
		result
	}
	
	rbind(processCommoditiyBalance(file.path(fao_data_dir, "CommodityBalances_Crops_E_All_Data_Norm.csv")),
			processCommoditiyBalance(file.path(fao_data_dir, "CommodityBalances_LivestockFish_E_All_Data_Norm.csv")))
}

########## Food supply
readFoodSupply = function() {
	processFoodSupply = function(fao_file) {
		fao_data =fread(fao_file)
		
		if (!"Country" %in% names(fao_data)) setnames(fao_data, "Area", "Country")
		fao_data[, Value := as.numeric(Value)]
		fao_data[, Year := as.numeric(Year)]
		
		setnames(fao_data, gsub(' ', '', (names(fao_data))))  
		fao_data = fao_data[!ItemCode == 2948 & !ItemCode == 2949] # remove duplicate eggs and milk totals, as they have the same Item text name as the individual items
		
		result = data.table(dcast(fao_data[Item != "Miscellaneous"], Country + Year + Item ~ Element, value.var = 'Value'))
		
		result = result[, list(Country, Year, Item, food_t = get("Food supply quantity (tonnes)"), energy_kcal_pc= get("Food supply (kcal/capita/day)"),
						protein_g_pc = get("Protein supply quantity (g/capita/day)"))]
		result[, Item := gsub(' and products', '', (Item))]
	}
	
	animal_food = processFoodSupply(file.path(fao_data_dir, "FoodSupply_LivestockFish_E_All_Data_Norm.csv"))
	
	rbind(processFoodSupply(file.path(fao_data_dir, "FoodSupply_Crops_E_All_Data_Norm.csv")),
			animal_food[Item %in% c(getFeedConvRatio()$Item, "Fish Seafood")])
}

handleProcessedOilcrops = function(com_bal,processedComm, resultantComms) {
	
	processedCropAggRows = com_bal[Item %in% resultantComms, list(Item=processedComm, 
					supply=sum(-export+import+stockvar), prod=0, # no change to prod or supply due to tracing processed commodities, adjust supply to match import/exports
					processing=-sum(prod), # so that processed amounts will be removed from oilcrop row when summed
					export=sum(export), import=sum(import), feed=sum(feed), food=sum(food), other=sum(other),
					seed=sum(seed), stockvar=sum(stockvar), waste=sum(waste)), by=list(Country, Year)]
	
	rbind(com_bal, processedCropAggRows)[, list(supply=sum(supply), export=sum(export), feed=sum(feed), food=sum(food), 
					import=sum(import), other=sum(other), processing=sum(processing), prod=sum(prod),
					seed=sum(seed), stockvar=sum(stockvar), waste=sum(waste)), by=list(Country, Year, Item)]
}

checkUnhandled = function(aCountry, aYear = 2011) {
	cropland = country_data[Country ==aCountry & Year == aYear, list(arable+ perm_crops)]
	havested = con_prod_unadj[Country ==aCountry & Item %in% itemDetails[produced==TRUE]$Item & Year == aYear, sum(prod_area, na.rm=TRUE)]
	modelled = crop_prod[Item %in% itemDetails$Item & Country == aCountry & Year == year, sum(area)] + 280.105200 # Oilcrops Primary not in dataset from FAO anymore 280.105200 is what it totals
	
	setasideOrFallow = (cropland-havested)/cropland
	modelled = (havested-modelled)/cropland
	c(setasideOrFallow=setasideOrFallow, modelled=modelled)
}

shortenCountryNames = function (dt) {
	dt[Country=="United States of America", Country := "USA"]
	dt[Country=="United Kingdom", Country := "UK"]
	dt[Country=="Iran (Islamic Republic of)", Country := "Iran"]
	dt[Country=="Russian Federation", Country := "Russia"]
	dt[Country=="Sudan (former)", Country := "Sudan"]
	dt[Country=="Venezuela (Bolivarian Republic of)", Country := "Venezuela"]
	dt[Country=="United Republic of Tanzania", Country := "Tanzania"]
	dt[Country=="Viet Nam", Country := "Vietnam"]
	dt[Country=="United Arab Emirates", Country := "UAE"]
	dt[Country=="Democratic Republic of the Congo", Country := "DR Congo"]
	dt[Country=="Bolivia (Plurinational State of)", Country := "Bolivia"]
	dt
}

country_data = readCountryData()
itemDetails = getItemDetails()
worldLUHist=getWorldLUHist()
