library(tidyverse)
library(foreign)

get_area <- function(lat, grid_size=0.5) {
  grid_size * pi / 180 * (sin(lat * pi / 180) - sin((lat - grid_size) * pi / 180)) * 6371^2 / 10000
}

project_dir <- "~/Documents/PhD/git_rstudio/landalloc/PLUM/LandCover"
plum_dir <- "~/Documents/plumv2"
landalloc_dir <- "~/Documents/PhD/git_rstudio/landalloc"

# SPAM 2010
# https://doi.org/10.5194/essd-12-3545-2020

areas_1 <- read.dbf(file.path(landalloc_dir, "InputData/SPAM2010/spam2010v2r0_global_phys_area.dbf/spam2010V2r0_global_A_TA.dbf")) %>%
  mutate(Lat = floor(Y / 0.5) * 0.5 + 0.25,
         Lon = floor(X / 0.5) * 0.5 + 0.25) %>%
  group_by(Lon, Lat) %>%
  summarise(across(WHEA_A:VEGE_A, sum))

ggplot(areas_1, aes(x=Lon, y=Lat, fill=WHEA_A)) +
  geom_raster()

mapping <- rbind(
 c("whea", "wheat"),
 c("rice", "rice"),
 c("maiz", "maize"),
 c("barl", "wheat"),
 c("pmil", "maize"),
 c("smil", "maize"),
 c("sorg", "maize"),
 c("ocer", "wheat"),
 c("pota", "starchyRoots"),
 c("swpo", "starchyRoots"),
 c("cass", "starchyRoots"),
 c("orts", "starchyRoots"),
 c("bean", "pulses"),
 c("chic", "pulses"),
 c("cowp", "pulses"),
 c("pige", "pulses"),
 c("lent", "pulses"),
 c("opul", "pulses"),
 c("soyb", "oilcropsNFix"),
 c("grou", "oilcropsNFix"),
 c("cnut", "oilcropsOther"),
 c("bana", "fruitveg"),
 c("plnt", "fruitveg"),
 c("trof", "fruitveg"),
 c("temf", "fruitveg"),
 c("vege", "fruitveg"),
 c("oilp", "oilcropsOther"),
 c("sunf", "oilcropsOther"),
 c("rape", "oilcropsOther"),
 c("sesa", "oilcropsOther"),
 c("ooil", "oilcropsOther"),
 c("sugc", "sugar"),
 c("sugb", "sugar")
)

mapping <- as.data.frame(mapping)
colnames(mapping) <- c("name", "Crop") 
mapping <- mutate(mapping, name = toupper(name))

lc <- read.table(file.path(plum_dir, "data/halfdeg/hilda_plus_2020.txt"), header = T) %>%
  mutate(Cropland = get_area(Lat) * CROPLAND) %>%
  select(Lon, Lat, Cropland) %>%
  mutate(Lat = Lat + 0.25, Lon = Lon + 0.25)

areas_agg <- areas_1 %>%
  pivot_longer(cols = WHEA_A:VEGE_A) %>%
  mutate(name = gsub("_A", "", name)) %>%
  inner_join(mapping, by=c("name"="name")) %>%
  group_by(Lon, Lat, Crop) %>%
  summarise(area = sum(value) / 1e6) %>%
  group_by(Lon, Lat) %>%
  pivot_wider(names_from = Crop, values_from = area, values_fill = 0) %>%
  na.omit() %>%
  mutate(energycrops = 0, setaside = 0)

areas_agg$total <- rowSums(areas_agg[, 3:13])

fractions <- areas_agg %>%
  inner_join(lc) %>%
  filter(Cropland > 0, total > 0) %>%
  mutate(MaxArea = pmax(Cropland, total),
         across(fruitveg:setaside, ~ .x / MaxArea))

summary(rowSums(fractions[, 3:13]))

ggplot(fractions, aes(Lon, Lat, fill=wheat)) +
  geom_raster() +
  scale_fill_gradientn(colours=rev(terrain.colors(20)))

foo <- fractions %>% mutate(across(fruitveg:energycrops, ~ .x * Cropland))
colSums(foo)
sum(colSums(foo)[3:12])


# Update crop areas -----------------------------------------------------------------------------------------------

# Crop Areas
fao_yields_data <- read_csv(file.path(landalloc_dir, "InputData/FAOStat-Nov2023",
                                      "Production_Crops_Livestock_E_All_Data_(Normalized)",
                                      "Production_Crops_Livestock_E_All_Data_(Normalized).csv"),
                            name_repair = "universal")

country_codes <- read_csv(file.path(landalloc_dir, "InputData/FAOSTAT_countries.csv"), name_repair = "universal") %>%
  rename(M49=M49.Code, Iso3=ISO3.Code)

plum_countries <- read_csv(file.path(plum_dir, "data/countries.csv")) %>% select(Iso3, PlumGroup, M49)

product_map <- read.csv(file.path(landalloc_dir, "PLUM/YieldCalibFactors/fao_crop_map.csv"))

fao_yields <- fao_yields_data %>%
  filter(Year == 2020, Element %in% c("Area harvested", "Production"), Area.Code < 1000) %>%
  left_join(country_codes %>% select(Country.Code, Iso3), by=c("Area.Code"="Country.Code")) %>%
  inner_join(product_map %>% select(-Item), by=c("Item.Code"="Item.Code"))  %>%
  pivot_wider(id_cols = c("Year", "Iso3", "Item", "PlumCrop"), names_from = Element, values_from = "Value", 
              names_repair = "universal") %>%
  filter(Production > 0, Area.harvested > 0) %>%
  group_by(Year, Iso3, PlumCrop) %>%
  summarise(Area = sum(Area.harvested) / 1e6,
            Production = sum(Production) / 1e6,
            Yield = Production / Area) %>%
  ungroup()

crop_names <- data.frame(FaoName = c("FruitVeg", "MaizeMilletSorghum", "OilcropsNFix", "OilcropsOther", "Pulses","Rice (Paddy Equivalent)", "Starchy Roots", "Sugar", "WheatBarleyOats"),
                         Crop = c("fruitveg", "maize", "oilcropsNFix", "oilcropsOther", "pulses", "rice", "starchyRoots", "sugar", "wheat"))

fao_yields <- left_join(fao_yields, crop_names, by = join_by(PlumCrop==FaoName))

# Distribute difference between SPAM 2010 and FAO 2020
borders <- read_csv(file.path(plum_dir, "data/halfdeg/country_boundaries.csv")) %>% 
  mutate(Lon = Lon + 0.25, Lat = Lat + 0.25)

out_areas <- data.frame()

for (m49 in plum_countries$M49) {
  iso3 <- plum_countries$Iso3[plum_countries$M49 == m49]
  print(iso3)
  fao_areas <- fao_yields %>% filter(Iso3 == iso3)
  
  crop_areas <- areas_agg %>% inner_join(borders %>% filter(M49 == m49)) %>%
    select(Lon, Lat, fruitveg:wheat) %>%
    pivot_longer(cols=fruitveg:wheat, names_to = "Crop", values_to = "Area") %>%
    filter(Crop %in% fao_areas$Crop)
  
  for (i in 1:10) {
    agg_areas <- crop_areas %>%
      group_by(Crop) %>%
      summarise(Area = sum(Area))
    
    ratio <- agg_areas %>%
      inner_join(fao_areas %>% rename("FaoArea"=Area), by=join_by(Crop)) %>%
      mutate(Ratio = if_else(Area > 0, FaoArea / Area, 1)) %>%
      select(Crop, Ratio)
    
    crop_areas <- crop_areas %>%
      left_join(ratio, by=join_by(Crop)) %>%
      mutate(Area = Area * Ratio) %>%
      select(-Ratio)
    
    total_cropland <- crop_areas %>%
      group_by(Lon, Lat) %>%
      summarise(Area = sum(Area)) %>%
      left_join(lc, by=join_by(Lon, Lat)) %>%
      mutate(Ratio = if_else(Area > 0, Cropland / Area, 0)) %>%
      select(Lon, Lat, Ratio)
    
    crop_areas <- crop_areas %>%
      left_join(total_cropland, by = join_by(Lon, Lat)) %>%
      mutate(Area = Area * Ratio) %>%
      select(-Ratio)
  }
  
  out_areas <- rbind(out_areas, crop_areas %>% mutate(Iso3=iso3))

}

crop_fractions <- out_areas %>%
  mutate(Area = Area * (1 - 0.08298 - 0.01323)) %>% # Adjust for setaside and unhandled crops
  select(-Iso3) %>%
  pivot_wider(names_from = Crop, values_from = Area, values_fill = 0) %>%
  left_join(lc, by = join_by(Lon, Lat)) %>%
  mutate(energycrops = 0,
         setaside = Cropland * (0.08298 + 0.01323))

crop_fractions$Check <- rowSums(crop_fractions[, c(3:11, 13, 14)])
crop_fractions <- mutate(crop_fractions, Diff=Check-Cropland)  

crop_fractions <- crop_fractions %>%
  select(Lon, Lat, fruitveg:oilcropsNFix, energycrops, setaside, Cropland) %>%
  mutate(across(fruitveg:setaside, ~ if_else(Cropland > 0, .x / Cropland, 0)))

crop_areas <- select(crop_fractions, -Cropland)

ggplot(crop_fractions, aes(Lon, Lat, fill=pulses)) +
  geom_raster()

write_csv(crop_fractions, file.path(plum_dir, "data/halfdeg/crop_fractions.csv"))

dt <- fao_yields %>%
  inner_join(plum_countries) %>%
  group_by(Crop) %>%
  summarise(Area = sum(Area)) %>%
  mutate(Area = round(Area, 0))

dt <- rbind(dt, c("pasture", 3180))
dt <- rbind(dt, c("energycrops", 30))

write_csv(dt, file.path(plum_dir, "data/global_crop_areas.csv"))


# Rice multiple cropping ------------------------------------------------------------------------------------------

nc_file <- nc_open(file.path(landalloc_dir, "/InputData/Waha_Katharina_02_Apr_2024/data/MultipleCropping_level3.nc"))

x <- ncatt_get(nc_file, "Cropping area level 3 - Individual system area", 'long_name')$value
x <- strsplit(x, ";")[[1]]
rice_idx <- x[grepl("rice.*rice", x)]
rice_idx <- x[grepl("rice", x)]
which(rice_idx[2] == x)

rice_systems_df <- data.frame(Name = c("l3_area_rice_rf_1", "l3_area_rice_rf_2", "l3_area_rice_ir_1.rice_ir_2", 
                                       "l3_area_rice_ir_2.rice_ir_1", "l3_area_rice_ir_3.rice_ir_1.rice_ir_2"),
                              Multi = c(1, 1, 2, 2, 3))

rice_multi_df <- data.frame()

for (i in 1:nrow(rice_systems_df)) {
  sys_name <- rice_systems_df$Name[i]
  multi <- rice_systems_df$Multi[i]
  
  idx <- which(sys_name == x)
  
  arr <- ncvar_get(nc_file, varid="Cropping area level 3 - Individual system area")[, , idx]
  arr[is.na(arr)] <- 0
  
  ras <- rast(t(arr))
  crs(ras) <- "+proj=longlat +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0"
  ext(ras) <- c(-180, 180, -90, 90)
  
  dt <- as.data.frame(ras, xy=T) %>% mutate(Multi = multi) %>% filter(lyr.1 > 0)
  
  rice_multi_df <- rbind(rice_multi_df, dt)
  
}

rice_multi_agg_df <- rice_multi_df %>%
  group_by(x, y) %>%
  summarise(Multi = sum(Multi * lyr.1) / sum(lyr.1)) %>%
  rename(Lat = y, Lon = x) %>%
  select(Lon, Lat, Rice = Multi)

ggplot(rice_multi_agg_df, aes(Lon, Lat, fill=Rice)) +
  geom_raster() +
  scale_fill_gradientn(colours = rev(terrain.colors(20)))


write_csv(rice_multi_agg_df, file.path(plum_dir, "data/halfdeg/rice_multi_crop.csv"))

