require(ggplot2)
require(gridExtra)
require(data.table)

baseFigDir="~/Documents/LURG/PLUM/SamsCouplingPaper"

plotAllLc = function(lcType, baseYr=2010, simFile="~/Documents/R_Workspace/UNPLUM/data/sims/ssp_sims.csv") {
	lc=fread("~/Documents/LURG/PLUM/SamsCouplingPaper/lc_concat.txt")
	lcSims =  lc
#	scenarios=fread(simFile)
#	lcSims = merge(scenarios, lc, by=c("Ensemble", "Scenario"))
#	lcSims = merge(lcSims, lcSims[Year == 1970, list(Scenario, fert1970=FertCrop)], by=c('Scenario'))
	
#	minCutOff = lcSims[Year == baseYr, mean(get(lcType)) - sd(get(lcType))]
#	maxCutOff = lcSims[Year == baseYr, mean(get(lcType)) + sd(get(lcType))]
#	goodScenarios = lcSims[Year == baseYr & get(lcType)>=minCutOff & get(lcType)<=maxCutOff, Scenario]
#	lcSims = lcSims[Scenario %in% goodScenarios]
	
	ggplot(lcSims, aes(x=Year, y=get(lcType), colour=Scenario, dummy=Ensemble))  + 
			geom_line() #+ theme(legend.position="none")
}

plotAllLc(lcType="Pasture", baseYr=1970, simFile="~/Documents/R_Workspace/UNPLUM/data/sims/hind_sims.csv")


plotConeLc = function (baseYr=2010, filename="~/Downloads/lc_concat.txt", outfilename) {
	lc=fread(filename)
	
	if (!"Scenario" %in% names(lc)) lc[, Scenario := "dummy"]
	if (!"Ensemble" %in% names(lc)) lc[, Ensemble := Scenario]
	lc = lc[Scenario !="calib"]
	
	lc[Ensemble=="hb", Ensemble:="Validation\nsimulations"]
	f_melt = melt(worldLUHist[, list(Year, cropland, pasture, fert, irrig)], id.vars='Year', variable.name='variable', value.name="Value")
	h_melt = melt(lc[, list(Ensemble, Scenario, Year, cropland=Cropland, pasture=Pasture, fert=FertCrop, irrig=IrrigCrop)], id.vars=c('Ensemble', 'Scenario', 'Year'), variable.name='variable', value.name="Value")
	h_melt[, rebased:="i) Unadjusted"]
	
	rebase_diff = merge(h_melt[Year == baseYr, list(Ensemble,Scenario, hind=Value, variable)], f_melt[Year == baseYr, list(fao=Value, variable)], by=c('variable'))[, list(Ensemble, Scenario, variable, diff=fao-hind)]
	h_r_melt = merge(h_melt, rebase_diff, by=c('Ensemble', 'Scenario', 'variable'))[, list(Ensemble, Scenario, Year, variable, Value=Value+diff, rebased="ii) Rebased")]
	h_avg = rbind(h_r_melt, h_melt)[, list(sdev = sd(Value), avg = median(Value)), by=list(Year, Ensemble, variable, rebased)]
	
	res2 = rbind(h_avg,
			data.table(rebased=c("i) Unadjusted", "ii) Rebased"), Ensemble="Historic (FAO/IFA)")[,as.list(f_melt[, list(Year, variable, avg=Value, sdev=NA)]),by=list(rebased, Ensemble)])
	
	res2[variable=="cropland", variable:="a) Cropland area\n (Mha)"]
	res2[variable=="pasture", variable:="b) Pasture area\n (Mha)"]
	res2[variable=="fert", variable:="c) Nitrogen applied\n (Mt)"]
	res2[variable=="irrig", variable:="d) Irrigation water\n withdrawn (km\u00b3)"]
	res2[, variable:=factor(variable, levels=c("a) Cropland area\n (Mha)", "b) Pasture area\n (Mha)", "c) Nitrogen applied\n (Mt)", "d) Irrigation water\n withdrawn (km\u00b3)"))]
	res2[, rebased:=factor(rebased, levels=c("i) Unadjusted", "ii) Rebased"))]
	
	if (baseYr > 2000) {
		minHistToPlot=1995 
		res2[Ensemble == "none", Ensemble := "Constant 2010\nclimate"]
#		res2[Ensemble == "Historic (FAO/IFA)", Ensemble := "Historic\n(FAO/IFA)"]
#		res2[, Ensemble:=factor(Ensemble, levels=c("Historic\n(FAO/IFA)", "26","45","60","85","Constant 2010\nclimate"))]
	} else { 
		minHistToPlot=1960
		res2 = res2[!(Year>2010 & Ensemble != "Historic (FAO/IFA)")]
		res2[Ensemble != "Historic (FAO/IFA)", Ensemble := "1970-2010\nmodelled"]
		res2[Ensemble == "Historic (FAO/IFA)", Ensemble := "Historic\n(FAO/IFA)"]
	}
	
	p1 = ggplot(res2) +
			geom_line(data=res2[Year >=minHistToPlot], aes(x=Year, y=avg, colour=Ensemble, group=Ensemble, linetype=Ensemble, alpha=Ensemble, fill=Ensemble, size=Ensemble)) +
			facet_grid(variable ~ rebased, scales="free", switch = 'y') +
			theme(legend.title=element_blank(), 
					strip.text.y = element_text(angle = 180),
					legend.key.size = unit(2.5, 'lines'),
					axis.title.y = element_blank(), # remove the default y-axis title, "wt"
					strip.background = element_rect(fill = 'transparent'), # replace the strip backgrounds with transparent
					strip.placement = 'outside', # put the facet strips on the outside
					axis.title.x = element_text(angle=0, size=9))
			#guides(fill=FALSE) + # scale_fill_discrete(labels="Uncertainty range")  +  
	
	if (baseYr==1970) {
		p1 = p1 + geom_ribbon(data=res2[Ensemble!="Historic (FAO/IFA)"], aes(x=Year, ymin=avg-sdev,ymax=avg+sdev, fill=Ensemble), alpha=0.10) +
				geom_ribbon(data=res2[Ensemble!="Historic (FAO/IFA)"], aes(x=Year, ymin=avg-2*sdev,ymax=avg+2*sdev, fill=Ensemble), alpha=0.10) +
				scale_colour_manual(values = c("red", "grey10")) +
				scale_alpha_manual(values = c(0.9, 1.0)) +
				scale_size_manual(values = c(0.45, 0.4)) +
				scale_fill_manual(values=c("red", "white")) +
				scale_linetype_manual(values=c("dashed", "solid"))
	} else {
		p1 = p1 + 
				geom_ribbon(data=res2[Ensemble!="Historic (FAO/IFA)"], aes(x=Year, ymin=avg-sdev,ymax=avg+sdev, fill=Ensemble), alpha=0.10) +
				#scale_colour_manual(values = c("grey10","#377eb8", "#984ea3","#ff7f00", "#e41a1c", "grey10")) +
				scale_colour_manual(values = c("#FFB72BCC","#96DA00CC","#00EABECC", "#00D9FFCC", "#F9A3FFCC", "grey10")) +
				scale_fill_manual(values=c("#FFB72BCC","#96DA00CC","#00EABECC", "#00D9FFCC", "#F9A3FFCC", "white")) +
				scale_alpha_manual(values = c(1.0, rep(0.85, 5))) +
				scale_size_manual(values = c(0.4, rep(0.45, 5))) +
				scale_linetype_manual(values=c(rep("dashed", 5), "solid"))
	}
	p1 + ggsave(file=file.path(baseFigDir, outfilename),width=8,height=6.5)
}
plotConeLc(baseYr=2010, filename="~/Documents/LURG/PLUM/SamsCouplingPaper/lc_concat.txt", outfilename="ssp.pdf")
plotConeLc(baseYr=1970, filename="~/Downloads/lc_h1.txt", outfilename="h1.pdf")

#plotConeLc(baseYr=1970, filename=file.path(baseFigDir, "hind_lc_concat.txt"), outfilename="hind_change_over_time.pdf")
plotConeLc(outfilename="change_over_time.pdf")
plotConeLc(baseYr=2010, filename="~/Downloads/lc.txt", outfilename="test.pdf")
plotConeLc(baseYr=1970, filename="~/Downloads/lc_h2.txt", outfilename="h2.pdf")





plotConeLcHind = function (baseYr=1970, endYr=2010, filename="~/Downloads/lc_concat.txt", outfilename, saveToFile=FALSE) {
	lc=fread(filename)
	
	f_melt = melt(worldLUHist[, list(Year, cropland, pasture, fert, irrig)], id.vars='Year', variable.name='variable', value.name="Value")
	h_melt = melt(lc[, list(Ensemble, Scenario, Year, cropland=Cropland, pasture=Pasture, fert=FertCrop, irrig=IrrigCrop)], id.vars=c('Ensemble', 'Scenario', 'Year'), variable.name='variable', value.name="Value")
	h_melt[, rebased:="(i) Unadjusted"]
	rebase_diff = merge(h_melt[Year == baseYr, list(Ensemble,Scenario, hind=Value, variable)], f_melt[Year == baseYr, list(fao=Value, variable)], by=c('variable'))[, list(Ensemble, Scenario, variable, diff=fao-hind)]
	h_r_melt = merge(h_melt, rebase_diff, by=c('Ensemble', 'Scenario', 'variable'))[, list(Ensemble, Scenario, Year, variable, Value=Value+diff, rebased="(ii) Rebased")]
	allRes = rbind(h_r_melt, h_melt)
	h_avg = allRes[, list(sdev = sd(Value), avg = median(Value)), by=list(Year, Ensemble, variable, rebased)]
	res2 = rbind(h_avg, data.table(rebased=c("(i) Unadjusted", "(ii) Rebased"), Ensemble="Historic (FAO/IFA)")[,as.list(f_melt[, list(Year, variable, avg=Value, sdev=NA)]),by=list(rebased, Ensemble)])
	
	res2[variable=="cropland", variable:="(a) Cropland area\n (Mha)"]
	res2[variable=="pasture", variable:="(b) Pasture area\n (Mha)"]
	res2[variable=="fert", variable:="(c) Nitrogen applied\n (Mt)"]
	res2[variable=="irrig", variable:="(d) Irrigation water\n withdrawn (km\u00b3)"]
	
	resToPlot = res2[!(Year>2010 & Ensemble != "Historic (FAO/IFA)")]
	resToPlot[Ensemble != "Historic (FAO/IFA)" & rebased == "(i) Unadjusted", c('Ensemble', 'sdev') := list("1970-2010 modeled    \n(unadjusted)", NA)]
	resToPlot[Ensemble != "Historic (FAO/IFA)" & rebased == "(ii) Rebased",  Ensemble := "1970-2010 modeled    \n(rebased at 1970)"]
	lc[, Ensemble:=factor(Ensemble, levels=c("1970-2010 modeled    \n(unadjusted)", "1970-2010 modeled    \n(rebased at 1970)", "Historic (FAO/IFA)"))]
	colours=c("#d53e4f", "#3288bd")
	boxplotWidth=0.4
	boxplotDt = allRes[Year == endYr]
	boxplotDt[Ensemble != "Historic (FAO/IFA)" & rebased == "(i) Unadjusted", Ensemble := "1970-2010 modeled\n(unadjusted)"]
	boxplotDt[Ensemble != "Historic (FAO/IFA)" & rebased == "(ii) Rebased", Ensemble := "1970-2010 modeled\n(rebased at 1970)"]
	cRange = range(allRes[variable=="cropland"]$Value, 1320, 1740)
	pRange = range(allRes[variable=="pasture"]$Value, 3000)
	fRange = range(allRes[variable=="fert"]$Value, 20)
	wRange = range(allRes[variable=="irrig"]$Value,1240, 3300)
	
	getConePlotForType = function (dt, dType, scaleRange, colours) {
		p1 = ggplot(dt[variable==dType]) +
				geom_line(aes(x=Year, y=avg, colour=Ensemble, group=Ensemble, linetype=Ensemble, alpha=Ensemble, fill=Ensemble, size=Ensemble)) +
				labs(y=dType) +
				scale_y_continuous(limits=scaleRange) +
				scale_x_continuous(minor_breaks = seq(1960, 2010, 5), breaks = seq(1960, 2010, 10)) +
				theme_minimal() + 
				theme(panel.border = element_rect(colour = "grey10", fill=NA, size=0.3), 
						legend.title=element_blank(), 
						legend.key.size = unit(1.5, 'lines'),
						axis.text=element_text(size=7),
						axis.title.x = element_text(angle=0, size=8), 
						axis.title.y = element_text(angle=90, size=9, face="bold"), 
						plot.margin = unit(c(0,0,0.3,0.4), "cm"),
						legend.position = "none") +
				scale_colour_manual(values = colours, guide = guide_legend(nrow = 1, byrow=TRUE)) +
				geom_ribbon(aes(x=Year, ymin=avg-2*sdev,ymax=avg+2*sdev, fill=Ensemble), alpha=0.07) +
				geom_ribbon(aes(x=Year, ymin=avg-sdev,ymax=avg+sdev, fill=Ensemble), alpha=0.12) +
				scale_alpha_manual(values = rep(1.0, 3)) +
				scale_size_manual(values = rep(0.5, 3)) +
				scale_fill_manual(values=c("red", "white", "white")) +
				scale_linetype_manual(values=c("solid", "solid", "dashed"))
	}
	
	cp = getConePlotForType(resToPlot, dType="(a) Cropland area\n (Mha)", scaleRange=cRange, colours=c(colours, "grey10"))	
	pp = getConePlotForType(resToPlot, dType="(b) Pasture area\n (Mha)", scaleRange=pRange, colours=c(colours, "grey10"))
	fp = getConePlotForType(resToPlot, dType="(c) Nitrogen applied\n (Mt)", scaleRange=fRange, colours=c(colours, "grey10"))
	wp = getConePlotForType(resToPlot, dType="(d) Irrigation water\n withdrawn (km\u00b3)", scaleRange=wRange, colours=c(colours, "grey10"))
	
	getBoxPlotForType = function (dt, scaleRange, colours) {
		ggplot(data = dt, aes(x="1", y=Value, colour=Ensemble)) +  
				geom_boxplot(position=position_dodge(1.0), outlier.size = 0.5) + 
				scale_y_continuous(limits=scaleRange) + 
				theme_minimal() +
				labs(x="") +
				theme(legend.position="none", panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
						axis.title.y = element_blank(),
						axis.title.x = element_text(angle=0, size=9), # need this to keep same spacing as cone plot
						axis.text.x=element_text(colour = "white"), axis.text.y=element_blank(),
						axis.ticks =element_blank(),
						plot.margin = unit(c(0,0,0.3,0), "cm")) +
				scale_colour_manual(values = colours)
	}
	
	cbp = getBoxPlotForType(boxplotDt[variable=="cropland"], scaleRange=cRange, colours)
	pbp = getBoxPlotForType(boxplotDt[variable=="pasture"], scaleRange=pRange, colours)
	fbp = getBoxPlotForType(boxplotDt[variable=="fert"], scaleRange=fRange, colours)
	wbp = getBoxPlotForType(boxplotDt[variable=="irrig"], scaleRange=wRange, colours)
	
	g <- ggplotGrob(cp + theme(legend.position="bottom"))$grobs
	legend <- g[[which(sapply(g, function(x) x$name) == "guide-box")]]
	
	if (saveToFile) pdf(width=8, 5.0, file = file.path(baseFigDir, outfilename))
	grid.arrange(cp, cbp, pp, pbp, fp, fbp, wp, wbp, legend, nrow=3, widths = c(6, boxplotWidth, 6, boxplotWidth), heights=c(1, 1, 0.25), padding=0, 
			layout_matrix = rbind(c(1,2, 3,4), c(5,6,7,8), c(9,9,9,9)))
	if (saveToFile) dev.off()
}
plotConeLcHind(filename="~/Documents/LURG/PLUM/offline_paper/hind1970/hc/lc_concat.txt", outfilename="hind_hc_comp_over_time.pdf", saveToFile=TRUE)




plotConeRcpOrSsp = function (ensemble, outBase=baseOutputDir, baseYr=2010, endYr=2100, outfilename, saveToFile=FALSE) {
	lc=fread(file.path(outBase, "lc_concat.txt"))
	
	if (ensemble %like% 'rcp') {
		lc = lc[Ensemble %in% c("26","45","60","85","constCO2_constClim") & Scenario != 's40']
		lc[, Ensemble := gsub("([0-9])([0-9])$", "RCP\\1.\\2",  Ensemble)]
		lc[Ensemble=="constCO2_constClim", Ensemble := "Constant-climate+CO2   "]
		lc[, Ensemble:=factor(Ensemble, levels=c("Constant-climate+CO2   ", "RCP2.6", "RCP4.5", "RCP6.0", "RCP8.5"))]
		lineTypes = c("dotdash", rep("solid", 4))
		colours = c("#3288bd", "#99d594", "#fee08b", "#fc8d59", "#d53e4f")
		fillColours = c("blue", "green", "yellow", "orange", "red", "white")
	} else if (ensemble %like% 'ssp') {
	#	lc = lc[!Scenario %in% c('s12', 's31', 's25', 's45')]
		lc[, Ensemble:=factor(Ensemble, levels=paste0("SSP",seq(1,5)))]
		lineTypes = rep("solid", 5)
		colours = c("green", "blue", "red", "orange", "magenta")
		fillColours = c(colours, "white")
	}
	
	minHistToPlot=1995 
	
	f_melt = melt(worldLUHist, id.vars='Year', variable.name='variable', value.name="Value")
	allRes = melt(lc[, list(Ensemble, Scenario, Year, cropland=Cropland, pasture=Pasture, fert=FertCrop, irrig=IrrigCrop, exEnergyCrop=Cropland-EnergyCrop)], id.vars=c('Ensemble', 'Scenario', 'Year'), variable.name='variable', value.name="Value")
	h_avg = allRes[, list(sdev = sd(Value), avg = median(Value)), by=list(Year, Ensemble, variable)]
	res2 = rbind(h_avg, f_melt[, list(Year, variable, avg=Value, sdev=NA, Ensemble="Historic (FAO/IFA)")])

	res2[variable=="cropland", variable:="(a) Cropland area\n(Mha)"]
	res2[variable=="pasture", variable:="(b) Pasture area\n(Mha)"]
	res2[variable=="fert", variable:="(c) Nitrogen applied\n(Mt)"]
	res2[variable=="irrig", variable:="(d) Irrigation water\nwithdrawn (km\u00b3)"]
	res2[variable=="exEnergyCrop", variable:="(e) Food and feed cropland\narea (Mha)"]
	
	# add yields from LandUseSummary.csv
#	luSum = fread(file.path(outBase, "LandUseSummary.csv"))[cropType %in% c('avgIncEC', 'avgExcEC') & Ensemble %in% c("26","45","60","85","constCO2_constClim")]
#	luSum[, Ensemble := gsub("([0-9])([0-9])$", "RCP\\1.\\2",  Ensemble)]
#	luSum[Ensemble=="constCO2_constClim", Ensemble := "RCP8.5, constant-climate+CO2"]
#	allRes = rbind(allRes, luSum[, list(Ensemble, Scenario, Year, variable=cropType, Value=yield)])
#	res2 = rbind(res2, luSum[, list(sdev = sd(yield), avg = median(yield)), by=list(Year, Ensemble, variable=cropType)])
#	res2[variable=="avgExcEC", variable:="e) Mean yield excluding\nenergy crops (t/ha)"]
	
	resToPlot = res2
	boxplotDt = allRes[Year == endYr]
	cRange = range(allRes[variable=="cropland"]$Value)
	pRange = range(allRes[variable=="pasture"]$Value)
	fRange = range(allRes[variable=="fert"]$Value, 100)
	wRange = range(allRes[variable=="irrig"]$Value)
#	yRange = range(allRes[variable=="avgExcEC"]$Value)
	exEcRange = range(allRes[variable=="exEnergyCrop"]$Value)
	
	getConePlotForType = function (dt, dType, scaleRange, colours) {
		d = dt[variable==dType]
		
		p1 = ggplot(d[Year>minHistToPlot]) +
				geom_line(aes(x=Year, y=avg, colour=Ensemble, group=Ensemble, linetype=Ensemble, alpha=Ensemble, fill=Ensemble, size=Ensemble)) +
				labs(y=dType) +
				scale_y_continuous(limits=scaleRange) +
				scale_x_continuous(minor_breaks = seq(2000, 2100, 10), breaks = seq(2000, 2100, 20)) + 
				theme_minimal() + 
				theme(panel.border = element_rect(colour = "grey10", fill=NA, size=0.3), #axis.line = element_line(size = 0.3, colour ="grey10"), 
					legend.title=element_blank(), 
					legend.key.size = unit(2, 'lines'),
					axis.text=element_text(size=7),
					axis.title.x = element_text(angle=0, size=8), 
					axis.title.y = element_text(angle=90, size=9, face="bold"), 
					legend.text=element_text(size=8),
					plot.margin = unit(c(0,0,0.3,0.4), "cm"),
					legend.position = "none") +
				scale_colour_manual(values = colours, guide = guide_legend(nrow = 4, byrow=TRUE)) +
				geom_ribbon(aes(x=Year, ymin=avg-sdev,ymax=avg+sdev, fill=Ensemble), alpha=0.05) +
				scale_fill_manual(values = fillColours) +
				scale_alpha_manual(values = c(rep(1.0, 6))) +
				scale_size_manual(values = c(rep(0.5, 6))) +
				scale_linetype_manual(values=c(lineTypes, "dashed"))
		p1
	}
	
	cp = getConePlotForType(resToPlot, dType="(a) Cropland area\n(Mha)", scaleRange=cRange, colours=c(colours, "grey10"))
	pp = getConePlotForType(resToPlot, dType="(b) Pasture area\n(Mha)", scaleRange=pRange, colours=c(colours, "grey10"))
	fp = getConePlotForType(resToPlot, dType="(c) Nitrogen applied\n(Mt)", scaleRange=fRange, colours=c(colours, "grey10"))
	wp = getConePlotForType(resToPlot, dType="(d) Irrigation water\nwithdrawn (km\u00b3)", scaleRange=wRange, colours=c(colours, "grey10"))
#	yp = getConePlotForType(resToPlot, dType="(e) Mean yield excluding\nenergy crops (t/ha)", scaleRange=yRange, colours=c(colours, "grey10"))
	ep = getConePlotForType(resToPlot, dType="(e) Food and feed cropland\narea (Mha)", scaleRange=exEcRange, colours=c(colours, "grey10"))
	
	getBoxPlotForType = function (dt, scaleRange, colours) {
		ggplot(data = dt, aes(x="1", y=Value, colour=Ensemble)) +  
				geom_boxplot(position=position_dodge(1.0), outlier.size = 0.5) + 
				scale_y_continuous(limits=scaleRange) + 
				theme_minimal() +
				labs(x="") +
				theme(legend.position="none", panel.grid.major = element_blank(), panel.grid.minor = element_blank(),
						axis.title.y = element_blank(),
						axis.title.x = element_text(angle=0, size=9), # need this to keep same spacing as cone plot
						axis.text.x=element_text(colour = "white"), axis.text.y=element_blank(),
						axis.ticks =element_blank(),
						plot.margin = unit(c(0,0,0.3,0), "cm")) +
				scale_colour_manual(values = colours)
	}
	
	cbp = getBoxPlotForType(boxplotDt[variable=="cropland"], scaleRange=cRange, colours)
	pbp = getBoxPlotForType(boxplotDt[variable=="pasture"], scaleRange=pRange, colours)
	fbp = getBoxPlotForType(boxplotDt[variable=="fert"], scaleRange=fRange, colours)
	wbp = getBoxPlotForType(boxplotDt[variable=="irrig"], scaleRange=wRange, colours)
#	ybp = getBoxPlotForType(boxplotDt[variable=="avgExcEC"], scaleRange=yRange, colours)
	ebp = getBoxPlotForType(boxplotDt[variable=="exEnergyCrop"], scaleRange=exEcRange, colours)
	
	g <- ggplotGrob(cp + theme(legend.position="bottom"))$grobs
	legend <- g[[which(sapply(g, function(x) x$name) == "guide-box")]]
	
	if (saveToFile) pdf(width=8, 7.0, file = file.path(baseFigDir, outfilename))
	grid.arrange(cp, cbp, pp, pbp, fp, fbp, wp, wbp, ep, ebp, legend, nrow=3, widths = c(6, 1, 6, 1), heights=c(1, 1, 1), padding=0, 
			layout_matrix = rbind(c(1,2, 3,4), c(5,6,7,8), c(9, 10,11,11)))
	if (saveToFile) dev.off()
}
#plotConeRcpOrSsp('rcp', outBase="~/Documents/LURG/PLUM/offline_paper/rcp", outfilename="rcp_comp_over_time.pdf", saveToFile=TRUE)
plotConeRcpOrSsp('ssp12', outBase="~/Documents/LURG/PLUM/SamsCouplingPaper", outfilename="ssp.pdf", saveToFile=TRUE)

