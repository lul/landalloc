require(raster)
require(data.table)
require(ggplot2)
require(gridExtra)
require(grid)
data(wrld_simpl)

# Read separate LPJ files
readLPJFiles = function(ljpDir, yieldFileName="yield.out", ferts=c("0", "0200", "1000")) {
	yields = fread(file.path(ljpDir, yieldFileName))
	allyields = data.table()
	
	for (fertRate in ferts) {
		print (paste("processing fert: ", fertRate))
		requiredCols = c("CerealsC3", "CerealsC4", "Oilcrops", "StarchyRoots", "Pulses", "Rice")
		requiredCols = c(requiredCols, paste0(requiredCols, 'i'))
		fertSpecificCols = paste0(requiredCols, fertRate)
		cols = c('Lat', 'Lon', fertSpecificCols)
		dt = yields[, cols, with=FALSE]
		setnames (dt, fertSpecificCols, requiredCols)
		allyields = rbind(allyields, cbind(dt, fert=as.numeric(fertRate)))
	}
	
	npps = fread(file.path(ljpDir, "anpp.out"))
	merge(allyields, npps[, list(pasture=max(PC3G, PC4G)), by=list(Lon, Lat)], by=c('Lon', 'Lat'))
}


allyields_2010=readLPJFiles("~/Documents/LURG/LPJ/LPJGPLUM_remap6p7_20190225/rcp26/2006-2010")
allyields_2100=readLPJFiles("~/Documents/LURG/LPJ/LPJGPLUM_remap6p7_20190225/rcp26/2096-2100")



hindyields1=readLPJFiles("/Users/peteralexander/Documents/LURG/LPJ/17July2017/LPJG_PLUM_expt1.1_1961-2015_ipsl_irrV6itp1_forED_20170717090445/1971-1975")
hindyields2=readLPJFiles("/Users/peteralexander/Documents/LURG/LPJ/Aug2017/LPJG_PLUM_expt1.1_1961-2015_ipsl_irrV6104_b20170728/1966-1970")
hindYield_r1=rasterFromXYZ(hindyields1[fert==200, list(Lon, Lat, TeWW)], crs = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
hindYield_r2=rasterFromXYZ(hindyields2[fert==200, list(Lon, Lat, TeWW)], crs = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
plot(hindYield_r2-hindYield_r1)
#plot(crop(hindYield_r2-hindYield_r1, extent(-20, 30, 30, 70)))

polar = CRS("+init=epsg:3995")
r_polar = raster::projectRaster(crop(ras, extent(-180,180,0,90)), crs = polar, method = "bilinear")
par(mar=c(0.5,0.5,0.5,1))
plot(r_polar,  axes=FALSE, box=FALSE)

shpFile = crop(readShapePoly("~/Documents/LURG/Data/NaturalEarthShapefile/ne_110m_admin_0_countries_lakes.shp", proj4string=CRS("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")), extent(-180,180, -55, 90))
plotRegionLPJ = function (lpjtype, regExtent=extent(0, 100, 10, 90)) {
	plot(crop(rasterFromXYZ(hindyields2[fert==200, list(Lon, Lat, get(lpjtype))], crs = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"), regExtent), main=lpjtype)
	plot(shpFile, add = TRUE, border=rgb(r=0.1, g=0.1, b=0.1, alpha=.6), lwd=0.5)
}
layout(matrix(c(1,2,3,4), 2, 2, byrow = TRUE))
par(mar=c(0.5,0.5,0.5,1))
plotRegionLPJ("TeSW")
plotRegionLPJ("TeWW")
plotRegionLPJ("TeCo")
plotRegionLPJ("pasture") 
#plotRegionLPJ("TrRi")



allyields=readLPJFiles("~/Documents/LURG/LPJ/LPJGPLUM_remap6p7_20190225/rcp26/2006-2010")


#  plot 3-d example
plotYieldSurfaceExample = function(lon, lat, crop, maxIrrigRate, title) {
	dt = cbind(rbind(allyields[Lon == lon & Lat == lat, list(fert, type="non-irrig", value=get(crop))], 
					allyields[Lon == lon & Lat == lat, list(fert, type="irrig", value=get(paste0(crop, 'i')))]), irrigMax = maxIrrigRate)
	fmin=min(dt$fert)
	fmax=max(dt$fert)
	fmid=dt[fert!=fmin & fert!=fmax, fert][1]
	
	parms = data.table(fmin, fmid, fmax, 
			vmin = dt[fert==fmin & type=="non-irrig", value], 
			vmax=dt[fert==fmax & type=="non-irrig", value], 
			vmid= dt[fert==fmid & type=="non-irrig", value],
			vmin_irrig= dt[fert==fmin & type=="irrig", value],
			vmax_irrig= dt[fert==fmax & type=="irrig", value],
			irrigMax = dt[fert==fmax & type=="irrig", irrigMax] # l/m2 or mm 299.573
	)
	parms[, fparm := -log(1 - ((vmid-vmin)/(vmax-vmin))) * (fmax-fmin) / (fmid-fmin)]
	parms[, iparm := -log(1 - 0.8) / (0.5)]
	
	proj = data.table(irrig=seq(0, 1, 0.05))[, as.list(data.table(fert = seq(0, fmax, 50))), by=irrig]
	proj = parms[, as.list(proj), by=list(fmin, fmid, fmax, vmin, vmid, vmax, vmin_irrig, vmax_irrig, fparm, iparm, irrigMax)]
	proj[, value := vmin + 
					(vmax-vmin) * (1 - exp(-fparm*(fert-fmin)/(fmax-fmin))) +
					(vmin_irrig-vmin) * (1 - exp(-iparm*irrig)) +
					(vmax_irrig+vmin-vmax-vmin_irrig) * (1 - exp(-fparm*(fert-fmin)/(fmax-fmin))) * (1 - exp(-iparm*irrig))
	]
	proj[, irrigAmnt:=irrig * irrigMax]	
	proj2 = as.data.table(dcast(proj[, list(fert, irrig, value)], fert ~ irrig))
	proj2[, fert:=NULL]
	
	par(mar=c(1,1,0,0), cex = 1.5)
	persp(unique(proj$fert), unique(proj$irrigAmnt), 10*as.matrix(proj2), theta = -40, phi = 20, expand = 0.6,
			ltheta = 65, shade = 0.55, ticktype = "detailed",
			xlab = "Fertiliser rate (kg/ha)", ylab = "Irrigation rate (litre/m2)", zlab = "Yield (t/ha)", r = 10, zlim = range(0, max(proj2)*10))
	
	grid.text(gp=gpar(cex=1.7, font=4), x=unit(.06, "npc"), y=unit(.97, "npc"), just=c("left", "top"), title)
}


plotYieldSurfaceExample(lon=-3.75, lat=54.25, crop="CerealsC3", maxIrrigRate=109.006, "(a)") # should automatically look up irrigition data file for max rates
plotYieldSurfaceExample(lon=-95.75, lat=29.25, crop="CerealsC4", maxIrrigRate=650.107, "(b)") # but currently need to do manually

plot(rasterFromXYZ(allyields[fert==200, list(Lon, Lat, CerealsC3*1.077*10)], crs = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"),
		axes=FALSE, box=FALSE, legend.lab="")
plot(rasterFromXYZ(allyields[fert==0, list(Lon, Lat, CerealsC3*1.077*10)], crs = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"),
		axes=FALSE, box=FALSE, breaks=seq(0,8.0, 0.034), legend=FALSE)
#
plotComp = function(dt, fmin = 0, fmax = 1000) {
	
	parms = data.table(fmin, fmid=c(20, 50, 100, 200, 500, 1000), fmax, vmin = dt[fert==fmin, value], vmax=dt[fert==fmax, value], vmid= dt[fert %in% c(20, 50, 100, 200, 500, 1000), value])
	proj = data.table(fert = seq(5, 2000, 5))
	
#   Cobb-Douglas	
#	parm = log((v_mid-v_min)/(v_max-v_min)) / log((mid-min)/(max-min))	
#	parms[, parm := log((vmid-vmin)/(vmax-vmin)) / log((fmid-fmin)/(fmax-fmin))]
#	proj = parms[, as.list(proj), by=list(fmin, fmid, fmax, vmin, vmid, vmax, parm)]
#	proj[, value := v_min + (vmax-vmin) * (((fert-fmin)/(fmax-fmin)) ** parm)]
	#	proj[fert > max, value := v_max]
	
	parms[, parm := -log(1 - ((vmid-vmin)/(vmax-vmin))) * (fmax-fmin) / (fmid-fmin)]
	print(parms)
	proj = parms[, as.list(proj), by=list(fmin, fmid, fmax, vmin, vmid, vmax, parm)]
	proj[, value := vmin + (vmax-vmin) * (1 - exp(-parm*(fert-fmin)/(fmax-fmin)) )]
	
	ggplot() + geom_line(data=dt, aes(x=fert, y=value)) + 
			geom_line(data=proj[fmid == 20], aes(x=fert, y=value, colour="mid= 20")) + 
			geom_line(data= proj[fmid == 50], aes(x=fert, y=value, colour = 'mid= 50')) + 
			geom_line(data= proj[fmid == 100], aes(x=fert, y=value, colour = 'mid=100')) + 
			geom_line(data= proj[fmid == 200], aes(x=fert, y=value, colour = 'mid=200')) + 
			geom_line(data= proj[fmid == 500], aes(x=fert, y=value, colour = 'mid=500')) + 
#						scale_x_continuous(limits=c(0, 500)) +
			theme(text = element_text(size=9)) + theme(legend.title=element_blank()) + 
			theme(legend.position="bottom") + theme(legend.text=element_text(size=11))+
			labs(x="Fertiliser rate (kg/ha)", y="Yield (kg/m2") 
	
}

#plotComp(allyields[Lon == -90 & Lat == 45, list(fert, value=TeSWi])
#plotComp(allyields[TeSW > 0, list(value=mean(TeSW)), by=fert])

gsw = plotComp(merge(allyields[fert == 200 & CerealsC3>mean(CerealsC3), list(Lat, Lon)], allyields, by=c("Lat", "Lon"))[, list(value=mean(CerealsC3)), by=fert]) 
gww = plotComp(merge(allyields[fert == 200 & CerealsC3>mean(Oilcrops), list(Lat, Lon)], allyields, by=c("Lat", "Lon"))[, list(value=mean(Oilcrops)), by=fert]) + theme(legend.position="none")
gco = plotComp(merge(allyields[fert == 200 & CerealsC3>mean(CerealsC4), list(Lat, Lon)], allyields, by=c("Lat", "Lon"))[, list(value=mean(CerealsC4)), by=fert]) + theme(legend.position="none")
gri = plotComp(merge(allyields[fert == 200 & CerealsC3>mean(Rice), list(Lat, Lon)], allyields, by=c("Lat", "Lon"))[, list(value=mean(Rice)), by=fert]) + theme(legend.position="none")
g = ggplotGrob(gsw)$grobs
legend <- g[[which(sapply(g, function(x) x$name) == "guide-box")]]
gsw = gsw + theme(legend.position="none")

grid.arrange(gsw, textGrob("teSW"), gww, textGrob("teWW"), gco, textGrob("teCo"), gri, textGrob("trRi"), legend, ncol=2, widths=c(10, 1), heights=c(4,4,4,4,1))




lpjDir="/Users/peteralexander/Documents/LURG/LPJ/19Jan2017/LPJG_PLUM_expt1.1_1971-2005_forED"
#lpjDir="/exports/csce/eddie/geos/groups/LURG/models/PLUM/LPJ/old/LPJG_PLUM_expt1.1_1966-2005_ipsl_lessIO_forED"
files = list.files(lpjDir, pattern = 'anpp.out', full.names=TRUE, recursive=TRUE)

for (nppFile in files) {
	npps = fread(nppFile)
	npps[Lat > 55, PC3G := PC3G * (1 - 0.8*(Lat-55)/35)]
	npps[Lat > 55, PC4G := PC4G * (1 - 0.8*(Lat-55)/35)]
	write.table(npps, nppFile, row.names=FALSE, quote=FALSE)
}