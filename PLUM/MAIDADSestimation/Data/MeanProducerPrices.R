library(plyr)

load("RData/mappingsFAO.RData")
load("RData/ProducerPrices.RData")
load("RData/Production.RData")

# Calculate mean producer price weighted by production
ProdPrice <- merge(ProducerPrices,Production,
                   by = c("FAOST_CODE","FAO1","Year"),all.x = TRUE)
ProdPrice <- plyr::rename(ProdPrice,
               c("value.x" = "Price",
                 "value.y" = "Production"))
ProdPrice <- subset(ProdPrice,Year %in% 2000:2011)
ProdPrice <- merge(ProdPrice,mapFAO1ref2[,c("FAO1","FAO1Name")])

ProdPrice <- within(ProdPrice,{
  CorrectedYear <- ifelse(Year %in% 2000:2002,
                          2001,
                          ifelse(Year %in% 2003:2005,
                                 2004,
                                 ifelse(Year %in% 2006:2008,
                                        2007,
                                        ifelse(Year %in% 2009:2011,
                                               2010,
                                               NA))))
})

ProdPrice3Y <- ddply(ProdPrice,
                     .(CorrectedYear,FAOST_CODE,FAO1),
                     summarize,
                     wtdmean = Hmisc::wtd.mean(x = Price, weights = Production,na.rm = TRUE),
                     mean = mean(Price,na.rm = TRUE))
ProdPrice3Y <- within(ProdPrice3Y,{
  value <- ifelse(!is.na(wtdmean),wtdmean,mean)
  Year <- CorrectedYear
  rm("wtdmean","mean","CorrectedYear")
})

MeanProducerPrices <- ddply(ProdPrice,
                           .(CorrectedYear,FAO1),
                           summarize,
                           wtdmean = Hmisc::wtd.mean(x = Price, weights = Production,na.rm = TRUE),
                           mean = mean(Price,na.rm = TRUE))
MeanProducerPrices <- within(MeanProducerPrices,{
  value <- ifelse(!is.na(wtdmean),wtdmean,mean)
  Year <- CorrectedYear
  rm("wtdmean","mean","CorrectedYear")
})

## StatPrice <- ddply(subset(ProdPrice,Year == 2010),
##                    .(FAO1Name),
##                    summarize,
##                    FAO1 = unique(FAO1),
##                    mean = mean(Price, na.rm = TRUE),
##                    sd   = sd(x = Price,na.rm = TRUE),
##                    wtdmean = Hmisc::wtd.mean(x = Price, weights = Production,na.rm = TRUE),
##                    wtdsd   = sqrt(Hmisc::wtd.var(x = Price,weights = Production, normwt = TRUE)),
##                    Production = mean(Production,na.rm = TRUE),
##                    number = length(Price),
##                    min    = min(Price, na.rm = TRUE),
##                    max    = max(Price, na.rm = TRUE))

save("ProdPrice3Y","MeanProducerPrices", file = "RData/MeanProducerPrices.RData")
