# Import EconMap projections
# ==========================

library(FAOSTAT)
library(magrittr)
library(plyr)
library(readr)
library(reshape2)

source("functions.R")

Projections <- read_csv("SourceData/CEPII/EconMap_2.4_reference_2100.csv") %>%
    subset(select = c(code_wb,name,year,gdp_05,population)) %>%
    plyr::rename(c('year' = 'Year',
             'population' = 'Population',
             'gdp_05' = 'ConstantGDP')) %>%
    as.data.frame()

# Check for duplicates
anyDuplicated(Projections[,c("code_wb","Year")])

# Convert ISO3 to FAOSTAT code
Projections <- translateCountryCode(Projections,
                           from = "ISO3_WB_CODE",
                           to = "FAOST_CODE",
                           oldCode = "code_wb")

# Remove duplicated observations for Sudan
Projections <- subset(Projections,!(FAOST_CODE == 276))

# Check for duplicates and NA
anyDuplicated(Projections[,c("Year","ISO3_WB_CODE","FAOST_CODE")])
anyDuplicated(Projections[,c("Year","ISO3_WB_CODE")])
anyNA(Projections$FAOST_CODE)

Projections[,"ISO3_WB_CODE"] <- NULL
Projections[,"name"] <- NULL

Projections <- within(Projections,{
    Population <- Population*1000
    ConstantGDPperCap <- 1E6*ConstantGDP/Population
})

Projections <- arrange(Projections,FAOST_CODE,Year)

# Export population to gms
write.gms(x = list(index = Projections[,c("FAOST_CODE","Year")],
                   value = Projections[,"Population"]),
          file = "GAMS-input/Populations.gms")

# Calculate the growth rate of GDP per capita
GDPgr <- ddply(Projections,
               .(FAOST_CODE),
               summarize,
               Year = (min(Year)+1):max(Year),
               ConstantGDPperCapgr = exp(diff(log(ConstantGDPperCap))))
anyNA(GDPgr$ConstantGDPperCapgr)

CountriesEconMap <- unique(GDPgr$FAOST_CODE)

save(CountriesEconMap, file = "RData/CountriesEconMap.RData")
save(Projections,file = "RData/EconMapProjections.RData")

# Write to gms file
write.gms(x = list(index = GDPgr[,c("FAOST_CODE","Year")], value = GDPgr[,"ConstantGDPperCapgr"]),
          file = "GAMS-input/GDPperCapProjections.gms")

# SSPs
SSP1 <- read_csv("SourceData/CEPII/EconMap_2.4_ssp1.csv")
SSP2 <- read_csv("SourceData/CEPII/EconMap_2.4_ssp2.csv")
SSP3 <- read_csv("SourceData/CEPII/EconMap_2.4_ssp3.csv")
SSP4 <- read_csv("SourceData/CEPII/EconMap_2.4_ssp4.csv")
SSP5 <- read_csv("SourceData/CEPII/EconMap_2.4_ssp5.csv")

SSP <- rbind(cbind(SSP1,SSP = "SSP1"),
             cbind(SSP2,SSP = "SSP2"),
             cbind(SSP3,SSP = "SSP3"),
             cbind(SSP4,SSP = "SSP4"),
             cbind(SSP5,SSP = "SSP5"))
SSP <- plyr::rename(SSP,c("year" = "Year",
                    "gdp_05"="ConstantGDP",
                    "population"="Population"))
SSP <- subset(SSP,select = c(code_wb,name,Year,ConstantGDP,Population,SSP))
SSP <- within(SSP,{
    Population <- Population*1000
    ConstantGDPperCap <- 1E6*ConstantGDP/Population
    SSP <- as.character(SSP)
})

# Check for duplicates
anyDuplicated(SSP[,c("code_wb","Year","SSP")])

# Convert ISO3 to FAOSTAT code
SSP <- translateCountryCode(SSP,
                            from = "ISO3_WB_CODE",
                            to = "FAOST_CODE",
                            oldCode = "code_wb")

# Remove duplicated observations for Sudan
SSP <- subset(SSP,!(FAOST_CODE == 276))

# Check for duplicates and NA
anyDuplicated(SSP[,c("Year","ISO3_WB_CODE","FAOST_CODE","SSP")])
anyDuplicated(SSP[,c("Year","ISO3_WB_CODE","SSP")])
anyNA(SSP$FAOST_CODE)

SSP[,"ISO3_WB_CODE"] <- NULL
SSP[,"name"] <- NULL

# Calculate the growth rate of GDP per capita
SSP <- arrange(SSP,SSP,FAOST_CODE,Year)
SSP.GDPgr <- ddply(SSP,
                   .(SSP,FAOST_CODE),
                   summarize,
                   Year = (min(Year)+1):max(Year),
                   ConstantGDPperCapgr = exp(diff(log(ConstantGDPperCap))))
anyNA(SSP.GDPgr[,"ConstantGDPperCapgr"])

SSP.CountriesEconMap <- unique(SSP.GDPgr$FAOST_CODE)

save(SSP,file = "RData/EconMapSSPProjections.RData")

# Write to gms file
write.gms(x = list(index = SSP.GDPgr[,c("SSP","FAOST_CODE","Year")],
                   value = SSP.GDPgr[,"ConstantGDPperCapgr"]),
          file = "GAMS-input/SSPGDPperCapProjections.gms")

write.gms(x = list(index = SSP[,c("SSP","FAOST_CODE","Year")],
                   value = SSP[,"Population"]),
          file = "GAMS-input/SSPPopulations.gms")

