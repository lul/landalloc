library(readr)
library(plyr)

datadir <- "SourceData/FAO-Bulk"

products <- c("Crops","CropsProcessed","LivestockPrimary","LivestockProcessed")
Production <- list()

for (iproducts in products) {
  unzip(paste(datadir,"faostat.zip",sep = "/"),
        paste(paste("Production_",iproducts,"_E_All_Data.zip",sep = "")),
        exdir = datadir)
  Production[[iproducts]] <- read_csv(paste(datadir,"/Production_",iproducts,"_E_All_Data.zip",sep = ""))
  file.remove(paste(datadir,"/Production_",iproducts,"_E_All_Data.zip",sep = ""))
}

Production <- rbind(Production[[1]],Production[[2]],Production[[3]],Production[[4]])

Production <- subset(Production,
                     ElementCode == 5510 & !(CountryCode %in% c(351,357)),
                     select = c(CountryCode,ItemCode,Year,Value))
Production <- rename(Production,
                     c("CountryCode" = "FAOST_CODE",
                       "ItemCode" = "FAO1",
                       "Value" = "value"))

# Corrections for rice
Production <- within(Production,{
  i <- FAO1 == 27
  value[i] <- value[i]*0.667
  FAO1[i] <- as.integer(30)
  i <- NULL
})

save("Production",file = "RData/Production.RData")
