# AggregatePrices - Aggregate prices from FAO1 to FAO2 level

library(plyr)
load("RData/PricesFAO1.RData")
load("RData/MeanPrices.RData")
load("RData/mappingsFAO.RData")

load("RData/TradeUVMM.RData")
Trade <- subset(TradePrices,Year %in% c(2001,2004,2007,2010))
Trade <- transform(Trade,tradevol = trade/value)

Trade <- merge(Trade,
               unique(mapFAO1ref2[,c("FAO1Ref","FAO2")]),
               by.x = "FAO1", by.y = "FAO1Ref")


# Aggregation of commodities that exist in subgroups

## Calculate weights
# Commodities are aggregated by weighting the unit values.
# Weights are defined by FAO2 entries and by year as the sum of trade volumes
# over all countries divided by the sum over all countries and FAO1 sectors in
# the FAO2 entry.
AgSectorCountry <- ddply(Trade,
                         .(FAO2,Year),
                         summarize,
                         SumSectorCountry = sum(tradevol,na.rm = TRUE))
AgCountry <- ddply(Trade,
                   .(FAO1,FAO2,Year),
                   summarize,
                   SumCountry = sum(tradevol,na.rm = TRUE))
Weights <- merge(AgSectorCountry, AgCountry, by = c("FAO2","Year"))
Weights <- within(Weights,{
  weights <- SumCountry/SumSectorCountry
  rm("SumCountry","SumSectorCountry")})
Weights <- merge(Weights[,c("FAO1","Year","weights")],
                 unique(mapFAO1ref2[,c("FAO1Ref","FAO2")]),
                 by.x = "FAO1", by.y = "FAO1Ref", all = TRUE)
# Add mapping for sugar-cane which is non-tradable
Weights <- rbind(Weights,
                 data.frame(FAO1 = 156,
                            Year = c(2001,2004,2007,2010),
                            weights = 1,
                            FAO2 = 2536))
saveRDS(Weights,file = "RData/WeightsFAO1.rds")

print("NA weights for products that are not traded (or fish treated elsewhere)")
unique(merge(subset(Weights,is.na(weights),select = c('FAO1','FAO2')),
             unique(mapFAO1ref2[,c("FAO1Ref","FAO2","FAO2Name","FAO1Name")]),
               by.x = c("FAO1","FAO2"), by.y = c("FAO1Ref","FAO2")))

rm('AgCountry','AgSectorCountry','Trade','TradePrices')

## Aggregate prices (country-level)
PricesFAO1 <- merge(subset(PricesFAO1,Year %in% c(2001,2004,2007,2010)),
                    Weights[,c("FAO1","Year","weights")],
                    all.x = TRUE, by = c("FAO1","Year"))
PricesFAO1 <- merge(PricesFAO1,mapFAO1ref2,by = "FAO1",all.x = TRUE)

### Set to 1 all weights of mapping to a single product
for (iFAO2 in unique(mapFAO1ref2$FAO2)) {
  if (dim(unique(subset(mapFAO1ref2,FAO2 == iFAO2, select = FAO1Ref)))[1] == 1) {
    PricesFAO1[PricesFAO1$FAO2 == iFAO2,"weights"] <- 1}}

### Check na weights
sum(is.na(PricesFAO1$weights))

### Aggregate unit values
PricesFAO2 <- ddply(PricesFAO1,
                    .(FAO2,FAOST_CODE,Year),
                    summarize,
                    value = sum(weights*value, na.rm = TRUE))

## Check na weights
sum(is.na(PricesFAO2$value))
### Check 0 values
sum(PricesFAO2$value == 0)

## Aggregate prices (world-level)
MeanPrices <- merge(MeanPrices,
                    Weights[,c("FAO1","Year","weights")],
                    all.x = TRUE, by = c("FAO1","Year"))
MeanPrices <- merge(MeanPrices,mapFAO1ref2,by = "FAO1",all.x = TRUE)

### Set to 1 all weights of mapping to a single product
for (iFAO2 in unique(mapFAO1ref2$FAO2)) {
  if (dim(unique(subset(mapFAO1ref2,FAO2 == iFAO2, select = FAO1Ref)))[1] == 1) {
    MeanPrices[MeanPrices$FAO2 == iFAO2,"weights"] <- 1}}

### Check na weights
sum(is.na(MeanPrices$weights))

### Aggregate unit values
MeanPricesFAO2 <- ddply(MeanPrices,
                        .(FAO2,Year),
                        summarize,
                        value = sum(weights*value, na.rm = TRUE))

### Check na weights
sum(is.na(MeanPricesFAO2$value))
### Check 0 values
sum(MeanPricesFAO2$value == 0)

save("PricesFAO2","MeanPricesFAO2", file = "RData/PricesFAO2.RData")
