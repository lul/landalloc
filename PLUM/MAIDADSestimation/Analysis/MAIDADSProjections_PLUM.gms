$if not set BS $set BS 0
$if not set Scenario $set Scenario Full
$if not set SSP $set SSP 0

* Least-square regression to initialize the level of utility based on per-capita income *
* Variable to determine: Intercept and Slope that are used later to initialize the
* utility for countries absent from estimation
VARIABLE
  Intercept
  Slope
  Obj
;

EQUATION SumSqr;

SumSqr.. Obj =e= sum(c$m(c),sqr(u(c)-Intercept-Slope*log10(m(c))));

MODEL FindU / SumSqr /;

u.FX(c) = u.L(c);
solve FindU minimizing Obj using qcp;
u.lo(c) = -inf;  u.up(c)      = inf ;
*---------------------------------------------------------------------------------------*

SET
  t           "Time index"    / 1980*2100 /
  tproj(t)    "Time index for projection" / 2011*2100 /
  cproj(c)    "Countries for projection"  /
$include ../Data/GAMS-input/CountriesProjection.gms
/
  cnotproj(c) "Countries that are not projected"
  decomp      "Results decompostion" / Total, Pop2010, GDPperCap2010 /
  SSP         "Shared Socioeconomic Pathways"
  / SSP1 Sustainability
    SSP2 Middle of the Road
    SSP3 Fragmentation
    SSP4 Inequality
    SSP5 Conventional Development
    /
;

cnotproj(c)     = yes;
cnotproj(cproj) = no;

PARAMETER
  ICPmodel(c,*)                     "Intercept and slop of the model predicting nonfood price" /
$include ../Data/GAMS-input/ICPmodel.gms
/
  pnonfoodtest(c)                   "Test for non-food price"
  pworld(i)                         "World price" /
$include ../Data/GAMS-input/MeanPrices2010.gms
            /
  Calories_i(decomp,i,t)            "Sectoral calories decomposition by scenario"
  CaloriesPerCap_i(i,t)             "Sectoral calories per capita"
  CaloriesPerCap_c(c,t)             "Country calories per capita"
  CaloriesPerCap_type(type,t)       "Calories per capita per type"
  CaloriesPerCap(t)                 "World calories per capita"
  Population(c,t)                   "Population projection" /
$include ../Data/GAMS-input/Populations.gms
            /
  SSPPopulation(SSP,c,t)            "Population projection" /
$include ../Data/GAMS-input/SSPPopulations.gms
            /
  GDPperCapgr(c,t)                  "Growth rate of the constant GDP per capita from EconMap (CEPII)" /
$include ../Data/GAMS-input/GDPperCapProjections.gms
            /
  SSPGDPperCapgr(SSP,c,t)           "Growth rate of the constant GDP per capita from EconMap (CEPII)" /
$include ../Data/GAMS-input/SSPGDPperCapProjections.gms
            /
  TotalCalories(decomp,t)           "Total calories decomposition by scenario"
  TotalCalories_type(decomp,type,t) "Calories decomposition by scenario per type"
  TotalPopulation(t)                "World population"
  m0(c)                             "GDP per capita" /
$include ../Data/GAMS-input/GDPperCap2010.gms
/
  xsim(c,i,t)                       "Predicted consumption"
  testcountry(c)                    "Test if countries can cover subsistence consumption"
  GrowthCalories(decomp,*)          "Percent growth between 2010 and 2050"
  TotalCal(*)                       "Total observed and predicted calories in 2010"
;

$if not %SSP%==0 GDPperCapgr(c,t) = sum(SSP$(ord(SSP)=%SSP%),SSPGDPperCapgr(SSP,c,t));
$if not %SSP%==0 Population(c,t)  = sum(SSP$(ord(SSP)=%SSP%),SSPPopulation(SSP,c,t));

TotalPopulation(t) = sum(c$cproj(c),Population(c,t));

$if %Scenario%=="Income" Population(c,t) = Population(c,"2010");
$if %Scenario%=="Population" GDPperCapgr(c,t) = 1;

pnonfoodtest(c)$m(c) = ICPmodel(c,"constant") + ICPmodel(c,"slope")*m(c)/m("231");
loop(c$m(c),
  abort$(abs(p(c,"5000")-pnonfoodtest(c))>=1E-2) "Predicted nonfood price does not match observed price", pnonfoodtest, p;);

p(c,i)$(not sameas(i,"5000")) = pworld(i);
p(c,"5000") = pmean("5000");

* Check predicted calories agains observed calories
TotalCal("0") = sum(c$m(c),Population(c,"2010")*TotalCalories0(c));
TotalCal("1") = sum(c$m(c),Population(c,"2010")*TotalCalorieshat(c));
display TotalCal;
*

$if %BS%==0 $goto Benchmark
$gdxin ./GDX/BootstrapedMAIDADS0.gdx
SET iB;
PARAMETER
  alphaB(iB,i)
  betaB(iB,i)
  deltaB(iB,i)
  tauB(iB,i)
  omegaB(iB)
  kappaB(iB)
;
$load iB, alphaB, betaB, deltaB, tauB, omegaB, kappaB

SINGLETON SET iBsel(iB);
iBsel(iB) = yes$(ord(iB)=%BS%);

alpha.L(i) = alphaB(iBsel,i);
beta.L(i)  = betaB(iBsel,i);
delta.L(i) = deltaB(iBsel,i);
tau.L(i)   = tauB(iBsel,i);
omega.L    = omegaB(iBsel);
kappa.L    = kappaB(iBsel);

abort$(kappa.L>=-1E-6) "Wrong bootstrap replica";
$label Benchmark

alpha.FX(i) = alpha.L(i);
beta.FX(i)  = beta.L(i);
delta.FX(i) = delta.L(i);
tau.FX(i)   = tau.L(i);
omega.FX    = omega.L;
kappa.FX    = kappa.L;

xsim(c,i,"2010") = xhat(c,i);

m0(cnotproj) = 0;
*m0(c) = m(c);

testcountry(c)$m0(c) = (m0(c)-sum(i,p(c,i)*delta.l(i)))<=0;
m0(c)$testcountry(c) = 0;

*loop(l$m0(l),
  m(c) = 0;
  m(l) = m0(l);

* Initialization of countries not included in the estimation
  u.L(l)$(u.L(l)=0 and m(l)) = Intercept.L+Slope.L*log10(m(l));
  SubsistenceC.L(l,j)$m(l) = (delta.L(j)+tau.L(j)*exp(omega.L*u.L(l)))/(1+exp(omega.L*u.L(l)));
  DiscretionaryC.L(l,i)$m(l) = max(0,[(alpha.L(i)+beta.L(i)*exp(u.L(l)))/(1+exp(u.L(l)))]/p(l,i)*
    [m(l)-sum(j,p(l,j)*SubsistenceC.L(l,j))]);
  what.L(l,j)$m(l) = (SubsistenceC.L(l,j)+DiscretionaryC.L(l,j))*p(l,j)/m(l);

  MAIDADS_Simulation.holdfixed = 1;
  solve MAIDADS_Simulation using mcp;
  xsim(c,i,"2010")$m(c) = what.L(c,i)*m(c)/p(c,i);
  loop(t$tproj(t),
    m(l)$m(l) = m(l)*GDPperCapgr(l,t);
    p(l,"5000")$m(l) = ICPmodel(l,"constant") + ICPmodel(l,"slope")*m(l)/m("231");
    solve MAIDADS_Simulation using mcp;
    xsim(c,i,t)$m(c) = what.L(c,i)*m(c)/p(c,i);
* For a cns solver
*    abort$(MAIDADS_Simulation.ModelStat ne 16 or MAIDADS_Simulation.SolveStat ne 1) "Model failed to solve";
* For an mcp solver
    abort$(MAIDADS_Simulation.ModelStat ne 1 or MAIDADS_Simulation.SolveStat ne 1) "Model failed to solve";
    abort$(MAIDADS_Simulation.numredef ne 0) "Bounded solution";
  );
*);
CaloriesPerCap_c(c,t)     = sum(ifood,xsim(c,ifood,t))*2000;
CaloriesPerCap_i(ifood,t) = sum(c$m0(c),xsim(c,ifood,t)*Population(c,t))*2000/sum(c$m0(c),Population(c,t));
CaloriesPerCap_type(type,t) = sum((c,ifood)$m0(c),CoefType(c,ifood,type)*xsim(c,ifood,t)*Population(c,t))*2000/sum(c$m0(c),Population(c,t));
Calories_i("Total",ifood,t)         = sum(c$m0(c),xsim(c,ifood,t)*Population(c,t))*2000*365;
Calories_i("Pop2010",ifood,t)       = sum(c$m0(c),xsim(c,ifood,t)*Population(c,"2010"))*2000*365;
Calories_i("GDPperCap2010",ifood,t) = sum(c$m0(c),xsim(c,ifood,"2010")*Population(c,t))*2000*365;
TotalCalories("Total",t)   = sum(c$m0(c),CaloriesPerCap_c(c,t)*Population(c,t))*365;
TotalCalories("Pop2010",t) = sum(c$m0(c),CaloriesPerCap_c(c,t)*Population(c,"2010"))*365;
TotalCalories("GDPperCap2010",t) = sum(c$m0(c),CaloriesPerCap_c(c,"2010")*Population(c,t))*365;
CaloriesPerCap(t)         = TotalCalories("Total",t)/(sum(c$m0(c),Population(c,t))*365);
TotalCalories_type("Total",type,t)         = sum((c,ifood)$m0(c),CoefType(c,ifood,type)*xsim(c,ifood,t)*Population(c,t))*2000*365;
TotalCalories_type("Pop2010",type,t)       = sum((c,ifood)$m0(c),CoefType(c,ifood,type)*xsim(c,ifood,t)*Population(c,"2010"))*2000*365;
TotalCalories_type("GDPperCap2010",type,t) = sum((c,ifood)$m0(c),CoefType(c,ifood,type)*xsim(c,ifood,"2010")*Population(c,t))*2000*365;

GrowthCalories(decomp,ifood)   = (Calories_i(decomp,ifood,"2050")/Calories_i(decomp,ifood,"2010")-1)*100;
GrowthCalories(decomp,type)    = (TotalCalories_type(decomp,type,"2050")/TotalCalories_type(decomp,type,"2010")-1)*100;
GrowthCalories(decomp,"Total") = (TotalCalories(decomp,"2050")/TotalCalories(decomp,"2010")-1)*100;


* Results exportation to a csv file
$if not %SSP%==0 file Results / "GDX/ProjectionsBSSSP%SSP%.csv" /;
$if %SSP%==0 file Results / "GDX/ProjectionsBS.csv" /;
$if not %BS%==0 Results.ap = 1;
Results.lw = 20;
put Results;
$if %BS%==0 put "itBS;Decomp;Bundle;Growth" /;
loop(decomp,
  loop(ifood,
    put %BS%:4:0 ";" decomp.tl ";" i.te(ifood) ";" GrowthCalories(decomp,ifood):11:10 /;
  );
  loop(type,
    put %BS%:4:0 ";" decomp.tl ";" type.te(type) ";" GrowthCalories(decomp,type):11:10 /;
  );
  put %BS%:4:0 ";" decomp.tl ";All food;" GrowthCalories(decomp,"Total"):11:10 /;
);
putclose Results;
