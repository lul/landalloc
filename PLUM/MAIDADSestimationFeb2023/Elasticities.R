library(gdxrrw); igdx(sub(";.*$", "", Sys.getenv("GAMSDIR")))
library(magrittr)
library(plyr)
library(reshape2)

setwd("C:/Users/fcossar/Documents/Food_demand_modelling/Gouel_MAIDADS/GlobalFoodDemand-Programs")

miter <- gdxrrw::rgdx.param("GDX/MAIDADS_Simulation0_PLUM.gdx",symName="miter",names = c("iter","GDPpercap"))
eta <- gdxrrw::rgdx.param("GDX/MAIDADS_Simulation0_PLUM.gdx",symName="eta",names = c("iter","i","eta"))
etakcal <- gdxrrw::rgdx.param("GDX/MAIDADS_Simulation0_PLUM.gdx",symName="etakcal",names = c("iter","eta"))
etatype <- gdxrrw::rgdx.param("GDX/MAIDADS_Simulation0_PLUM.gdx",symName="eta_Type",names = c(".te","iter","eta"))
i <- gdxrrw::rgdx.set("GDX/MAIDADS_Simulation0_PLUM.gdx",symName="i",te = TRUE)

levels(etatype$.te) <- c("Vegetal-based","Animal-based")

mselect <- c(5E2,1E3,5E3,1E4,5E4)

data <- rbind(
    merge(miter,eta) %>%
      subset(GDPpercap %in% mselect) %>%
      arrange(GDPpercap) %>%
      merge(i) %>%
      dcast(.te ~ GDPpercap,value.var = "eta"),
    etakcal %>%
      merge(miter) %>%
      subset(GDPpercap %in% mselect) %>%
      arrange(GDPpercap) %>%
      transform(.te = "All food") %>%
      dcast(.te ~ GDPpercap,value.var = "eta"),
    etatype %>%
      merge(miter) %>%
      subset(GDPpercap %in% mselect) %>%
      arrange(GDPpercap) %>%
      dcast(.te ~ GDPpercap,value.var = "eta"))


formatNum <- function(x) formatC(x,digits = 2,format = "f") %>% ifelse(!(. %in% c("0.00","-0.00")),.,"0")

data[c(1,7,6,8,5,3,2,10,11,9,4),] %>%
    transform(`500` = formatNum(`500`),
              `1000` = formatNum(`1000`),
              `5000` = formatNum(`5000`),
              `10000` = formatNum(`10000`),
              `50000` = formatNum(`50000`)) %>%
    write.table(file = "../Article/Tables/Elasticities.tex",
                quote = FALSE,
                sep = "&",
                eol = "\\\\\n",
                row.names = FALSE,
                col.names = FALSE)
