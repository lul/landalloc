library(plyr)
library(reshape2)


load("RData/CountryList.RData")
load("RData/FBS.RData")
load("RData/Fish.RData")
load("RData/mappingsFAO.RData")
load("RData/PricesFAO2.RData")
load("RData/MeanPrices.RData")

MeanPricesFAO2 <- rbind(plyr::rename(MeanPrices,c("value" = "Price")),  ##price is USD/tonne
                        MeanFishPrices)

countries <- read.delim("mappings/countries.txt",
                        col.names = c("FAOST_CODE","CountryName"),header = FALSE)
PotentialCountries <- subset(countries,
                             !(FAOST_CODE %in% unique(c(NoFBS$FAOST_CODE,NoPopulation$FAOST_CODE))))

FBS <- subset(FBS,
              Year %in% c(2010,2013,2016,2019) & FAOST_CODE %in% PotentialCountries$FAOST_CODE)
FBS <- reshape2::dcast(FBS,
             FAOST_CODE + Year + FAO2 ~ variable)

TotalFBS <- ddply(FBS,
                  .(Year,FAO2),
                  summarize,
                  TotalFood = sum(Food,na.rm = TRUE),
                  kcal = sum((Food*1000)*kcalperg,na.rm = TRUE))

FishFAO2Code <- c(2762,2761,2764,2763,2768,2769,2775)
TotalFBSFish <- ddply(subset(TotalFBS,FAO2 %in% FishFAO2Code),
                      .(Year),
                      summarize,
                      TotalFood = sum(TotalFood,na.rm = TRUE),
                      kcal = sum(kcal,na.rm = TRUE),
                      FAO2 = 5000)

TotalFBS <- rbind(subset(TotalFBS,!(FAO2 %in% FishFAO2Code)),
                  TotalFBSFish)

MeandataFAO2 <- merge(MeanPricesFAO2,TotalFBS, all.y = TRUE)
MeandataFAO2 <- merge(MeandataFAO2,
                      unique(mapFoAg[,c("FoAg","FoAgName","FAO2")]),by = "FAO2")
MeandataFAO2 <- subset(MeandataFAO2,FoAgName != "nonfood")
MeandataFAO2 <- subset(MeandataFAO2,!is.na(Price))

MeanPriceFoAg <- ddply(MeandataFAO2,
                       .(Year,FoAgName),
                       summarize,
                       Priceperkcal = sum(TotalFood*(Price/1000),na.rm = FALSE)*2000*365/(sum(kcal, na.rm = FALSE)))
MeanKcalperg <- ddply(MeandataFAO2,
                       .(Year,FoAgName),
                       summarize,
                       Kcalperg = sum(kcal, na.rm = FALSE)/sum(TotalFood*1000,na.rm = FALSE))
MeanKcalperg <- subset(MeanKcalperg, subset=Year==2019)
write.csv(MeanKcalperg,"meanKcalperg.csv")

save("MeanPriceFoAg", file = "RData/MeanPriceFoAg.RData")
