FoAg	FoAgName	FAO2	FAO2Name	Type	FAO2Fish
2905	Cereals, roots, and tubers	2511	Wheat and products	Vegetal	2511
2905	Cereals, roots, and tubers	2805	Rice (Milled Equivalent)	Vegetal	2805
2905	Cereals, roots, and tubers	2513	Barley and products	Vegetal	2513
2905	Cereals, roots, and tubers	2514	Maize and products	Vegetal	2514
2905	Cereals, roots, and tubers	2515	Rye and products	Vegetal	2515
2905	Cereals, roots, and tubers	2516	Oats	Vegetal	2516
2905	Cereals, roots, and tubers	2517	Millet and products	Vegetal	2517
2905	Cereals, roots, and tubers	2518	Sorghum and products	Vegetal	2518
2905	Cereals, roots, and tubers	2520	Cereals, Other	Vegetal	2520
2905	Cereals, roots, and tubers	2531	Potatoes and products	Vegetal	2531
2905	Cereals, roots, and tubers	2533	Sweet Potatoes	Vegetal	2533
2905	Cereals, roots, and tubers	2532	Cassava and products	Vegetal	2532
2905	Cereals, roots, and tubers	2534	Roots, Other	Vegetal	2534
2905	Cereals, roots, and tubers	2535	Yams	Vegetal	2535
2909	Sugar, sugar crops, and sweeteners	2537	Sugar Beet	Vegetal	2537
2909	Sugar, sugar crops, and sweeteners	2536	Sugar Cane	Vegetal	2536
2909	Sugar, sugar crops, and sweeteners	2542	Sugar (Raw Equivalent)	Vegetal	2542
2909	Sugar, sugar crops, and sweeteners	2541	Sugar, Non-Centrifugal	Vegetal	2541
2909	Sugar, sugar crops, and sweeteners	2745	Honey	Vegetal	2745
2909	Sugar, sugar crops, and sweeteners	2543	Sweeteners, Other	Vegetal	2543
2907	Pulses, nuts, and oilcrops	2546	Beans	Vegetal	2546
2907	Pulses, nuts, and oilcrops	2547	Peas	Vegetal	2547
2907	Pulses, nuts, and oilcrops	2549	Pulses, Other and products	Vegetal	2549
2907	Pulses, nuts, and oilcrops	2551	Nuts and products	Vegetal	2551
2907	Pulses, nuts, and oilcrops	2555	Soyabeans	Vegetal	2555
2907	Pulses, nuts, and oilcrops	2556	Groundnuts (Shelled Eq)	Vegetal	2556
2907	Pulses, nuts, and oilcrops	2557	Sunflowerseed	Vegetal	2557
2907	Pulses, nuts, and oilcrops	2558	Rape and Mustardseed	Vegetal	2558
2907	Pulses, nuts, and oilcrops	2559	Cottonseed	Vegetal	2559
2907	Pulses, nuts, and oilcrops	2560	Coconuts - Incl Copra	Vegetal	2560
2907	Pulses, nuts, and oilcrops	2561	Sesameseed	Vegetal	2561
2907	Pulses, nuts, and oilcrops	2562	Palm kernels	Vegetal	2562
2907	Pulses, nuts, and oilcrops	2563	Olives (including preserved)	Vegetal	2563
2907	Pulses, nuts, and oilcrops	2570	Oilcrops, Other	Vegetal	2570
2914	Oils and fats	2578	Coconut Oil	Vegetal	2578
2914	Oils and fats	2575	Cottonseed Oil	Vegetal	2575
2914	Oils and fats	2572	Groundnut Oil	Vegetal	2572
2914	Oils and fats	2582	Maize Germ Oil	Vegetal	2582
2914	Oils and fats	2586	Oilcrops Oil, Other	Vegetal	2586
2914	Oils and fats	2580	Olive Oil	Vegetal	2580
2914	Oils and fats	2577	Palm Oil	Vegetal	2577
2914	Oils and fats	2576	Palmkernel Oil	Vegetal	2576
2914	Oils and fats	2574	Rape and Mustard Oil	Vegetal	2574
2914	Oils and fats	2581	Ricebran Oil	Vegetal	2581
2914	Oils and fats	2579	Sesameseed Oil	Vegetal	2579
2914	Oils and fats	2571	Soyabean Oil	Vegetal	2571
2914	Oils and fats	2573	Sunflowerseed Oil	Vegetal	2573
2918	Vegetables and fruits	2601	Tomatoes and products	Vegetal	2601
2918	Vegetables and fruits	2605	Vegetables, Other	Vegetal	2605
2918	Vegetables and fruits	2602	Onions	Vegetal	2602
2918	Vegetables and fruits	2611	Oranges, Mandarines	Vegetal	2611
2918	Vegetables and fruits	2612	Lemons, Limes and products	Vegetal	2612
2918	Vegetables and fruits	2613	Grapefruit and products	Vegetal	2613
2918	Vegetables and fruits	2620	Grapes and products (excl wine)	Vegetal	2620
2918	Vegetables and fruits	2625	Fruits, Other	Vegetal	2625
2918	Vegetables and fruits	2614	Citrus, Other	Vegetal	2614
2918	Vegetables and fruits	2617	Apples and products	Vegetal	2617
2918	Vegetables and fruits	2615	Bananas	Vegetal	2615
2918	Vegetables and fruits	2618	Pineapples and products	Vegetal	2618
2918	Vegetables and fruits	2616	Plantains	Vegetal	2616
2918	Vegetables and fruits	2619	Dates	Vegetal	2619
5000	Non-food	2630	Coffee and products	Vegetal	2630
5000	Non-food	2633	Cocoa Beans and products	Vegetal	2633
5000	Non-food	2635	Tea (including mate)	Vegetal	2635
5000	Non-food	2640	Pepper	Vegetal	2640
5000	Non-food	2641	Pimento	Vegetal	2641
5000	Non-food	2642	Cloves	Vegetal	2642
5000	Non-food	2645	Spices, Other	Vegetal	2645
5000	Non-food	2655	Wine	Vegetal	2655
5000	Non-food	2656	Beer	Vegetal	2656
5000	Non-food	2658	Beverages, Alcoholic	Vegetal	2658
5000	Non-food	2657	Beverages, Fermented	Vegetal	2657
5000	Non-food	2659	Alcohol, Non-Food	Vegetal	2659
2943	Meat and seafood	2731	Bovine Meat	Animal	2731
2943	Meat and seafood	2735	Meat, Other	Animal	2735
2943	Meat and seafood	2732	Mutton & Goat Meat	Animal	2732
2943	Meat and seafood	2733	Pigmeat	Animal	2733
2943	Meat and seafood	2734	Poultry Meat	Animal	2734
2948	Dairy and eggs	2848	Milk - Excluding Butter	Animal	2848
2948	Dairy and eggs	2744	Eggs	Animal	2744
2943	Meat and seafood	2766	Cephalopods	Animal	2766
2943	Meat and seafood	2765	Crustaceans	Animal	2765
2943	Meat and seafood	5000	Demersal Fish	Animal	2762
2943	Meat and seafood	5000	Freshwater Fish	Animal	2761
2943	Meat and seafood	5000	Marine Fish, Other	Animal	2764
2943	Meat and seafood	2767	Molluscs, Other	Animal	2767
2943	Meat and seafood	5000	Pelagic Fish	Animal	2763
2914	Oils and fats	2740	Butter, Ghee	Animal	2740
2914	Oils and fats	2743	Cream	Animal	2743
2914	Oils and fats	2737	Fats, Animals, Raw	Animal	2737
2914	Oils and fats	2781	Fish, Body Oil	Animal	2781
2914	Oils and fats	2782	Fish, Liver Oil	Animal	2782
2943	Meat and seafood	2736	Offals, Edible	Animal	2736
2943	Meat and seafood	5000	Meat, Aquatic Mammals	Animal	2768
2943	Meat and seafood	5000	Aquatic Animals, Others	Animal	2769
2943	Meat and seafood	5000	Aquatic Plants	Vegetal	2775
5000	Non-food	2680	Infant food	Vegetal	2680
5000	Non-food	2899	Miscellaneous	Vegetal	2899
