```{r Initialize, echo=FALSE}
library(FAOSTAT)
library(reshape2)

source("functions.R")
```


Download and format producer prices from FAOSTAT
================================================

```{r ImportData}
ProducerPrices <- read_faostat_bulk("SourceData/FAO-Bulk/Prices_E_All_Data_(Normalized).zip")

ProducerPrices <- subset(ProducerPrices,
                         element_code == 5532 & area_code < 5000,
                         select = -c(area,item,element,element_code,area_code__m49_,item_code__cpc_,unit,flag,months_code))
ProducerPrices <- plyr::rename(ProducerPrices,
                         c("area_code" = "FAOST_CODE",
                           "value" = "value",
                           "item_code" = "FAO1"))

# Keep only separated countries from China
ProducerPrices <- subset(ProducerPrices, !(FAOST_CODE %in% c(351,357)))

ProducerPrices <- na.omit(ProducerPrices)

CountriesWithPriceData <- unique(ProducerPrices$FAOST_CODE[ProducerPrices$year=="2019"])
print("Countries with price data for 2019")
subset(FAOcountryProfile,FAOST_CODE %in% CountriesWithPriceData,select = c(FAOST_CODE,FAO_TABLE_NAME))

print("Countries without price data for 2019")
subset(FAOcountryProfile,!(FAOST_CODE %in% CountriesWithPriceData),select = c(FAOST_CODE,FAO_TABLE_NAME))

if (anyDuplicated(subset(ProducerPrices,select = c("FAOST_CODE","FAO1","year"))) != 0)
    stop('Duplicated entries in ProducerPrices')
```


Preprocess data to make them compatible between sources
=======================================================

Sugar
-----

Sugar at the FAO2 level exists under 4 forms:

- 2541 - Sugar non-centrifugal: a non-tradable form, only consumed in sugar producing countries. So for the price, it would be better to use the producer price, but this is not available. Instead, we use the trade unit value corresponding to the HS6 170111	(Raw cane sugar, in solid form).
- 2542/2818 - Sugar (Raw Equivalent)/Sugar, Refined Equiv: The coefficient of transformation from raw eq. to refined eq. is 0.92. We should use the unit values of refined sugar (FAO1: 164), but not producer prices.
    - 2542 has been removed converted to refined eq.in FBS data
    - In the FAO1 to FAO2 mapping, refined sugar has been considered the reference everywhere and 2818 has been substituted for 2542.
- 2543 - Sweeteners, Other: OK
- 2745 - Honey: OK

Rice (FAO1={27,30})
-------------------

- In FAOSTAT Food Balance, rice consumption is expressed in milled equivalent and the conversion factor from paddy to milled is 0.667.
- For producers, rice is expressed in paddy, so producer prices have to be converted from paddy to milled equivalent by dividing them by 0.667.
- In FAOSTAT trade data, most rice trade takes place in products *30 Rice – total (Rice milled equivalent)*, but this code does not exist in other classifications. So the code has been added to the mapping FAO1 to FAO2 for rice, and to make prices comparable, the rice code for producers has to be changed from *27 Rice, paddy* to 30.

```{r CorrectRice}
ProducerPrices <- within(ProducerPrices,{
  i <- FAO1 == 27
  value[i] <- value[i]/0.667
  FAO1[i] <- as.integer(30)
  i <- NULL
})
```

Groundnuts
----------

Groundnuts exist in 2 versions in FAOSTAT: with shell and shelled. Producer data (production and price) are with shell. In FBS, consumption of groundnuts are in *shelled eq.* Based on the comparison of production and consumption data, it seems to be with shell.

Groundnuts are mostly traded as shelled, so unit values are for shelled version, while producer prices are for with shell.

Conversion rates are available in FAOSTAT commodity list.

The conversion rate from *Groundnut with shell* to *Groundnut shelled* appears to be 0.7.

One changes is done: Convert producer price from with shell (FAO1=242) to shelled (FAO1=243) by dividing it by 0.7, and change the code from 242 to 243

```{r CorrectGroundnuts}
ProducerPrices <- within(ProducerPrices,{
  i <- FAO1 == 242
  value[i] <- value[i]/0.7
  FAO1[i] <- as.integer(243)
  i <- NULL
})
```

Milk
----

For milk products, we will use for trade unit values the most traded dairy products: whole dry milk. Based on unit values at the world level between whole dry milk and whole cow milk, the conversion factor is equal to 0.1.

Two changes are done:

- Convert the code of "Milk, Whole fresh cow" (882) in producer prices to 897 "Dry Whole Cow Milk"
- Multiply the unit values of 897 by 0.1 to convert to fresh milk equivalent.

```{r CorrectMilk}
ProducerPrices <- within(ProducerPrices,{
  i <- FAO1 == 882
  FAO1[i] <- as.integer(897)
  i <- NULL
})
```

Remove useless observations
===========================

Keep only the products under study and remove any producer prices for "Oil and fats" as they will be taken only from unit values.

From the official FAOSTAT mapping between FAO1 and FAO2, we have done our own mapping in order to focus, when possible, on the primary commodities for which we will have both trade unit values and producer prices and not processed products, only available in trade unit values. The resulting mapping is available in <mappings/mapFAO1ref2.xlsx>.

```{r RmObs}
load("RData/mappingsFAO.RData")
ProducerPrices <- merge(ProducerPrices,unique(mapFAO1ref2[,c("FAO1Ref","FAO2")]),
                        by.x = "FAO1",by.y = "FAO1Ref")
ProducerPrices <- merge(ProducerPrices,mapFoAg,by = "FAO2") #PLUM food groups
ProducerPrices <- subset(ProducerPrices,
                         !(FoAgName %in% c("Non-food")) & !is.na(FoAgName),
                         select = c(FAOST_CODE,FAO1,year,value))
```

Clean producer prices from extreme values
=========================================

# it seems there are a good number of values above 10,000.  For now, in the Jan 2023 updates, I am keeping the extreme values. 

```{r CleanProdPrices}
# Remove prices below 10$/ton and above 100,000$/ton
#ProducerPrices <- subset(ProducerPrices,
#                         value >= 10 & value <= 1E5)

save("ProducerPrices", file = "RData/ProducerPrices.RData")
```

Compare producer prices to their median
---------------------------------------

```{r ComparePrices}
PPMedian <- ddply(ProducerPrices,
                  .(FAO1,year),
                  CompareToMedian)

quantile(PPMedian$ToMedian,
         c(0,0.01,0.05,0.1,0.25,0.5,0.75,0.9,0.95,0.99,1),
         na.rm = TRUE)
quantile(subset(PPMedian,ToMedian <= 20 & ToMedian >= 0.1)$ToMedian,
         c(0,0.01,0.05,0.1,0.25,0.5,0.75,0.9,0.95,0.99,1),
         na.rm = TRUE)

hist(subset(PPMedian,ToMedian <= 20 & ToMedian >= 0.1)$ToMedian,30)
hist(log(subset(PPMedian,ToMedian <= 20 & ToMedian >= 0.1)$ToMedian),30)
```


Prepare prices at the FAO1 level
================================

Trade unit values from BACI and MacMaps
---------------------------------------

```{r MacMapsUV}
source('PricesPrep2020.R')
source('MMUnitValues.R') ## updated January 2023
```

Mean producer prices
--------------------

```{r MeanProducerPrices}
source("MeanProducerPrices.R") ## updated January 2023 at FAO2 level
```

Mean prices
-----------

```{r MeanPrices}
source("MeanPricesFAO1.R") #now uses FAO2
```


Combine the trade unit values and the producer prices
-----------------------------------------------------

We keep in priority the trade data. 5937 missing values of trade data, 11035 missing values from producer price data. 

```{r CombinePrices}
load("RData/PricesFAO2.RData")
#load("RData/MeanProducerPrices.RData")

Prices <- merge(TradePricesFAO2,
                ProdPrice3Y,
                by = c("FAO2","Year","FAOST_CODE"),
                all = TRUE,
                suffixes = c(".Trade",".Prod"))

Prices <- within(Prices,{
 value <- ifelse(!is.na(value.Trade),value.Trade,value.Prod)
 Origin <- ifelse(!is.na(value.Trade),"Trade",
                                    ifelse(!is.na(value.Prod),"Producer",""))
 rm('value.Trade','value.Prod')
})
```

Fill missing values in price data
=================================

```{r FillMissingValues}
source("FillMissingPricesWorldMean.R") #updated years Jan 2023
```

Aggregate prices at the FAO2 level from FAO1
============================================

```{r AggregatetoFAO2}
#source("AggregatePrices.R") #updated years Jan 2023, prices data already at FAO2 level from PricesPrep2020
```

Fish data
=========

Since there is no mapping from FCL to HS6 for seafood products, we treat them
separately:

```{r FishData}
source("FishFAO1.R") ##not updated Jan 2023 (Fish excluded from PLUM anyway)
```

Outside good
============

```{r OutsideGood}
source("OutsideGoodPrice.R") #updated Jan 2023
```
