### updates to the FBSData.R file (from G&G) to download FAO FBS data up to 2020.
##note: 'Food' is now in kg per capita per year; previously it was not per capita (I think)

library(plyr)
library(readr)
library(reshape2)


load("RData/Macro.RData")
load("RData/mappingsFAO.RData")

if(!file.exists("SourceData/FAO-Bulk/FBS.RData")) {
  FBS <- read_faostat_bulk("SourceData/FAO-Bulk/FoodBalanceSheets_E_All_Data_(Normalized).zip")
  save("FBS",file = "SourceData/FAO-Bulk/FBS.RData")
} else load("SourceData/FAO-Bulk/FBS.RData")


FBS <- plyr::rename(FBS,c("area_code" = "FAOST_CODE",
                          "value" = "value",
                          "item_code" = "FAO2",
                          "item" = "FAO2Name",
                          "year" = "Year"))

FBS <- subset(FBS,
              subset = element_code %in% c(645,664) &
                Year %in% 2010:2019 &
                FAOST_CODE < 5000,
              select = -c(element_code,area_code__m49_,item_code__cpc_,unit,flag))
gc()

# Keep only separated countries from China
FBS <- subset(FBS, !(FAOST_CODE %in% c(351,357))) 

# Select non-aggregated items
FBS <- subset(FBS,FAO2 %in% unique(mapFAO12$FAO2))

# Calculate kcal per gram
FBS <- dcast(FBS,FAOST_CODE + FAO2 + Year ~ element)
FBS <- merge(FBS,Macro[,c("Year","FAOST_CODE","Population")])
FBS$kcal <- FBS[,"food_supply__kcal_capita_day_"]*365
FBS$kcalperg <- FBS$kcal/(FBS[,"food_supply_quantity__kg_capita_yr_"]*1000)

FBS <- plyr::rename(FBS,replace = c("food_supply__kcal_capita_day_"          = "kcalpercap",
                                    "food_supply_quantity__kg_capita_yr_" = "Food"))

FBS <- subset(FBS,select = c(FAOST_CODE,Year,FAO2,Food,kcalperg,kcalpercap))

# Convert sugar to refined equivalent
## The code 2542 is "Sugar (Raw Equivalent)" in FBS, but its price is only
## available for "Sugar, Refined Equiv". The coefficient of transformation from
## raw eq. to refined eq. is 0.92.
FBS <- within(FBS,{
  i <- FAO2 == 2542
  Food[i] <- Food[i]*0.92
  kcalperg[i] <- kcalperg[i]/0.92
  i <- NULL})
FBS <- melt(FBS,id.vars = c("FAOST_CODE","Year","FAO2"))

if (anyDuplicated(subset(FBS,select = c("FAOST_CODE","FAO2","Year","variable"))) != 0)
  stop('Duplicated entries in FBS')

save("FBS",file = "RData/FBS.RData")
