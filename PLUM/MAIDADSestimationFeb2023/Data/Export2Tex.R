library(FAOSTAT)
library(plyr)
library(readr)
library(reshape2)

load("RData/CountryList.RData")

# List of countries
cproj <- read.table("GAMS-input/CountriesProjection.gms",sep = "\t",
                    col.names = c("FAOST_CODE","CountryName"),
                    header = FALSE)

cesti <- read.table("GAMS-input/CountriesEstimation.gms",sep = "\t",
                    col.names = c("FAOST_CODE","CountryName"),
                    header = FALSE)

countries <- merge(data.frame(cproj,Proj = 1),data.frame(cesti,Esti = 1),all = T)
countries <- within(countries,{
  Proj <- ifelse(is.na(Proj),"\\tnote{\\textexclamdown}","")
  Esti <- ifelse(is.na(Esti),"\\tnote{\\textdagger}","")
  CountryName <- paste(CountryName,Proj,Esti,sep = "")})

CountryName <- sort(countries[,"CountryName"])
ncol <- 4
NbEmptyLinesToAdd <- length(CountryName) %% ncol
if(NbEmptyLinesToAdd!=0) NbEmptyLinesToAdd <- (length(CountryName) %/% ncol+1)*ncol-length(CountryName)
CountryName <- c(CountryName,rep("",NbEmptyLinesToAdd))
CountryName <- matrix(CountryName,ncol = ncol)

write.table(CountryName,
            file = "../Article/Tables/Countries.tex",
            quote = F,
            sep = "&",
            eol = "\\\\\n",
            row.names = F,
            col.names = F)

# Missing values
missing <- merge(cesti,readRDS("RData/SummaryMissingPrices.rds"))
missing <- subset(missing,
                  shareMissing >=1 ,
                  select = c("CountryName","shareMissing"))
missing <- arrange(missing,CountryName)
missing <- transform(missing,
                     CountryName = as.character(CountryName),
                     shareMissing = formatC(shareMissing,digits = 1,format = "f"))

ncol <- 2
NbEmptyLinesToAdd <- length(missing) %% ncol
missing <- rbind(missing,data.frame(CountryName = rep("",NbEmptyLinesToAdd),shareMissing = rep(NA,NbEmptyLinesToAdd)))
nr <- dim(missing)[1]/2
missing <- data.frame(C1 = missing[1:nr,1],
                      S1 = missing[1:nr,2],
                      C2 = missing[(nr+1):(2*nr),1],
                      S2 = missing[(nr+1):(2*nr),2])

write.table(missing,
            file = "../Article/Tables/MissingPrices.tex",
            quote = F,
            sep = "&",
            eol = "\\\\\n",
            row.names = F,
            col.names = F)

missingProducts <- readRDS("RData/SummaryMissingPricesProduct.rds") %>%
  subset(select = c("FoAgName","sharemissing")) %>%
  transform(sharemissing = formatC(sharemissing,digits = 1,format = "f"))

write.table(missingProducts[c(1,6,5,7,4,3,2),],
            file = "../Article/Tables/MissingPricesProducts.tex",
            quote = F,
            sep = "&",
            eol = "\\\\\n",
            row.names = F,
            col.names = F)

# World Bank country classification
countriesclass <- read_tsv("SourceData/WorldBankCountryClassification.txt")
countriesclass <- translateCountryCode(as.data.frame(countriesclass),
                                       from = "ISO3_WB_CODE",
                                       to = "FAOST_CODE",
                                       oldCode = "CountryCode")
countriesclass <- subset(countriesclass,
                         GroupCode %in% c("HIC","LIC","UMC","LMC"),
                         select = -c(ISO3_WB_CODE))

# Past growth
load("RData/Macro.RData")
Macro <- subset(Macro,
                Year %in% c(1961,2010),
                select = c("FAOST_CODE","Year","NY.GDP.PCAP.KD","Population"))
Macro <- plyr::rename(Macro,c("NY.GDP.PCAP.KD" = "ConstantGDPperCap"))

GrowthPast.countries <- reshape2::melt(Macro,
                             measure.vars = c("ConstantGDPperCap","Population"))
GrowthPast.countries <- reshape2::dcast(GrowthPast.countries,
                              FAOST_CODE + variable ~ paste("X",Year,sep = ""),
                              subset = .(Year %in% c(1961,2010)))
GrowthPast.countries <- within(GrowthPast.countries,{
  AnnuGrowth = (1 + X2010/X1961) ^ (1/49) - 1
  rm(X1961,X2010)
})

GrowthPast.groups <- merge(Macro,countriesclass)
GrowthPast.groups <- ddply(GrowthPast.groups,
                           .(Year,GroupName),
                           summarize,
                           ConstantGDPperCap = sum(ConstantGDPperCap*Population,na.rm = T)/sum(Population,na.rm = T),
                           Population = sum(Population,na.rm = T))
GrowthPast.groups <- reshape2::melt(GrowthPast.groups,
                          measure.vars = c("ConstantGDPperCap","Population"))
GrowthPast.groups <- reshape2::dcast(GrowthPast.groups,
                           GroupName + variable ~ paste("X",Year,sep = ""),
                           subset = .(Year %in% c(1961,2010)))
GrowthPast.groups <- within(GrowthPast.groups,{
  AnnuGrowth = (1 + X2010/X1961) ^ (1/49) - 1
  rm(X1961,X2010)
})

GrowthPast.groups2 <- merge(Macro,countriesclass)
GrowthPast.groups2 <- ddply(subset(GrowthPast.groups2,
                                   GroupCode %in% c("LMC","UMC") & !(FAOST_CODE %in% c(100,41))),
                           .(Year,GroupName),
                           summarize,
                           ConstantGDPperCap = sum(ConstantGDPperCap*Population,na.rm = T)/sum(Population,na.rm = T),
                           Population = sum(Population,na.rm = T))
GrowthPast.groups2 <- reshape2::melt(GrowthPast.groups2,
                          measure.vars = c("ConstantGDPperCap","Population"))
GrowthPast.groups2 <- reshape2::dcast(GrowthPast.groups2,
                           GroupName + variable ~ paste("X",Year,sep = ""),
                           subset = .(Year %in% c(1961,2010)))
GrowthPast.groups2 <- within(GrowthPast.groups2,{
  AnnuGrowth = (1 + X2010/X1961) ^ (1/49) - 1
  rm(X1961,X2010)
})
GrowthPast.groups2 <- transform(GrowthPast.groups2,
                                GroupName = paste("\\hspace{1.5ex} Other",tolower(GroupName)))

GrowthPast.world <- ddply(Macro,
                          .(Year),
                          summarize,
                          ConstantGDPperCap = sum(ConstantGDPperCap*Population,na.rm = T)/sum(Population,na.rm = T),
                          Population = sum(Population,na.rm = T))
GrowthPast.world <- reshape2::melt(GrowthPast.world,
                         measure.vars = c("ConstantGDPperCap","Population"))
GrowthPast.world <- reshape2::dcast(GrowthPast.world,
                          variable ~ paste("X",Year,sep = ""),
                          subset = .(Year %in% c(1961,2010)))
GrowthPast.world <- within(GrowthPast.world,{
  AnnuGrowth = (1 + X2010/X1961) ^ (1/49) - 1
  rm(X1961,X2010)
})

# Growth projections
load("RData/EconMapProjections.RData")

GrowthProj.countries <- reshape2::melt(subset(Projections,select = -c(ConstantGDP)),
                             measure.vars = c("ConstantGDPperCap","Population"))
GrowthProj.countries <- reshape2::dcast(GrowthProj.countries,
                              FAOST_CODE + variable ~ paste("X",Year,sep = ""),
                              subset = .(Year %in% c(2010,2050)))
GrowthProj.countries <- within(GrowthProj.countries,{
  AnnuGrowth = (1 + X2050/X2010) ^ (1/50) - 1
  rm(X2010,X2050)
})

GrowthProj.groups <- merge(Projections,countriesclass)
GrowthProj.groups <- ddply(GrowthProj.groups,
                            .(Year,GroupName),
                            summarize,
                            ConstantGDPperCap = sum(ConstantGDP)/sum(Population),
                            Population = sum(Population))
GrowthProj.groups <- reshape2::melt(GrowthProj.groups,
                           measure.vars = c("ConstantGDPperCap","Population"))
GrowthProj.groups <- reshape2::dcast(GrowthProj.groups,
                            GroupName + variable ~ paste("X",Year,sep = ""),
                            subset = .(Year %in% c(2010,2050)))
GrowthProj.groups <- within(GrowthProj.groups,{
  AnnuGrowth = (1 + X2050/X2010) ^ (1/50) - 1
  rm(X2010,X2050)
})

GrowthProj.groups2 <- merge(Projections,countriesclass)
GrowthProj.groups2 <- ddply(subset(GrowthProj.groups2,
                                   GroupCode %in% c("LMC","UMC") & !(FAOST_CODE %in% c(100,41))),
                           .(Year,GroupName),
                           summarize,
                           ConstantGDPperCap = sum(ConstantGDP)/sum(Population),
                           Population = sum(Population))
GrowthProj.groups2 <- reshape2::melt(GrowthProj.groups2,
                          measure.vars = c("ConstantGDPperCap","Population"))
GrowthProj.groups2 <- reshape2::dcast(GrowthProj.groups2,
                           GroupName + variable ~ paste("X",Year,sep = ""),
                           subset = .(Year %in% c(2010,2050)))
GrowthProj.groups2 <- within(GrowthProj.groups2,{
  AnnuGrowth = (1 + X2050/X2010) ^ (1/50) - 1
  rm(X2010,X2050)
})
GrowthProj.groups2 <- transform(GrowthProj.groups2,
                                GroupName = paste("\\hspace{1.5ex} Other",tolower(GroupName)))

GrowthProj.world <- ddply(Projections,
                           .(Year),
                           summarize,
                           ConstantGDPperCap = sum(ConstantGDP)/sum(Population),
                           Population = sum(Population))
GrowthProj.world <- reshape2::melt(GrowthProj.world,
                          measure.vars = c("ConstantGDPperCap","Population"))
GrowthProj.world <- reshape2::dcast(GrowthProj.world,
                           variable ~ paste("X",Year,sep = ""),
                           subset = .(Year %in% c(2010,2050)))
GrowthProj.world <- within(GrowthProj.world,{
  AnnuGrowth = (1 + X2050/X2010) ^ (1/50) - 1
  rm(X2010,X2050)
})

# Collecte growth rates together
GrowthPast.world <- transform(GrowthPast.world,Region = "World")
GrowthPast.groups <- plyr::rename(GrowthPast.groups,c("GroupName" = "Region"))
GrowthPast.groups <- transform(GrowthPast.groups,Region = paste("\\hspace{0.75ex}",Region))
GrowthPast.groups2 <- plyr::rename(GrowthPast.groups2,c("GroupName" = "Region"))
GrowthPast.countries <- merge(GrowthPast.countries,CountryList)
GrowthPast.countries <- plyr::rename(GrowthPast.countries,c("CountryName" = "Region"))

GrowthPast <- rbind(GrowthPast.world,
                    GrowthPast.groups,
                    GrowthPast.groups2,
                    subset(GrowthPast.countries,Region %in% c("China mainland","India"),
                           select = -c(FAOST_CODE)))
GrowthPast <- transform(GrowthPast,Period = "Past")

GrowthProj.world <- transform(GrowthProj.world,Region = "World")
GrowthProj.groups <- plyr::rename(GrowthProj.groups,c("GroupName" = "Region"))
GrowthProj.groups <- transform(GrowthProj.groups,Region = paste("\\hspace{0.75ex}",Region))
GrowthProj.groups2 <- plyr::rename(GrowthProj.groups2,c("GroupName" = "Region"))
GrowthProj.countries <- merge(GrowthProj.countries,CountryList)
GrowthProj.countries <- plyr::rename(GrowthProj.countries,c("CountryName" = "Region"))

GrowthProj <- rbind(GrowthProj.world,
                    GrowthProj.groups,
                    GrowthProj.groups2,
                    subset(GrowthProj.countries,Region %in% c("China mainland","India"),
                           select = -c(FAOST_CODE)))
GrowthProj <- transform(GrowthProj,Period = "Projections")

Growth <- rbind(GrowthPast,
                GrowthProj)
Growth <- transform(Growth,AnnuGrowth = formatC(AnnuGrowth*100,digits = 2,format = "f"))
Growth <- reshape2::dcast(Growth,Region ~ Period + variable,value.var = "AnnuGrowth")
Growth <- transform(Growth,
                    Region = revalue(Region,
                                     c("China mainland" = "\\hspace{1.5ex} China",
                                       "India" = "\\hspace{1.5ex} India")))

OrderTable <- data.frame(Region = c("World",
                                    "\\hspace{0.75ex} High income",
                                    "\\hspace{0.75ex} Upper middle income",
                                    "\\hspace{1.5ex} China",
                                    "\\hspace{1.5ex} Other upper middle income",
                                    "\\hspace{0.75ex} Lower middle income",
                                    "\\hspace{1.5ex} India",
                                    "\\hspace{1.5ex} Other lower middle income",
                                    "\\hspace{0.75ex} Low income"),
                         Rank = 1:9)
Growth <- merge(Growth,OrderTable)
Growth <- arrange(Growth,Rank)

write.table(subset(Growth,select = -Rank),
            file = "../Article/Tables/GrowthRates.tex",
            quote = F,
            sep = "&",
            eol = "\\\\\n",
            row.names = F,
            col.names = F)

# SSP

load("RData/EconMapSSPProjections.RData")

SSP.Growth.countries <- reshape2::melt(subset(SSP,select = -c(ConstantGDP)),
                             measure.vars = c("ConstantGDPperCap","Population"))
SSP.Growth.countries <- reshape2::dcast(SSP.Growth.countries,
                              SSP + FAOST_CODE + variable ~ paste("X",Year,sep = ""),
                              subset = .(Year %in% c(2010,2050)))
SSP.Growth.countries <- within(SSP.Growth.countries,{
  AnnuGrowth = (1 + X2050/X2010) ^ (1/50) - 1
  rm(X2010,X2050)
})

SSP.Growth.groups <- merge(SSP,countriesclass)
SSP.Growth.groups <- ddply(SSP.Growth.groups,
                               .(SSP,Year,GroupName),
                               summarize,
                               ConstantGDPperCap = sum(ConstantGDP)/sum(Population),
                               Population = sum(Population))
SSP.Growth.groups <- reshape2::melt(SSP.Growth.groups,
                              measure.vars = c("ConstantGDPperCap","Population"))
SSP.Growth.groups <- reshape2::dcast(SSP.Growth.groups,
                               SSP + GroupName + variable ~ paste("X",Year,sep = ""),
                               subset = .(Year %in% c(2010,2050)))
SSP.Growth.groups <- within(SSP.Growth.groups,{
  AnnuGrowth = (1 + X2050/X2010) ^ (1/50) - 1
  rm(X2010,X2050)
})

SSP.Growth.groups2 <- merge(SSP,countriesclass)
SSP.Growth.groups2 <- ddply(subset(SSP.Growth.groups2,
                                       GroupCode %in% c("LMC","UMC") & !(FAOST_CODE %in% c(100,41))),
                                .(SSP,Year,GroupName),
                                summarize,
                                ConstantGDPperCap = sum(ConstantGDP)/sum(Population),
                                Population = sum(Population))
SSP.Growth.groups2 <- reshape2::melt(SSP.Growth.groups2,
                               measure.vars = c("ConstantGDPperCap","Population"))
SSP.Growth.groups2 <- reshape2::dcast(SSP.Growth.groups2,
                                SSP + GroupName + variable ~ paste("X",Year,sep = ""),
                                subset = .(Year %in% c(2010,2050)))
SSP.Growth.groups2 <- within(SSP.Growth.groups2,{
  AnnuGrowth = (1 + X2050/X2010) ^ (1/50) - 1
  rm(X2010,X2050)
})
SSP.Growth.groups2 <- transform(SSP.Growth.groups2,
                                GroupName = paste("\\hspace{1.5ex} Other",tolower(GroupName)))

SSP.Growth.world <- ddply(SSP,
                              .(SSP,Year),
                              summarize,
                              ConstantGDPperCap = sum(ConstantGDP)/sum(Population),
                              Population = sum(Population))
SSP.Growth.world <- reshape2::melt(SSP.Growth.world,
                             measure.vars = c("ConstantGDPperCap","Population"))
SSP.Growth.world <- reshape2::dcast(SSP.Growth.world,
                              SSP + variable ~ paste("X",Year,sep = ""),
                              subset = .(Year %in% c(2010,2050)))
SSP.Growth.world <- within(SSP.Growth.world,{
  AnnuGrowth = (1 + X2050/X2010) ^ (1/50) - 1
  rm(X2010,X2050)
})

# Collecte growth rates together
SSP.Growth.world <- transform(SSP.Growth.world,Region = "World")
SSP.Growth.groups <- plyr::rename(SSP.Growth.groups,c("GroupName" = "Region"))
SSP.Growth.groups <- transform(SSP.Growth.groups,Region = paste("\\hspace{0.75ex}",Region))
SSP.Growth.groups2 <- plyr::rename(SSP.Growth.groups2,c("GroupName" = "Region"))
SSP.Growth.countries <- merge(SSP.Growth.countries,CountryList)
SSP.Growth.countries <- plyr::rename(SSP.Growth.countries,c("CountryName" = "Region"))

SSP.Growth <- rbind(SSP.Growth.world,
                    SSP.Growth.groups,
                    SSP.Growth.groups2,
                    subset(SSP.Growth.countries,Region %in% c("China mainland","India"),
                           select = -c(FAOST_CODE)))
SSP.Growth <- transform(SSP.Growth,AnnuGrowth = formatC(AnnuGrowth*100,digits = 2,format = "f"))
SSP.Growth <- transform(SSP.Growth,
                        Region = revalue(Region,
                                         c("China mainland" = "\\hspace{1.5ex} China",
                                           "India" = "\\hspace{1.5ex} India")))

SSP.Growth <- reshape2::dcast(SSP.Growth,Region ~ variable + SSP,value.var = "AnnuGrowth")

SSP.Growth <- merge(SSP.Growth,OrderTable)
SSP.Growth <- arrange(SSP.Growth,Rank)

write.table(subset(SSP.Growth,select = -Rank),
            file = "../Article/Tables/SSPGrowthRates.tex",
            quote = F,
            sep = "&",
            eol = "\\\\\n",
            row.names = F,
            col.names = F)
