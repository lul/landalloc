# TradeData - Import and format FAOSTAT trade data
### updates to the TradeData.R file (from G&G) to download FAO FBS data up to 2020.

library(plyr)
library(readr)
library(reshape2)

## Import Trade data from FAOSTAT
datadir <- "SourceData/FAO-Bulk"
#unzip(paste(datadir,"faostat.zip",sep = "/"),
#      "Trade_Crops_Livestock_E_All_Data.zip",
#      exdir = datadir)
## ATTENTION: The following line will use 3.2Go of memory
#Trade <- read_csv(paste(datadir,"Trade_Crops_Livestock_E_All_Data.zip",sep = "/"))
#file.remove(paste(datadir,"Trade_Crops_Livestock_E_All_Data.zip",sep = "/"))

Trade <- read_faostat_bulk(paste(datadir,"FoodBalanceSheets_E_All_Data_(Normalized).zip",sep = "/"))


# Format the data according to our standards
Trade <- rename(Trade,c("area_code" = "FAOST_CODE",
                        "value" = "value",
                        "item_code" = "FAO1",
                        "item" = "FAO1Name",
                        "year" = "Year"))
Trade <- subset(Trade,
                subset = element_code %in% c(5911,5611) &
                  Year %in% 2010:2019 &
                  FAOST_CODE < 5000,
                select = -c(area,element_code,area_code__m49_,item_code__cpc_,unit,flag))


FAO1 <- unique(Trade[,c("FAO1","FAO1Name")])
write.table(FAO1, "mappings/FAO1.txt", quote = FALSE, sep = "\t", row.names = FALSE)

Trade$FAO1Name <- NULL
Trade <- dcast(Trade,FAOST_CODE + FAO1 + Year ~ element, drop = TRUE)
Trade <- rename(Trade,
                c("export_quantity" = "ExportVol",
                  "import_quantity" = "ImportVol"))

# Keep only separated countries from China
Trade <- subset(Trade, !(FAOST_CODE %in% c(351,357)))

if (anyDuplicated(subset(Trade,select = c("FAOST_CODE","FAO1","Year"))) != 0)
    stop('Duplicated entries in Trade')

save("Trade",file = "SourceData/FAO-Bulk/Trade.RData")
