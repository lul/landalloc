$title MAIDADS Estimation

$if not set ZeroBeta $set ZeroBeta 1
$if not set Bshare $set Bshare 0

$offsymxref
$offsymlist
$ondotl
option limcol=0, limrow=0, sysout=off, solprint=on, nlp=conopt;
option decimals=8, reslim = 99999, iterlim = 99999, domlim = 1000 ;

SET
  c       "Country index" /
$include Countries.gms
/
  ca(c)   "Active countries for estimation" /
$include CountriesEstimation.gms
/
  i       "Sector index" /
$include sectors.gms
/
  ifood(i)
  type    "Type of calories"  /
  Vegetal "Vegetal-based"
  Animal  "Animal-based"
/
  gCV     "Groups for cross-validation" / 1*10 /
  caCV(c,gCV) "Country-group for cross-validation" /
$include CountriesGroupCV.gms
/
;

SINGLETON SET
  gTest(gCV) "Testing group for cross-validation"
;
gTest(gCV) = no;
$if set gTest gTest("%gTest%") = yes;

ifood(i) = yes$(not sameAs(i,"5000")) + no$sameAs(i,"5000");

ALIAS
  (i,j,ii)
  (c,l);

PARAMETER
  p(c,i)  "Price" /
$include Prices2010.gms
  /
  x(c,i)  "Per capita real expenditure" /
$include Consumption2010.gms
  /
  m(c)    "GDP per capita" /
$include GDPperCap2010.gms
/
  ni      Number of sectors
  nc      Number of countries
;

m(c)$(not ca(c)) = 0;
p(c,i)$(not ca(c)) = 0;

ni = card(i);
nc = sum(c$[m(c) and not caCV(c,gTest)], 1 );

* Calculate non-food consumption
x(c,"5000")$p(c,"5000") = (m(c)-sum(ifood,x(c,ifood)*p(c,ifood)))/p(c,"5000");
loop(c$m(c),abort$(x(c,"5000")<=0) "Negative non-food consumption", x;);

display m, p, x;

PARAMETER
  w(c,i)      Commodity budget share
  xhat(c,i)   Predicted Per cap real exp divided by 100 (quantity divided by 100)
  wmean(i)    Mean of actual budget share by commodity
  pmean(i)    Mean price level by commodity
  mmean       Mean GDP per capita
  wsd(i)      Standard deviation of actual budget share by commodity
  psd(i)      Standard deviation of price by commodity
  msd         Standard deviation of GDP per capita
  whatmean(i) Mean of fitted budget share by commodity
  whatsd(i)   Standard deviation of fitted budget share by commodity
  umean       Mean level of utility
  mdiscup     Upper bound on budget share going to subsistence consumption / 1 /
  Calories0(c,i)      Benchmark calories consumption
  TotalCalories0(c)   Benchmark total calories consumption
  Calories_Type0(c,type) Benchmark calories consumption by type
  Calorieshat(c,i)    Predicted calories consumption
  TotalCalorieshat(c) Predicted total calories consumption
  CoefType(*,i,type) /
$include CoefType.gms
/
  SIGMA(i,i)            Covariance matrix
;

w(c,i)$m(c)        = x(c,i)*p(c,i)/m(c);
Calories0(c,ifood) = x(c,ifood)*2000;
Calories_Type0(c,type) = sum(ifood,CoefType(c,ifood,type)*Calories0(c,ifood));
TotalCalories0(c)  = sum(ifood,Calories0(c,ifood));
mmean              = sum(c$m(c), m(c))/sum(c$m(c),1) ;
wmean(i)           = sum(c$m(c), w(c,i))/sum(c$m(c),1);
pmean(i)           = sum(c$m(c), p(c,i))/sum(c$m(c),1);
psd(i)             = sqrt(sum(c$m(c), power(p(c,i)-pmean(i),2))/sum(c$m(c),1));
wsd(i)             = sqrt(sum(c$m(c), power(w(c,i)-wmean(i),2))/sum(c$m(c),1));
msd                = sqrt(sum(c$m(c), power(m(c)-mmean,2))/sum(c$m(c),1));

POSITIVE VARIABLES
  alpha(i)              Parameter of AIDADS
  beta(i)               Parameter of AIDADS
  gamma(i)              Parameter of AIDADS
  delta(i)              Parameter of generalized gamma
  tau(i)                Parameter of generalized gamma
  omega                 Parameter of generalized gamma
  what(c,i)             Fitted budget share in AIDADS
  DiscretionaryC(c,i)   Discretionary consumption
  SubsistenceC(c,i)
;

VARIABLES
  kappa         Parameter of AIDADS
  u(c)          Utility by country
  v(c,i)        Error term by country and commodity
  LL            Concentrated log-likelihood
  r(i,i)        Cholesky factors
;

EQUATIONS
  EQ_LL                  "Definition of concentrated log-likelihood"
  EQ_r(i,j)              "Definition of Cholesky factors"
  EQ_v(c,i)              "Definition of error term"
  EQ_what(c,j)           "Definition of fitted budget share"
  EQ_u(c)                "Implicit definition of utility"
  EQ_SumAlpha            "Constraint that sum(i,alpha(i))=1"
  EQ_SumBeta             "Constraint that sum(i,beta(i))=1"
  EQ_MinDisc(c)          "Feasibility constraint that discretionary spending is positive"
  EQ_DiscretionaryC(c,i) "Definition of discretionary consumption"
  EQ_SubsistenceC(c,i)    "Definition of subsitence consumption"
  EQ_MAIDADS2AIDADS(i)   "Constraint transforming the MAIDADS in AIDADS"
;

EQ_LL..
$if %Bshare%==0 LL =e= -0.5*nc*[    ni*(1+log(2*pi)) + log(prod(i,sqr(r(i,i))))];
$if %Bshare%==1 LL =e= -0.5*nc*[(ni-1)*(1+log(2*pi)) + log(prod(i$(ord(i) lt card(i)),sqr(r(i,i))))];

$if %Bshare%==0 EQ_r(i,j)..
$if %Bshare%==1 EQ_r(i,j)$((ord(i) lt card(i)) and (ord(j) lt card(j)))..
  sum(ii$((ord(ii) le ord(i)) and (ord(ii) le ord(j))),r(ii,i)*r(ii,j)) =e= sum(c$[m(c) and not caCV(c,gTest)], v(c,i)*v(c,j) )/nc ;

EQ_v(c,i)$m(c)..
$if %Bshare%==0 v(c,i) =e= (w(c,i) - what(c,i))*m(c)/p(c,i) ;
$if %Bshare%==1 v(c,i) =e= (w(c,i) - what(c,i)) ;

EQ_u(c)$m(c)..
  sum(i, (alpha(i)+beta(i)*exp(u(c)))/(1+exp(u(c)))*log(DiscretionaryC(c,i)))-u(c) =e= kappa ;

EQ_SumAlpha..   sum(i,alpha(i)) =e= 1 ;

EQ_SumBeta..   sum(i,beta(i))  =e= 1 ;

EQ_what(c,j)$m(c)..
  what(c,j) =e= (SubsistenceC(c,j)+DiscretionaryC(c,j))*p(c,j)/m(c);

EQ_MinDisc(c)$[m(c) and not caCV(c,gTest)]..  sum(j, p(c,j)*SubsistenceC(c,j)) =l= mdiscup*m(c) ;

EQ_DiscretionaryC(c,i)$m(c).. DiscretionaryC(c,i) =e=
    [(alpha(i)+beta(i)*exp(u(c)))/(1+exp(u(c)))]/p(c,i)*
    [m(c)-sum(j,p(c,j)*SubsistenceC(c,j))];

EQ_SubsistenceC(c,j)$m(c)..
  SubsistenceC(c,j) =e= (delta(j)+tau(j)*exp(omega*u(c)))/(1+exp(omega*u(c)));

EQ_MAIDADS2AIDADS(i)..
  delta(i) =e= tau(i);

MODEL
  MAIDADS /
  EQ_LL
  EQ_r
  EQ_v
  EQ_what
  EQ_u
  EQ_SumAlpha
  EQ_SumBeta
  EQ_MinDisc
  EQ_DiscretionaryC
  EQ_SubsistenceC /
  AIDADS /
  MAIDADS
  EQ_MAIDADS2AIDADS /
  MAIDADS_Simulation /
  EQ_what.what
  EQ_u.u
  EQ_DiscretionaryC.DiscretionaryC
  EQ_SubsistenceC.SubsistenceC /
;

alpha.up(i)  = 1 ;
beta.up(i)   = 1 ;

what.lo(c,j) =   0 ;   what.up(c,j) =  1 ;
u.lo(c)      = -12 ;   u.up(c)      = 20 ;
DiscretionaryC.LO(c,i)   = 1E-11;
alpha.lo(ifood)   = 1E-6;

**
** Generate starting values for AIDADS using some arbitrary LES preferences
**

alpha.l(i)                 = wmean(i) ;
beta.l(i)                  = alpha.l(i) ;
kappa.l                    = 1;
gamma.l(i)                 = 0.25*smin(c$m(c),x(c,i)) ;
what.l(c,i)$m(c)           = gamma.l(i)*p(c,i)/m(c) + alpha.l(i)*(1-sum(j,p(c,j)*gamma.l(j))/m(c)) ;
$if %Bshare%==0 v.l(c,i)$m(c) = (w(c,i) - what.l(c,i))*m(c)/p(c,i) ;
$if %Bshare%==1 v.l(c,i)$m(c) = (w(c,i) - what.l(c,i)) ;
DiscretionaryC(c,i)$m(c) = alpha.l(i)*(m(c)-sum(j,p(c,j)*gamma.l(j)))/p(c,i);
u.l(c)$m(c)                = sum(i, alpha.l(i)*log(DiscretionaryC(c,i))) - kappa.l ;
SubsistenceC(c,i)         = gamma(i);
delta(i)                 = gamma(i);
tau(i)                   = gamma(i);



display v.l;

r.fx(i,j)$(ord(i) gt ord(j)) = 0 ;
r.l(i,j) = 0 ;
$if %Bshare%==0 loop(i,
$if %Bshare%==0   loop(j$(ord(j) ge ord(i)),
$if %Bshare%==1 loop(i$(ord(i) lt card(i)),
$if %Bshare%==1   loop(j$((ord(j) ge ord(i)) and (ord(j) lt card(j))),
    if(ord(j) ne ord(i),
      r.l(i,j) = (sum(c$[m(c) and not caCV(c,gTest)],v.l(c,i)*v.l(c,j))/nc
$if %Bshare%==0   -sum(ii$(ord(ii) lt ord(i)),
$if %Bshare%==1   -sum(ii$((ord(ii) lt ord(i)) and (ord(ii) lt card(ii))),
    r.l(ii,j)*r.l(ii,i)))/r.l(i,i) ;
    else
      r.l(i,j) = sqrt(sum(c$[m(c) and not caCV(c,gTest)],v.l(c,i)*v.l(c,j))/nc
$if %Bshare%==0  -sum(ii$(ord(ii) lt ord(i)),
$if %Bshare%==1  -sum(ii$((ord(ii) lt ord(i)) and (ord(ii) lt card(ii))),
    r.l(ii,j)*r.l(ii,i))) ;
    ) ;
  ) ;
) ;

$if %Bshare%==0 LL = -0.5*nc*[    ni*(1+log(2*pi)) + log(prod(i,sqr(r(i,i))))];
$if %Bshare%==1 LL = -0.5*nc*[(ni-1)*(1+log(2*pi)) + log(prod(i$(ord(i) lt card(i)),sqr(r(i,i))))];

option solvelink = 5;

omega.FX     = 0;
solve AIDADS using NLP maximizing LL;
omega.UP    = inf;
omega       = 1;
solve MAIDADS using NLP maximizing LL;
$if not %ZeroBeta%==1 $goto Next
PARAMETER
  Loglik0(*)
  LR0
;
Loglik0("H1") = LL;
beta.FX(ifood) = 0;
solve MAIDADS using NLP maximizing LL;
Loglik0("H0") = LL;
LR0 = 2*(Loglik0("H0")-Loglik0("H1"));
$label Next
* Check the solution using conopt4 instead of conopt3
option nlp=conopt4;
solve MAIDADS using NLP maximizing LL;
option nlp=conopt;


* Check if some computational constraints are binding
loop(c$m(c),
   abort$(EQ_MinDisc.M(c)) "Subsistence consumption is at its upper bound", EQ_MinDisc.M;
    loop(i,
      if(DiscretionaryC(c,i)=DiscretionaryC.LO(c,i), display "Discretionary spending is at its lower bound", DiscretionaryC.L;);
      abort$(what(c,i)=what.LO(c,i)) "Fitted budget share is at its lower bound", what.L;
      abort$(what(c,i)=what.UP(c,i)) "Fitted budget share is at its upper bound", what.L;
    );
);
loop(i,
  abort$(alpha(i)=alpha.UP(i)) "alpha is at its upper bound", alpha.L;
);

whatmean(i)      = sum(c$m(c), what.l(c,i))/sum(c$m(c),1);
whatsd(i)        = sqrt(sum(c$m(c), power(what.l(c,i)-whatmean(i),2))/sum(c$m(c),1));
umean            = sum(c$m(c), u.l(c))/sum(c$m(c),1) ;
xhat(c,i)$p(c,i) = what(c,i)*m(c)/p(c,i);

display caCV, x, xhat, v.L;

Calorieshat(c,ifood) = xhat(c,ifood)*2000;
TotalCalorieshat(c)  = sum(ifood,Calorieshat(c,ifood));

* Engel elasticities
PARAMETER
  eta(c,i)           Engel elasticities
  lambda(c)          Lagrange multiplier on the budget constraint
  etaType(c,type)    Engel elasticities by type of calories
  etakcal(c)         Engel elasticities for all calories
  elastprice(c,i,j)  Price elasticities
  elastownprice(c,i) Own-price elasticities
  elastsubst(c,i,j)  Substitution elasticities
  testelastprice(c,i,j)  Price elasticities
  alphaP(i)              estimated parameter of MAIDADS
  betaP(i)               estimated parameter of MAIDADS
  deltaP(i)              estimated parameter of MAIDADS
  tauP(i)                estimated parameter of MAIDADS
  omegaP                  estimated parameter of MAIDADS
  kappaP                  estimated parameter of MAIDADS
;

lambda(c)$m(c) = -power(sum(i,[(beta(i)-alpha(i))*exp(u(c))/power(1+exp(u(c)),2)*log(xhat(c,i)-(delta(i)+tau(i)*exp(omega*u(c)))/(1+exp(omega*u(c))))-(alpha(i)+beta(i)*exp(u(c)))/(1+exp(u(c)))*power(xhat(c,i)-(delta(i)+tau(i)*exp(omega*u(c)))/(1+exp(omega*u(c))),-1)*((tau(i)-delta(i))*omega*exp(omega*u(c)))/power(1+exp(omega*u(c)),2)]$(alpha(i) or beta(i)))-1,-1)*power(m(c)-sum(i,p(c,i)*(delta(i)+tau(i)*exp(omega*u(c)))/(1+exp(omega*u(c)))),-1);

eta(c,i)$m(c) = m(c)/(p(c,i)*xhat(c,i))*((alpha(i)+beta(i)*exp(u(c)))/(1+exp(u(c)))+(m(c)-sum(j,p(c,j)*(delta(j)+tau(j)*exp(omega*u(c)))/(1+exp(omega*u(c)))))*(beta(i)-alpha(i))*exp(u(c))*lambda(c)/(1+exp(u(c)))**2+(tau(i)-delta(i))*omega*exp(omega*u(c))*p(c,i)*lambda(c)/(1+exp(omega*u(c)))**2-(alpha(i)+beta(i)*exp(u(c)))/(1+exp(u(c)))*sum(j,(tau(j)-delta(j))*omega*exp(omega*u(c))*p(c,j)/(1+exp(omega*u(c)))**2)*lambda(c));

elastprice(c,i,j)$m(c)  = [[(alpha(i)+beta(i)*exp(u(c)))/(1+exp(u(c)))]*[m(c)-sum(ii,p(c,ii)*SubsistenceC(c,ii))]/(what(c,i)*m(c))]*([(alpha(j)+beta(j)*exp(u(c)))/(1+exp(u(c)))] - 1$sameas(i,j)) - what(c,j)*eta(c,i);
elastownprice(c,i)$m(c) = elastprice(c,i,i);

elastsubst(c,i,j)$(m(c) and not sameas(i,j)) = [(xhat(c,i)-SubsistenceC(c,i))*(xhat(c,j)-SubsistenceC(c,j))/(xhat(c,i)*xhat(c,j))]/[(m(c)-sum(ii,p(c,ii)*SubsistenceC(c,ii)))/m(c)];

* Check that the elasticities satisfy standard relations from demand theory
loop(c$m(c),
  abort$(abs(sum(i,eta(c,i)*what(c,i))-1)>=1E-6) "Engel elasticities do not respect Engel aggregation";
  loop(i,
    abort$(abs(sum(j,what(c,j)*elastprice(c,j,i))+what(c,i))>=1E-6) "Price elasticities do not respect Cournot aggregation";
    abort$(abs(sum(j,elastprice(c,i,j))+eta(c,i))>=1E-6) "Elasticities do not respect homogeneity condition";
    loop(j$(not sameas(i,j)),
      abort$(abs(elastprice(c,i,j)-what(c,j)*(elastsubst(c,i,j)-eta(c,i)))>=1E-6) "Elasticities do not respect Slutsky equation"
    );
  );
);

etaType(c,type)$m(c) = sum(ifood,CoefType(c,ifood,type)*eta(c,ifood)*xhat(c,ifood))/sum(ifood,CoefType(c,ifood,type)*xhat(c,ifood));
etakcal(c)$m(c)      = sum(ifood,eta(c,ifood)*xhat(c,ifood))/sum(ifood,xhat(c,ifood));

SIGMA(i,j) = sum(c$[m(c) and not caCV(c,gTest)], v(c,i)*v(c,j) )/nc;

alphaP(i) = alpha.L(i);
betaP(i) = beta.L(i);
deltaP(i) = delta.L(i);
tauP(i) = tau.L(i);
omegaP = omega.L;
kappaP = kappa.L;

EXECUTE_UNLOAD "Parameters" i, alphaP, betaP, deltaP, tauP, omegaP, kappaP;