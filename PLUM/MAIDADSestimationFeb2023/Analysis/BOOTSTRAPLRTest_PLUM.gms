option solprint=off;

MAIDADS.solvelink = 5;

* Produce a save point from the benchmark
beta.FX(ifood) = 0;
option savepoint = 1;
solve MAIDADS using NLP maximizing LL;
option savepoint = 0;;

option iterlim = 1E3;

PARAMETER
  w0(c,i)
  v0(c,i)
  what0(c,i)
;

SCALAR
  nc          Number of countries
  ni          Number of sectors
  iter
  Draw
;

SET
  iB / 1*2000 /;

SINGLETON SET
  rc(c)      Random draw of c;

PARAMETER
  Stat(iB,*,*)
  LR(iB)
  Loglik(iB,*)
  Pvalue
  Success(iB)
;

nc         = sum(c$m(c),1);
ni         = card(i);

w0(c,i)    = w(c,i);
what0(c,i) = what.L(c,i);
v0(c,i)    = v.L(c,i);


loop(iB,
  loop(c$m(c),
    w(c,"3905") = -1;
    while(smin(i,w(c,i)) le 0,
      Draw = uniformInt(1,nc);
      rc(l) = yes$(ord(l) eq Draw);
      w(c,i)$(ord(i) ne card(i)) = what0(c,i)+v0(rc,i)*p(c,i)/m(c);
      w(c,i)$(ord(i) eq card(i)) = 1-sum(j$(ord(j) ne card(j)),w(c,j));
    );
  );
  execute_loadpoint 'MAIDADS_p';
  beta.FX(ifood) = 0;
  solve MAIDADS using NLP maximizing LL;
  Loglik(iB,"H0") = LL.L;
  Stat(iB,"H0","Model") = MAIDADS.ModelStat;
  Stat(iB,"H0","Solve") = MAIDADS.SolveStat;

  beta.LO(i) = 0; beta.UP(i) = 1;
  solve MAIDADS using NLP maximizing LL;
  Loglik(iB,"H1") = LL.L;
  Stat(iB,"H1","Model") = MAIDADS.ModelStat;
  Stat(iB,"H1","Solve") = MAIDADS.SolveStat;
);
LR(iB) = 2*(Loglik(iB,"H0")-Loglik(iB,"H1"));

Success(iB)$(Stat(iB,"H0","Model")=2 and Stat(iB,"H0","Solve")=1 and Stat(iB,"H1","Model")=2 and Stat(iB,"H1","Solve")=1) = 1;

Pvalue = sum(iB$(Success(iB) and LR(iB)>=LR0),1)/sum(iB$Success(iB),1);
