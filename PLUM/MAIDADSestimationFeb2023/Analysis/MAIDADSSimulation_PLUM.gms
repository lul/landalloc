$ontext
Simulation of the MAIDADS function calibrated using the estimation from NLSUR.gms
$offtext

$if not set BS $set BS 0

$ondotl

$gdxin ./GDX/MAIDADS_Estimation0_PLUM.gdx
SET
  c
  i
  ifood(i)
  iter     Index iterations on GDP / 1*300 /
  type               / Vegetal, Animal /
;
$load c, i, ifood

ALIAS (i,j,ii);

POSITIVE VARIABLE
  DiscretionaryC(i)  Discretionary consumption
  SubsistenceC(i)    Subsistence consumption
  w(i)               Budget share by commodity
;

VARIABLE
  u            Utility
  alpha(i)     Parameter of MAIDADS
  beta(i)      Parameter of MAIDADS
  kappa        Parameter of MAIDADS
  delta(i)     Parameter of MAIDADS
  tau(i)       Parameter of MAIDADS
  omega        Parameter of MAIDADS
;
$load  alpha, beta, kappa, delta, tau, omega

PARAMETER
  p(i)                Price /
$include ../Data/GAMS-input/MeanPrices2010.gms
   /
  m                     GDP per capita
  eta(iter,i)           Engel elasticities
  etakcal(iter)         Engel elasticities for all calories
  eta_Type(type,iter)
  elastprice(iter,i,j)  Price elasticities
  elastownprice(iter,i) Own-price elasticities
  lambda                Lagrange multiplier on the budget constraint
  mu(i)                 Parameter used in the calculation of lambda
  mc(c)                 GDP per capita by country
  miter(iter)           Vector of GDP per capita (equally space on a log scale) /
$include ../Data/GAMS-input/GDPperCapSimulation.gms
            /
  witer(iter,i)         Vector of budget share
  uiter(iter)           Vector of utility
  xiter(iter,i)         Vector of consumed quantities
  DiscretionaryCiter(iter,i)
  SubsistenceCiter(iter,i)
  Calories(iter,i)              Vector of calories consumption
  TotalCalories(iter)           Vector of total calories consumption
  TotalCalories_Type(type,iter) Vector of total calories consumption by type of calories
  CoefType(*,i,type) /
$include ../Data/GAMS-input/CoefType.gms
/
;
$load m=mmean, mc=m
$gdxin

$if %BS%==0 $goto Benchmark
$gdxin ./GDX/BootstrapedMAIDADS0.gdx
SET iB;
PARAMETER
  alphaB(iB,i)
  betaB(iB,i)
  deltaB(iB,i)
  tauB(iB,i)
  omegaB(iB)
  kappaB(iB)
;
$load iB, alphaB, betaB, deltaB, tauB, omegaB, kappaB

SINGLETON SET iBsel(iB);
iBsel(iB) = yes$(ord(iB)=%BS%);

alpha(i) = alphaB(iBsel,i);
beta(i)  = betaB(iBsel,i);
delta(i) = deltaB(iBsel,i);
tau(i)   = tauB(iBsel,i);
omega    = omegaB(iBsel);
kappa    = kappaB(iBsel);

abort$(kappa.L>=-1E-6) "Wrong bootstrap replica";
$label Benchmark

display p;
p("5000") = 60;

w.up(j) = 1;

* Fix parameters at their estimated values
alpha.fx(i) = alpha.l(i);
beta.fx(i)  = beta.l(i);
kappa.fx    = kappa.l;
delta.fx(i) = delta.l(i);
tau.fx(i)   = tau.l(i);
omega.fx    = omega.l;

EQUATIONS
  EQ_w(i)      "Definition of budget share"
  EQ_u         "Implicit definition of utility"
  EQ_DiscretionaryC(i)
  EQ_SubsistenceC(j)
;

EQ_w(j)..
  w(j) =e= (SubsistenceC(j)+DiscretionaryC(j))*p(j)/m;

EQ_u..
  sum(i, (alpha(i)+beta(i)*exp(u))/(1+exp(u))*log(DiscretionaryC(i)))-u =e= kappa ;

EQ_DiscretionaryC(i).. DiscretionaryC(i) =e=
    [(alpha(i)+beta(i)*exp(u))/(1+exp(u))]/p(i)*
    [m-sum(j,p(j)*SubsistenceC(j))];

EQ_SubsistenceC(j)..
  SubsistenceC(j) =e= (delta(j)+tau(j)*exp(omega*u))/(1+exp(omega*u));

model MAIDADS_Sim "MAIDADS model for simulation" /
      EQ_w.w
      EQ_u.u
      EQ_DiscretionaryC.DiscretionaryC
      EQ_SubsistenceC.SubsistenceC
 / ;

MAIDADS_Sim.holdfixed = 1;
MAIDADS_Sim.optfile   = 1;
option solvelink = 5;
option cns=path;

u          = 0;
SubsistenceC(j)  = (delta(j)+tau(j)*exp(omega*u))/(1+exp(omega*u));
DiscretionaryC(i) = [(alpha(i)+beta(i)*exp(u))/(1+exp(u))]/p(i)*
    [m-sum(j,p(j)*SubsistenceC(j))];
DiscretionaryC.LO(i) = 1E-11;
SubsistenceC.LO(j) = 0;
w(j)       = (SubsistenceC(j)+DiscretionaryC(j))*p(j)/m;

display miter;

solve MAIDADS_Sim using cns;
p("5000") = 45.376+53.036*m/mc("231");
solve MAIDADS_Sim using cns;

loop(iter,
  m = miter(iter);
  p("5000") = min(100,45.376+53.036*m/mc("231"));
  display m;
  solve MAIDADS_Sim using cns;
  witer(iter,i) = w.l(i);
  uiter(iter)   = u.l;
  xiter(iter,i) = w.l(i)*m/p(i);
  DiscretionaryCiter(iter,i) = DiscretionaryC(i);
  SubsistenceCiter(iter,i) = SubsistenceC(i);

* Elasticities
  mu(i) = 0;
  mu(i)$(alpha(i) or beta(i)) = (beta(i)-alpha(i))*exp(u)/power(1+exp(u),2)*log(xiter(iter,i)-SubsistenceC(i));
  lambda = -power(sum(i,mu(i)-[(alpha(i)+beta(i)*exp(u))/(1+exp(u))*power(xiter(iter,i)-SubsistenceC(i),-1)*((tau(i)-delta(i))*omega*exp(omega*u))/power(1+exp(omega*u),2)]$(alpha(i) or beta(i)))-1,-1)*power(m-sum(i,p(i)*SubsistenceC(i)),-1);
  eta(iter,i) = (1/w(i))*((alpha(i)+beta(i)*exp(u))/(1+exp(u))+(m-sum(j,p(j)*SubsistenceC(j)))*(beta(i)-alpha(i))*exp(u)*lambda/(1+exp(u))**2+(tau(i)-delta(i))*omega*exp(omega*u)*p(i)*lambda/(1+exp(omega*u))**2-(alpha(i)+beta(i)*exp(u))/(1+exp(u))*sum(j,(tau(j)-delta(j))*omega*exp(omega*u)*p(j)/(1+exp(omega*u))**2)*lambda);
  elastprice(iter,i,j)  = [[(alpha(i)+beta(i)*exp(u))/(1+exp(u))]*[m-sum(ii,p(ii)*SubsistenceC(ii))]/(w(i)*m)]*([(alpha(j)+beta(j)*exp(u))/(1+exp(u))] - 1$sameas(i,j)) - w(j)*eta(iter,i);
  elastownprice(iter,i) = elastprice(iter,i,i);
  abort$(abs(sum(i,eta(iter,i)*w(i))-1)>=1E-6) "Engel elasticities do not respect Engel aggregation";
  loop(i,
    abort$(abs(sum(j,w(j)*elastprice(iter,j,i))+w(i))>=1E-6) "Price elasticities do not respect Cournot aggregation";
    abort$(abs(sum(j,elastprice(iter,i,j))+eta(iter,i))>=1E-6) "Elasticities do not respect homogeneity condition";
  );
);
etakcal(iter) = sum(ifood,eta(iter,ifood)*xiter(iter,ifood))/sum(ifood,xiter(iter,ifood));
eta_Type(type,iter) = sum(ifood,eta(iter,ifood)*CoefType("5000",ifood,type)*xiter(iter,ifood))/sum(ifood,CoefType("5000",ifood,type)*xiter(iter,ifood));

Calories(iter,ifood) = xiter(iter,ifood)*2000;
TotalCalories(iter)  = sum(ifood,Calories(iter,ifood));
TotalCalories_Type(type,iter) = sum(ifood,CoefType("5000",ifood,type)*Calories(iter,ifood));

EXECUTE_UNLOAD './GDX/MAIDADS_Simulation%BS%_PLUM.gdx';
