## created by Frances Cossar 18/10/2018
## To produce tradeBarriers input file for PLUM
## using By Country MFN Weighted Average in percentage for Food, from World between 2010 and 2010 
## from World Integrated Trade Solution (wits.worldbank.org)

## updated 20/07/2021 to use mean MFN weighted average tariff for 2005-2015, instead of just 2010. 
 
library("tidyverse")

landalloc_path <- "~/Documents/PhD/git_rstudio/landalloc"
setwd(file.path(landalloc_path, "PLUM/tradeBarriersSubsidies"))
plum_data_path <- "~/Documents/plumv2/data_update"

# input files
wits_countries <- read_csv("WITS-Countries.csv", name_repair = "universal") %>%
  rename(Iso3=Country.ISO3, Country=Country.Name)

# WITS tariffs
# Using the Effectively Applied Tariff (AHS Weighted Average (%)
# https://wits.worldbank.org/wits/wits/witshelp/content/data_retrieval/p/intro/c2.types_of_tariffs.htm
# Source:
# https://wits.worldbank.org/CountryProfile/en/Country/BY-COUNTRY/StartYear/1988/EndYear/2020/TradeFlow/Import/Indicator/AHS-WGHTD-AVRG/Partner/WLD/Product/16-24_FoodProd#
# Animal: meat, livestock, milk etc.
# Vegetable: cereals, fruit, veg, other raw crops
# Food products: sugar, oil, other processed foods
# Search "HS 1988/92" for classifications

wits_1 <- rbind(
  read_csv("WITS_avg_weighted_tariff_animal.csv", name_repair = "universal"),
  read_csv("WITS_avg_weighted_tariff_veg.csv", name_repair = "universal"),
  read_csv("WITS_avg_weighted_tariff_food.csv", name_repair = "universal")
) %>%
  rename_with(~ gsub("[\\.]+", "", .x))

wits <- wits_1 %>%
  select("Country"=ReporterName, ProductGroup, `2015`:`2019`) %>%
  pivot_longer(cols=`2015`:`2019`, names_to = "Year", values_to = "Tariff") %>%
  group_by(Country, ProductGroup) %>%
  summarise(Tariff = mean(Tariff, na.rm=T)) %>%
  left_join(select(wits_countries, Iso3, Country, Income.Group, Region), by="Country") %>%
  ungroup() %>%
  filter(!is.na(Tariff), !is.nan(Tariff))


plumCountries <- read_csv(file.path(plum_data_path, "countries.csv"))

# Sugar and oil crops under food products since those are mostly traded processed i.e. sugar and oils
product_map <- data.frame(
  plumCropItem=c("Ruminants","FruitVeg","WheatBarleyOats","Pulses","Starchy Roots","Oilcrops","Monogastrics",
    "MaizeMilletSorghum","Sugar","Rice (Paddy Equivalent)"),
  ProductGroup=c("Animal","Vegetable","Vegetable","Vegetable","Vegetable","Food Products", "Animal","Vegetable",
    "Food Products", "Vegetable")
)



EU <- c("Austria", "Belgium", "Bulgaria", "Croatia", "Cyprus", "Czech Republic", "Denmark", "Estonia", "Finland", "France",
        "Germany", "Greece", "Hungary", "Ireland", "Italy", "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands",
        "Poland", "Portugal", "Romania", "Slovak Republic", "Slovenia", "Spain", "Sweden")

# Convert to ISO3
EU <- wits_countries$Iso3[wits_countries$Country %in% EU]

foo <- wits %>%
  filter(Iso3 %in% EU)

euTariff <- wits[wits$Country=="European Union","Tariff",drop=T]

# merge tables
PLUM_tradeBarriers <- expand.grid(Iso3=plumCountries$Iso3, Item=product_map$plumCropItem,
                                  stringsAsFactors = F) %>%
  arrange(Iso3, Item) %>%
  left_join(product_map, by=c("Item"="plumCropItem")) %>%
  left_join(wits, by=c("Iso3", "ProductGroup")) %>%
  group_by(Item) %>%
  mutate(world_mean = mean(Tariff, na.rm=T)) %>%
  group_by(Region, Item) %>%
  mutate(region_mean = mean(Tariff, na.rm=T)) %>%
  mutate(Tariff = if_else(!is.na(Tariff), Tariff, if_else(!is.na(region_mean), region_mean, world_mean)),
         Tariff = Tariff / 100,
         add_distortion = 0) %>%
  ungroup() %>%
  select(Iso3, Item, Tariff, add_distortion)

write.csv(PLUM_tradeBarriers, file = "tradeBarriers.csv", 
          quote = FALSE, row.names = FALSE, eol="\n") # linux eol

