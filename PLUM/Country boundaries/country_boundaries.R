library(dplyr)
library(ggplot2)
library(tidyr)
library(sf)
library(terra)

plum_data_dir <- "~/Documents/plumv2/data"
landalloc_dir <- "~/Documents/PhD/git_rstudio/landalloc"
project_dir <- file.path(landalloc_dir, "PLUM/Country boundaries")
setwd(project_dir)

get_area <- function(lat, grid_size=0.5) {
  grid_size * pi / 180 * (sin(lat * pi / 180) - sin((lat - grid_size) * pi / 180)) * 6371^2 / 10000
}

countries <- terra::vect("world-administrative-boundaries/world-administrative-boundaries.shp")

country_map <- read.csv(file.path(plum_data_dir, "countries.csv"))

countries$iso3[countries$name=="Isle of Man"] <- "IMN"

countries <- merge(countries, select(country_map, Iso3, M49), by.x="iso3", by.y="Iso3" ,all.x=T)

countries$M49[countries$name == "Hong Kong"] <- country_map$M49[country_map$Area=="China"]
countries$M49[countries$name == "Macao"] <- country_map$M49[country_map$Area=="China"]
countries$M49[countries$name == "Jammu-Kashmir"] <- country_map$M49[country_map$Area=="India"]
countries$M49[countries$name == "Arunachal Pradesh"] <- country_map$M49[country_map$Area=="India"]
countries$M49[countries$name == "Hala'ib Triangle"] <- country_map$M49[country_map$Area=="Egypt"]
countries$M49[countries$name == "Madeira Islands"] <- country_map$M49[country_map$Area=="Portugal"]
countries$M49[countries$name == "Jersey"] <- country_map$M49[country_map$Area=="United Kingdom"]
countries$M49[countries$name == "Guernsey"] <- country_map$M49[country_map$Area=="United Kingdom"]
countries$M49[countries$name == "Kuril Islands"] <- country_map$M49[country_map$Area=="Russian Federation"]
countries$M49[countries$name == "Aksai Chin"] <- country_map$M49[country_map$Area=="China"]
countries$M49[countries$name == "Western Sahara"] <- country_map$M49[country_map$Area=="Morocco"]
countries$M49[countries$name == "Abyei"] <- country_map$M49[country_map$Area=="Sudan"]
countries$M49[countries$name == "Puerto Rico"] <- country_map$M49[country_map$Area=="United States of America"]
# 2023-11-01 removed Greenland as causing issues
#countries$M49[countries$name == "Greenland"] <- country_map$M49[country_map$Area=="Denmark"]
countries$M49[countries$name == "French Guiana"] <- country_map$M49[country_map$Area=="France"]
countries$M49[countries$name == "Reunion"] <- country_map$M49[country_map$Area=="France"]

plot(countries[is.na(countries$M49)])

foo <- as.data.frame(countries) %>% filter(is.na(M49))

countries <- countries[!is.na(countries$M49)]
plot(countries, "M49")
ras <- rast(nrows = 180 * 2, ncols = 360 * 2)
ras <- rasterize(countries, ras, field="M49", touches=T)
plot(ras)
plot(ras, xlim=c(-15, 15), ylim=c(45,65))

ras_df <- as.data.frame(ras, xy=T) %>%
  mutate(x = x - 0.25,
         y = y - 0.25) %>%
  rename(Lon=x, Lat=y)

lc <- read.table(file.path(plum_data_dir, "halfdeg/hilda_plus_2020.txt"), header=T)

ras_df <- inner_join(ras_df, select(lc, Lon, Lat), by=c("Lon", "Lat"))

write.csv(ras_df, "country_boundaries.csv", row.names = F, quote = F)

ggplot(ras_df, aes(Lon, Lat, fill=M49)) +
  geom_raster() +
  theme(legend.position = "none")

