# Models country-level BMI distributions using food supply data from PLUM
# Uses log-normal distributions fitted to historical BMI data
# Projects future BMI distributions based on:
# - Food supply (affects distribution mean)
# - Income inequality (affects distribution spread)
# Author: bart.arendarczyk@ed.ac.uk

# Libraries -------------------------------------------------------------------------------------------------------

#library(tidyverse)
library(readxl)

# Functions -------------------------------------------------------------------------------------------------------

# Converts Gini coefficient to standard deviation assuming log-normal distribution
# Used to transform income inequality measures into distribution parameters
gini_to_sd <- function(gini) {
  sqrt(2) * qnorm((gini + 1) / 2)
}
gini_to_sd <- Vectorize(gini_to_sd)

# Data Loading and Processing ------------------------------------------------------------------------------------

# Working directory
bmi_path = file.path(r_script_root, "PLUM/BMI")

# FAO country codes
countries_df <- read_csv(file.path(bmi_path, "FAOSTAT_countries.csv"), name_repair = "universal") %>%
  select(Country, M49.Code, ISO3.Code)

# SSP projections country codes
ssp_countries_df <- read_csv(file.path(bmi_path,"ssp_countries.csv"))

# Historical food supply data (kcal/capita/day)
# Combines two FAOSTAT datasets with different time periods
# https://www.fao.org/faostat/en/
kcal_df <- rbind(
  read_csv(file.path(bmi_path,"FAOSTAT_kcal.csv"), name_repair = "universal") %>% select(-Note),
  read_csv(file.path(bmi_path,"FAOSTAT_kcal_old.csv"), name_repair = "universal") %>% filter(Year <= 2009)
  ) %>%
  filter(Item == "Grand Total") %>%
  left_join(countries_df, by = join_by( Area.Code..M49. == M49.Code)) %>%
  select(ISO3.Code, Country, Year, "FoodSupply" = Value)

# Income inequality projections for Shared Socioeconomic Pathways (SSPs)
# Interpolates 5-year interval data to annual values
# https://ssp-extensions.apps.ece.iiasa.ac.at/learn
gini_ssp_df0 <- read_excel(file.path(bmi_path,"SSP-Extensions_Gini_Coefficient_v1.0.xlsx"), sheet = "data") %>%
  filter(Variable == "Gini Income Inequality Coefficient") %>%
  inner_join(ssp_countries_df, by = join_by(Region)) %>%
  select(Scenario, ISO3.Code, "Country"=Region, `2015`:`2100`) %>%
  pivot_longer(cols = `2015`:`2100`, names_to = "Year", values_to = "Gini") %>%
  mutate(Year = as.integer(Year))

# Create continuous annual Gini projections and convert to standard deviations
gini_ssp_df <- expand.grid(Scenario = unique(gini_ssp_df0$Scenario), ISO3.Code = unique(gini_ssp_df0$ISO3.Code), Year = 2020:2100) %>%
  left_join(gini_ssp_df0 %>% select(Scenario, ISO3.Code, Year, Gini), by = join_by(Scenario, ISO3.Code, Year)) %>%
  arrange(Scenario, ISO3.Code, Year) %>%
  group_by(Scenario, ISO3.Code) %>%
  mutate(Gini = approx(Year, Gini, xout=Year, method="linear")$y,
         IncomeSD = gini_to_sd(Gini/100))

rm(gini_ssp_df0)

# Historical Gini coefficients from World Income Inequality Database
# https://www.wider.unu.edu/database/world-income-inequality-database-wiid
gini_hist_df_0 <- read_excel(file.path(bmi_path,"wiidcountry.xlsx"), sheet=1) %>%
  filter(giniseries == 1) %>%
  select("Country"=country, "ISO3.Code"=c3, "Year"=year, "Gini"=gini_std) %>%
  mutate(Year = as.integer(Year),
         Gini = Gini / 100) %>%
  group_by(ISO3.Code) %>%
  mutate(Count = length(ISO3.Code)) %>%
  filter(Count >= 2) %>%
  select(-Count) %>%
  group_by(Year, ISO3.Code, Country) %>%
  summarise(Gini = mean(Gini)) %>%
  ungroup()

# Interpolate missing values
gini_hist_df <- gini_hist_df_0 %>%
  select(Country, ISO3.Code) %>%
  unique() %>%
  cross_join(data.frame(Year=1950:2022)) %>%
  left_join(gini_hist_df_0, by = join_by(Country, ISO3.Code, Year)) %>%
  group_by(ISO3.Code) %>%
  mutate(Gini = approx(Year, Gini, xout=Year, method="linear", rule=2)$y) %>%
  filter(Year >= 1990) %>%
  mutate(IncomeSD = gini_to_sd(Gini))

rm(gini_hist_df_0)

# Age projection data from SSP Database 3.1
# Processes demographic projections and combines age bands for older populations
# https://data.ece.iiasa.ac.at/ssp/#/downloads
age_ssp_df_0 <- read_excel(file.path(bmi_path,"1721734326790-ssp_basic_drivers_release_3.1_full.xlsx"), sheet="data",
                           col_types = c(rep("text", 5), rep("numeric", 31))) %>%
  filter(grepl("Population\\|(?:Male|Female)\\|Age \\d{1,3}-\\d{1,3}$", Variable)) %>%
  separate(Variable, c("Variable", "Sex", "AgeBand"), sep="\\|") %>%
  mutate(AgeBand = gsub("Aged", "", AgeBand)) %>%
  select(Region, Scenario, Sex, AgeBand, `1950`:`2100`) %>%
  pivot_longer(cols=`2010`:`2100`, names_to = "Year", values_to = "Population") %>%
  mutate(AgeBand = gsub("Age ", "", AgeBand),
         AgeBand = if_else(AgeBand %in% c("85-89", "90-94", "95-99", "100+"), "85plus", AgeBand),
         Year = as.integer(Year)) %>%
  filter(Scenario != "Historical Reference", Year >= 2020) %>%
  group_by(Region, Year, Scenario, AgeBand, Sex) %>%
  summarise(Population = sum(Population)) %>%
  filter(!(AgeBand %in% c("0-4", "5-9", "10-14"))) %>%
  mutate(Population = if_else(AgeBand == "15-19", Population * 0.4, Population),
         AgeBand = if_else(AgeBand == "15-19", "18-19", AgeBand)) %>%
  inner_join(ssp_countries_df, join_by(Region)) %>%
  ungroup() %>%
  select(ISO3.Code, Scenario, Year, Sex, AgeBand, Population)

# Interpolate missing values
age_ssp_df <- expand_grid(ISO3.Code=unique(age_ssp_df_0$ISO3.Code), Year=2010:2100, Sex=unique(age_ssp_df_0$Sex), 
                          AgeBand=unique(age_ssp_df_0$AgeBand), Scenario=unique(age_ssp_df_0$Scenario)) %>%
  left_join(age_ssp_df_0, join_by(ISO3.Code, Scenario, Year, Sex, AgeBand)) %>%
  group_by(Scenario, ISO3.Code, AgeBand, Sex) %>%
  mutate(Population = approx(Year, Population, xout=Year, method="linear", rule=2)$y,
         Scenario = substr(Scenario, 1, 4),
         Sex = if_else(Sex=="Female", "Women", "Men"),
         Cohort = paste0(Sex, "_", AgeBand)) %>%
  ungroup()

rm(age_ssp_df_0)

# BMI Distribution Modeling --------------------------------------------------------------------------------------
##To speed performance, the code below, which converts the RisC BMI data to lognormal 
##distribution parameters, has been run once and the results saved to a file
bmi_param_df= read.csv(file.path(bmi_path, "RisC_BMI_Params.csv"))

# Define BMI categories for analysis
# bmi_subset <- c("Prevalence of BMI<18.5 kg/m² (underweight)",
#                 "Prevalence of BMI 18.5 kg/m² to <20 kg/m²",
#                 "Prevalence of BMI 20 kg/m² to <25 kg/m²",
#                 "Prevalence of BMI 25 kg/m² to <30 kg/m²",
#                 "Prevalence of BMI 30 kg/m² to <35 kg/m²",
#                 "Prevalence of BMI 35 kg/m² to <40 kg/m²",
#                 "Prevalence of BMI >=40 kg/m² (morbid obesity)")

# Load and process BMI prevalence data
# Combines male and female data and adds BMI category bounds
# From https://www.ncdrisc.org/data-downloads-adiposity.html
# # Original source https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(23)02750-2/fulltext
# bmi_df <- rbind(
#   read_csv(file.path(bmi_path,"NCD_RisC_Lancet_2024_BMI_female_age_specific_country.csv")),
#   read_csv(file.path(bmi_path,"NCD_RisC_Lancet_2024_BMI_male_age_specific_country.csv"))) %>%
#   pivot_longer(cols=6:35, names_to = "BMIGroup", values_to = "Prevalance") %>%
#   filter(BMIGroup %in% bmi_subset)  %>%
#   rename(ISO3.Code = ISO, AgeGroup = `Age group`) %>%
#   mutate(
#     Lower = case_when(
#       BMIGroup == "Prevalence of BMI<18.5 kg/m² (underweight)" ~ -Inf,
#       BMIGroup == "Prevalence of BMI 18.5 kg/m² to <20 kg/m²" ~ 18.5,
#       BMIGroup == "Prevalence of BMI 20 kg/m² to <25 kg/m²" ~ 20,
#       BMIGroup == "Prevalence of BMI 25 kg/m² to <30 kg/m²" ~ 25,
#       BMIGroup == "Prevalence of BMI 30 kg/m² to <35 kg/m²" ~ 30,
#       BMIGroup == "Prevalence of BMI 35 kg/m² to <40 kg/m²" ~ 35,
#       BMIGroup == "Prevalence of BMI >=40 kg/m² (morbid obesity)" ~ 40),
#     Upper = case_when(
#       BMIGroup == "Prevalence of BMI<18.5 kg/m² (underweight)" ~ 18.5,
#       BMIGroup == "Prevalence of BMI 18.5 kg/m² to <20 kg/m²" ~ 20,
#       BMIGroup == "Prevalence of BMI 20 kg/m² to <25 kg/m²" ~ 25,
#       BMIGroup == "Prevalence of BMI 25 kg/m² to <30 kg/m²" ~ 30,
#       BMIGroup == "Prevalence of BMI 30 kg/m² to <35 kg/m²" ~ 35,
#       BMIGroup == "Prevalence of BMI 35 kg/m² to <40 kg/m²" ~ 40,
#       BMIGroup == "Prevalence of BMI >=40 kg/m² (morbid obesity)" ~ Inf)
#   )

# # Limit data to 2020
# bmi_cohorts_df <- bmi_df %>%
#   filter(Year == 2020) %>%
#   select(Year, ISO3.Code, Sex, AgeGroup) %>%
#   unique()

# Fit log-normal distributions to BMI data using Kullback-Leibler divergence minimization
# Processes each country-year-age-sex combination separately
# opt_res_list <- list()
# rn <- 1
# print("Fitting log-normal BMI distributions to data, please wait 5-10 minutes...")
# for (i in 1:nrow(bmi_cohorts_df)) {
#   iso3 <- bmi_cohorts_df$ISO3.Code[i]
#   year <- bmi_cohorts_df$Year[i]
#   age <- bmi_cohorts_df$AgeGroup[i]
#   sex <- bmi_cohorts_df$Sex[i]
#   
#   dt <- filter(bmi_df, ISO3.Code == iso3, Year == year, AgeGroup == age, Sex == sex)
#   
#   # Objective function for KL divergence minimization
#   obj_fn <- function(x) {
#     if (any(x <= 0)) {return(1e6)}
#     
#     y <- dt %>%
#       mutate(P = plnorm(Upper, x[1], x[2]) - plnorm(Lower, x[1], x[2]),
#              CE = Prevalance * log(Prevalance/P))
#     sum(y$CE)
#   }
#   
#   x <- optim(par=c(3, 0.2), fn=obj_fn)
#   opt_res_list[[rn]] <- data.frame(iso3, year, sex, age, mu = x$par[1], sigma = x$par[2])
#   rn <- rn + 1
#   #print(paste(iso3, year, sex, age))
# }
# 
# bmi_param_df <- do.call(rbind, opt_res_list)
# colnames(bmi_param_df)[1:4] <- c("ISO3.Code", "Year", "Sex", "AgeGroup")
# 
# bmi_param_df <- bmi_param_df %>%
#   mutate(AgeBand = if_else(AgeGroup == "85plus", "85-89", AgeGroup)) %>%
#   separate(AgeBand, c("AgeLower", "AgeUpper"), "-", remove=F) %>%
#   mutate(AgeMid = (as.integer(AgeUpper) + as.integer(AgeLower)) / 2)%>%
#   mutate(Cohort = paste0(Sex, "_", AgeGroup))
# 
# write.csv(bmi_param_df, file.path(bmi_path, "RisC_BMI_Params.csv"))

# # Check fit
# bmi_param_df %>%
#   left_join(bmi_df, join_by(ISO3.Code, Year, Sex, AgeGroup)) %>%
#   mutate(Fit = plnorm(Upper, mu, sigma) - plnorm(Lower, mu, sigma)) %>%
#   filter(Year == 2020) %>%
#   ggplot(aes(Prevalance, Fit)) +
#     geom_point() +
#     facet_wrap(~ BMIGroup) +
#   geom_abline(slope=1, intercept = 0)

# BMI Model data 
model_df <- bmi_param_df %>%
  inner_join(kcal_df, join_by(ISO3.Code, Year)) %>%
  inner_join(gini_hist_df %>% select(Year, ISO3.Code, IncomeSD), by = join_by(ISO3.Code, Year)) %>%
  filter(Year == 2020) 

#ggplot(model_df, aes(FoodSupply, mu)) + geom_point() + geom_smooth(formula = y ~ I(log(x)), method="lm") +
#  facet_wrap(~ AgeGroup)

#ggplot(model_df, aes(IncomeSD, sigma, colour)) + geom_point() + geom_smooth(formula = y ~ x, method="lm") +
#  facet_wrap(~ AgeGroup)

# Fit model for each cohort
mu_lm_list <- list()
for (cohort in unique(model_df$Cohort)) {
  mu_lm <- lm(mu ~ log(FoodSupply), data = filter(model_df, Cohort == cohort))
  mu_lm_list[[cohort]] <- mu_lm
}

sigma_lm_list <- list()
for (cohort in unique(model_df$Cohort)) {
  sigma_lm <- lm(sigma ~ IncomeSD, data = filter(model_df, Cohort == cohort))
  sigma_lm_list[[cohort]] <- sigma_lm
}

# Future Projections --------------------------------------------------------------------------------------------
# These functions are run in the healthOutcomes.R code to convert PLUM results to BMI projections

# Project BMI distributions using food supply and income inequality relationships
# Uses regression models fitted to historical data
proj_bmi_cohort <- function(iso3, demand_df) {
  
  # Combine food supply and income inequality projections
  ssp_input_df <- demand_df %>%
    filter(Iso3 == iso3) %>%
    left_join(gini_ssp_df, by = join_by(SSP_SCENARIO==Scenario, Year, Iso3==ISO3.Code)) %>%
    mutate(FoodSupply = totalKcalPc)
  
  # Project distribution parameters for each cohort
  ssp_param_list <- list()
  cohorts <- names(mu_lm_list)
  for (cohort in cohorts) {
    pred_df <- ssp_input_df %>% mutate(Cohort=cohort)
    pred_df$mu_pred <- predict(mu_lm_list[[cohort]], newdata = pred_df)
    pred_df$sigma_pred <- predict(sigma_lm_list[[cohort]], newdata = pred_df)
    ssp_param_list[[cohort]] <- pred_df
  }
  
  ssp_param_df <- do.call(bind_rows, ssp_param_list)
  
  # Combine and rebase parameters to match historical values
  ssp_param_df <- ssp_param_df %>%
    left_join(bmi_param_df %>% filter(Year==2020) %>% select(Cohort, "mu_base"=mu, "sigma_base"=sigma, ISO3.Code),
              join_by(Cohort, Iso3==ISO3.Code)) %>%
    group_by(Iso3, Scenario, Cohort, Ensemble) %>%
    mutate(mu = mu_pred + (mu_base - mu_pred[Year==2020]),
           sigma = sigma_pred + (sigma_base - sigma_pred[Year==2020]))
  
  # Some plots
  # ggplot(ssp_param_df %>% filter(grepl("s1$", Scenario), Iso3=="IND"), aes(Year, mu, colour=SSP)) +
  #   geom_line() +
  #   facet_wrap(~ Cohort)
  # 
  # ggplot(ssp_param_df %>% filter(grepl("s1$", Scenario), Iso3=="IND"), aes(Year, sigma, colour=SSP)) +
  #   geom_line() +
  #   facet_wrap(~ Cohort)
  
  # Calculate BMI category prevalences from projected distributions
  bmi_cohort_df <- ssp_param_df %>%
    mutate(`P[0,17]` = plnorm(17, mu, sigma),
           `P[17,18.5]` = plnorm(18.5, mu, sigma) - plnorm(17, mu, sigma),
           `P[18.5,20]` = plnorm(20, mu, sigma) - plnorm(18.5, mu, sigma),
           `P[20,25]` = plnorm(25, mu, sigma) - plnorm(20, mu, sigma),
           `P[25,30]` = plnorm(30, mu, sigma) - plnorm(25, mu, sigma),
           `P[30,35]` = plnorm(35, mu, sigma) - plnorm(30, mu, sigma),
           `P[35,40]` = plnorm(40, mu, sigma) - plnorm(35, mu, sigma),
           `P[40,Inf]` = plnorm(Inf, mu, sigma) - plnorm(40, mu, sigma)) %>%
    select(Scenario, Ensemble, SSP_SCENARIO, Country, Iso3, Year, Cohort, IncomeSD, FoodSupply, "mu"=mu, "sigma"=sigma, `P[0,17]`:`P[40,Inf]`)
  
  bmi_cohort_df
}


# Generate and save results
# Warning: slow. Could parallelize
proj_bmi_cohorts = function(demand_df){

bmi_all_list <- list()
for (iso3 in unique(demand_df$Iso3)) {
  
  bmi_cohort_df <- proj_bmi_cohort(iso3, demand_df)
  
  # Aggreate cohorts
  bmi_all_df <- bmi_cohort_df %>%
    select(Scenario:Cohort, `P[0,17]`:`P[40,Inf]`) %>%
    pivot_longer(cols=`P[0,17]`:`P[40,Inf]`, names_to = "BMIGroup", values_to = "Prevalence") %>%
    left_join(age_ssp_df %>% select(ISO3.Code, Year, Cohort, Population, Scenario), 
              join_by(Iso3==ISO3.Code, Year, Cohort, SSP_SCENARIO==Scenario)) %>%
    group_by(Scenario, Ensemble, Country, Iso3, Year, SSP_SCENARIO, BMIGroup) %>%
    summarise(Population = sum(Prevalence * Population)) %>%
    group_by(Scenario, Ensemble, Country, Iso3, Year, SSP_SCENARIO) %>%
    mutate(Prevalence = Population / sum(Population))
  
  bmi_all_list[[iso3]] <- bmi_all_df
  print(iso3)
}

# Combine results and write to CSV
bmi_all_df <- do.call(rbind, bmi_all_list)

bmi_all_df
}

#write_csv(bmi_all_df, "bmi_proj.csv")


# bmi_all_piv_df <- bmi_all_df %>%
#   select(-Population) %>%
#   pivot_wider(names_from = BMIGroup, values_from = Prevalance) %>%
#   left_join(age_ssp_df %>% group_by(ISO3.Code, Year, Scenario) %>% summarise(`Population_18+`=sum(Population)),
#             join_by(Iso3==ISO3.Code, Year, SSP==Scenario)) %>%
#   ungroup() %>%
#   select(Scenario:Year, `Population_18+`, `P[0,17]`:`P[40,Inf]`)
# 
# write_csv(bmi_all_piv_df, "india_bmi_agg.csv")
# 
# temp_df <- bmi_cohort_df %>%
#   mutate(SSP = substr(Scenario, 1, 4)) %>%
#   left_join(age_ssp_df %>% select(ISO3.Code, Year, Cohort, Population, Scenario), 
#             join_by(Iso3==ISO3.Code, Year, Cohort, SSP==Scenario)) %>%
#   select(-SSP) %>%
#   select(Scenario:sigma, Population, everything())
# 
# write_csv(temp_df, "india_bmi_cohorts.csv")

# Validation and other --------------------------------------------------------------------------------------------

# # Population
# # https://www.fao.org/faostat/en/
# pop_df <- read_csv("FAOSTAT_population.csv") %>%
#   filter(Element %in% c("Total Population - Male", "Total Population - Female")) %>%
#   select("M49.Code"=`Area Code (M49)`, Year, Element, "Population"=Value) %>%
#   mutate(Sex = case_match(Element,
#                           "Total Population - Male" ~ "Men", 
#                           "Total Population - Female" ~ "Women")) %>%
#   left_join(countries_df, by = join_by(M49.Code))
# 
# # Calculate age standardised BMI distributions so we can compare projections with historical trends
# bmi_as_df <- read_csv("NCD_RisC_Lancet_2024_BMI_age_standardised_country.csv") %>%
#   pivot_longer(cols=5:34, names_to = "BMIGroup", values_to = "Prevalance") %>%
#   filter(BMIGroup %in% bmi_subset)  %>%
#   rename(ISO3.Code = ISO) %>%
#   inner_join(pop_df, by = join_by(ISO3.Code, Year, Sex)) %>%
#   mutate(Count = Prevalance * Population) %>% # Aggregate sex to get population statistics
#   group_by(ISO3.Code, Country, Year, BMIGroup) %>%
#   summarise(Count = sum(Count)) %>%
#   group_by(ISO3.Code, Country, Year) %>%
#   mutate(Prevalance = Count / sum(Count)) %>%
#   mutate(
#     Lower = case_when(
#       BMIGroup == "Prevalence of BMI<18.5 kg/m² (underweight)" ~ -Inf,
#       BMIGroup == "Prevalence of BMI 18.5 kg/m² to <20 kg/m²" ~ 18.5,
#       BMIGroup == "Prevalence of BMI 20 kg/m² to <25 kg/m²" ~ 20,
#       BMIGroup == "Prevalence of BMI 25 kg/m² to <30 kg/m²" ~ 25,
#       BMIGroup == "Prevalence of BMI 30 kg/m² to <35 kg/m²" ~ 30,
#       BMIGroup == "Prevalence of BMI 35 kg/m² to <40 kg/m²" ~ 35,
#       BMIGroup == "Prevalence of BMI >=40 kg/m² (morbid obesity)" ~ 40),
#     Upper = case_when(
#       BMIGroup == "Prevalence of BMI<18.5 kg/m² (underweight)" ~ 18.5,
#       BMIGroup == "Prevalence of BMI 18.5 kg/m² to <20 kg/m²" ~ 20,
#       BMIGroup == "Prevalence of BMI 20 kg/m² to <25 kg/m²" ~ 25,
#       BMIGroup == "Prevalence of BMI 25 kg/m² to <30 kg/m²" ~ 30,
#       BMIGroup == "Prevalence of BMI 30 kg/m² to <35 kg/m²" ~ 35,
#       BMIGroup == "Prevalence of BMI 35 kg/m² to <40 kg/m²" ~ 40,
#       BMIGroup == "Prevalence of BMI >=40 kg/m² (morbid obesity)" ~ Inf)
#   )
# 
# # Estimate log-normal distribution for BMI using the Kullback–Leibler divergence
# opt_res_as_list <- list()
# rn <- 1
# 
# for (iso3 in unique(bmi_as_df$ISO3.Code)) {
#   dt1 <- filter(bmi_as_df, ISO3.Code == iso3)
#   
#   for (year in unique(dt1$Year)) {
#     dt2 <- filter(dt1, Year == year)
#     
#     obj_fn <- function(x) {
#       if (any(x <= 0)) {return(1e6)}
#       
#       y <- dt2 %>%
#         mutate(P = plnorm(Upper, x[1], x[2]) - plnorm(Lower, x[1], x[2]),
#                CE = Prevalance * log(Prevalance/P))
#       sum(y$CE)
#     }
#     
#     x <- optim(par=c(3, 0.2), fn=obj_fn)
#     opt_res_as_list[[rn]] <- data.frame(iso3, year, mu = x$par[1], sigma = x$par[2])
#     rn <- rn + 1
#     print(paste(iso3, year))
#   }
# }
# 
# bmi_param_as_df <- do.call(rbind, opt_res_as_list)
# colnames(bmi_param_as_df)[1:2] <- c("ISO3.Code", "Year")
# 
# # Some plots  
# plot_stat_df <- bmi_all_df %>%
#   group_by(SSP, Year, Country, BMIGroup) %>%
#   summarise(Med = median(Prevalance),
#             Low = quantile(Prevalance, 0.05),
#             High = quantile(Prevalance, 0.95))
# 
# test_df <- plot_stat_df %>% filter(Country == "India")
# 
# hist_df <- bmi_param_as_df %>%
#   filter(ISO3.Code == "IND") %>%
#   mutate(`P[0,17]` = plnorm(17, mu, sigma),
#          `P[17,18.5]` = plnorm(18.5, mu, sigma) - plnorm(17, mu, sigma),
#          `P[18.5,20]` = plnorm(20, mu, sigma) - plnorm(18.5, mu, sigma),
#          `P[20,25]` = plnorm(25, mu, sigma) - plnorm(20, mu, sigma),
#          `P[25,30]` = plnorm(30, mu, sigma) - plnorm(25, mu, sigma),
#          `P[30,35]` = plnorm(35, mu, sigma) - plnorm(30, mu, sigma),
#          `P[35,40]` = plnorm(40, mu, sigma) - plnorm(35, mu, sigma),
#          `P[40,Inf]` = plnorm(Inf, mu, sigma) - plnorm(40, mu, sigma)) %>%
#   pivot_longer(cols=`P[0,17]`:`P[40,Inf]`) %>%
#   mutate(Med = value, BMIGroup=name)
# 
# p1 <- ggplot() +
#   geom_line(data = test_df, aes(x=Year, y=Med, colour=SSP)) +
#   geom_line(data = hist_df, aes(x=Year, y=Med)) +
#   facet_wrap(~BMIGroup) +
#   scale_y_continuous(name="Ensemble Median") +
#   theme_bw(base_size = 12) 
# 
# p1


