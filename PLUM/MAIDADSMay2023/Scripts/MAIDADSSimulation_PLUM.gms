$ontext
Simulation of the MAIDADS function calibrated using the estimation from NLSUR.gms
$offtext

$if not set BS $set BS 0

$ondotl

$gdxin ./GAMS-input/GDX/MAIDADS_Estimation0_PLUM.gdx
SET
  c
  i
  ifood(i)
  iter     Index iterations on GDP / 1*300 /
  type               / Vegetal, Animal /
;
$load c, i, ifood

ALIAS (i,j,ii);

POSITIVE VARIABLE
  DiscretionaryC(i)  Discretionary consumption
  SubsistenceC(i)    Subsistence consumption
  w(i)               Budget share by commodity
;

VARIABLE
  u            Utility
  alpha(i)     Parameter of MAIDADS
  beta(i)      Parameter of MAIDADS
  kappa        Parameter of MAIDADS
  delta(i)     Parameter of MAIDADS
  tau(i)       Parameter of MAIDADS
  omega        Parameter of MAIDADS
  error
  ressq
;
$load  alpha, beta, kappa, delta, tau, omega

PARAMETER
  p(i)                Price /
$include ./GAMS-input/MeanPrices.gms
   /
  m                     GDP per capita
  eta(iter,i)           Engel elasticities
  etakcal(iter)         Engel elasticities for all calories
  eta_Type(type,iter)
  elastprice(iter,i,j)  Price elasticities
  elastownprice(iter,i) Own-price elasticities
  lambda                Lagrange multiplier on the budget constraint
  mu(i)                 Parameter used in the calculation of lambda
  mc(c)                 GDP per capita by country
  miter(iter)           Vector of GDP per capita (equally space on a log scale) /
$include ./GAMS-input/GDPperCapSimulation.gms
            /
  witer(iter,i)         Vector of budget share
  uiter(iter)           Vector of utility
  xiter(iter,i)         Vector of consumed quantities
  DiscretionaryCiter(iter,i)
  SubsistenceCiter(iter,i)
  Calories(iter,i)              Vector of calories consumption
  TotalCalories(iter)           Vector of total calories consumption
  TotalCalories_Type(type,iter) Vector of total calories consumption by type of calories
  CoefType(*,i,type) /
*$include ../Data/GAMS-input/CoefType.gms
/
;

$load m=mmean, mc=m

display p;

PARAMETER pAdj(i);
pAdj(i) = p(i);

w.up(j) = 1;

* Fix parameters at their estimated values
alpha.fx(i) = alpha.l(i);
beta.fx(i)  = beta.l(i);
kappa.fx    = kappa.l;
delta.fx(i) = delta.l(i);
tau.fx(i)   = tau.l(i);
omega.fx    = omega.l;

*alpha.fx("3008") = 0.001;
*alpha.fx("3010") = 0.001;
*alpha.fx("5000") = 1 - sum(ifood, alpha(ifood));

EQUATIONS
  EQ_w(i)      "Definition of budget share"
  EQ_u         "Implicit definition of utility"
  EQ_DiscretionaryC(i)
  EQ_SubsistenceC(j)  
  EQ_u_NLS     "Relaxed constraint for NLS"
  EQ_ressq_NLS
;

EQ_w(j)..
  w(j) =e= (SubsistenceC(j)+DiscretionaryC(j))*pAdj(j)/m;

EQ_u..
  sum(i, (alpha(i)+beta(i)*exp(u))/(1+exp(u))*log(DiscretionaryC(i)))-u =e= kappa ;
  
EQ_u_NLS ..
  sum(i, (alpha(i)+ beta(i)* exp(u)) / (1 + exp(u)) * log(DiscretionaryC(i))) - u =e= kappa + error;

EQ_DiscretionaryC(i).. DiscretionaryC(i) =e=
    [(alpha(i)+beta(i)*exp(u))/(1+exp(u))]/pAdj(i)*
    [m-sum(j,pAdj(j)*SubsistenceC(j))];

EQ_SubsistenceC(j)..
  SubsistenceC(j) =e= (delta(j)+tau(j)*exp(omega*u))/(1+exp(omega*u));
  
EQ_ressq_NLS ..
  ressq =e= sqr(error);

model MAIDADS_Sim "MAIDADS model for simulation" /
      EQ_w.w
      EQ_u.u
      EQ_DiscretionaryC.DiscretionaryC
      EQ_SubsistenceC.SubsistenceC
 / ;
 
model MAIDADS_Sim_NLP /
      EQ_w
      EQ_u_NLS
      EQ_DiscretionaryC
      EQ_SubsistenceC
      EQ_ressq_NLS
/ ;


MAIDADS_Sim.holdfixed = 1;

option cns=path;

u          = 0;
SubsistenceC(j)  = (delta(j)+tau(j)*exp(omega*u))/(1+exp(omega*u));
DiscretionaryC(i) = [(alpha(i)+beta(i)*exp(u))/(1+exp(u))]/pAdj(i)*
    [m-sum(j,pAdj(j)*SubsistenceC(j))];
DiscretionaryC.LO(i) = 1E-10;
SubsistenceC.LO(j) = 1E-11;
w(j)       = (SubsistenceC(j)+DiscretionaryC(j))*pAdj(j)/m;

display miter;

Parameter minBundle;

scalar k;

*tau.fx("3001") = 0.53;
*tau.fx("3002") = 0.075;
*tau.fx("3003") = 0.14;
*tau.fx("3005") = 0.045;
*tau.fx("3006") = 0.30;
*tau.fx("3008") = 0.04;
*tau.fx("3010") = 0.03;


loop(iter,
  m = miter(iter);
  pAdj("5000") = (45.52 + 86.29 * exp(1.87 * (log(m) - 10.37))) / (1 + exp(1.87 * (log(m) - 10.37))) * 100;
  
*  k = 1 - max((50000 - m) / 50000, 0);
k = 1;
  
*  alpha.fx("3001") = 0.000001 * (1 - k) + 0.0000010 * k;
*  alpha.fx("3002") = 0.0000903286 * (1 - k) + 0.0000371 * k;
*  alpha.fx("3003") = 0.00604078 * (1 - k) + 0.0064518 * k;
*  alpha.fx("3005") = 0.000116057 * (1 - k) + 0.0011244 * k;
*  alpha.fx("3006") = 0.000690987 * (1 - k) + 0.0012265 * k;
*  alpha.fx("3008") = 0.0162619 * (1 - k) + 0.0087 * k;
*  alpha.fx("3010") = 0.00998629 * (1 - k) + 0.0033 * k;

*  alpha.fx("5000") = 1 - sum(ifood, alpha(ifood));

  u = 0;
  SubsistenceC(j)  = (delta(j)+tau(j)*exp(omega*u))/(1+exp(omega*u));
  DiscretionaryC(i) = [(alpha(i)+beta(i)*exp(u))/(1+exp(u))]/pAdj(i)*
    [m-sum(j,pAdj(j)*SubsistenceC(j))];
  w(j)       = (SubsistenceC(j)+DiscretionaryC(j))*pAdj(j)/m;

  
* In case food expenditure exceeds income, adjust income otherwise solver will fail
  minBundle = m - sum(i, delta(i) * pAdj(i));
  if ((minBundle < 0), m = m - minBundle + 1);
    
  solve MAIDADS_Sim_NLP using nlp minimising ressq;
  solve MAIDADS_Sim using cns;
  if ((MAIDADS_Sim.modelstat ne 16),
        solve MAIDADS_Sim_NLP using nlp minimising ressq;
  );
  witer(iter,i) = w.l(i);
  uiter(iter)   = u.l;
  xiter(iter,i) = w.l(i)*m/pAdj(i);
  DiscretionaryCiter(iter,i) = DiscretionaryC(i);
  SubsistenceCiter(iter,i) = SubsistenceC(i);

* Elasticities
  mu(i) = 0;
  mu(i)$(alpha(i) or beta(i)) = (beta(i)-alpha(i))*exp(u)/power(1+exp(u),2)*log(xiter(iter,i)-SubsistenceC(i));
  lambda = -power(sum(i,mu(i)-[(alpha(i)+beta(i)*exp(u))/(1+exp(u))*power(xiter(iter,i)-SubsistenceC(i),-1)*((tau(i)-delta(i))*omega*exp(omega*u))/power(1+exp(omega*u),2)]$(alpha(i) or beta(i)))-1,-1)*power(m-sum(i,pAdj(i)*SubsistenceC(i)),-1);
  eta(iter,i) = (1/w(i))*((alpha(i)+beta(i)*exp(u))/(1+exp(u))+(m-sum(j,pAdj(j)*SubsistenceC(j)))*(beta(i)-alpha(i))*exp(u)*lambda/(1+exp(u))**2+(tau(i)-delta(i))*omega*exp(omega*u)*pAdj(i)*lambda/(1+exp(omega*u))**2-(alpha(i)+beta(i)*exp(u))/(1+exp(u))*sum(j,(tau(j)-delta(j))*omega*exp(omega*u)*pAdj(j)/(1+exp(omega*u))**2)*lambda);
  elastprice(iter,i,j)  = [[(alpha(i)+beta(i)*exp(u))/(1+exp(u))]*[m-sum(ii,pAdj(ii)*SubsistenceC(ii))]/(w(i)*m)]*([(alpha(j)+beta(j)*exp(u))/(1+exp(u))] - 1$sameas(i,j)) - w(j)*eta(iter,i);
  elastownprice(iter,i) = elastprice(iter,i,i);
  abort$(abs(sum(i,eta(iter,i)*w(i))-1)>=1E-6) "Engel elasticities do not respect Engel aggregation";
  loop(i,
    abort$(abs(sum(j,w(j)*elastprice(iter,j,i))+w(i))>=1E-6) "Price elasticities do not respect Cournot aggregation";
    abort$(abs(sum(j,elastprice(iter,i,j))+eta(iter,i))>=1E-6) "Elasticities do not respect homogeneity condition";
  );
);
etakcal(iter) = sum(ifood,eta(iter,ifood)*xiter(iter,ifood))/sum(ifood,xiter(iter,ifood));
*eta_Type(type,iter) = sum(ifood,eta(iter,ifood)*CoefType("5000",ifood,type)*xiter(iter,ifood))/sum(ifood,CoefType("5000",ifood,type)*xiter(iter,ifood));

Calories(iter,ifood) = xiter(iter,ifood)*2000;
TotalCalories(iter)  = sum(ifood,Calories(iter,ifood));
*TotalCalories_Type(type,iter) = sum(ifood,CoefType("5000",ifood,type)*Calories(iter,ifood));

EXECUTE_UNLOAD './GAMS-input/GDX/MAIDADS_Simulation%BS%_PLUM.gdx';
