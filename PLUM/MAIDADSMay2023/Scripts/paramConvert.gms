$ondotl

Set j;

PARAMETER
  alphaP(j)     Parameter of MAIDADS
  betaP(j)      Parameter of MAIDADS
  kappaP        Parameter of MAIDADS
  deltaP(j)     Parameter of MAIDADS
  tauP(j)       Parameter of MAIDADS
  omegaP        Parameter of MAIDADS
;

$gdxin Parameters.gdx
$load j, alphaP, betaP, kappaP, deltaP, tauP, omegaP
$gdxin

Set plumi / CerealsStarchyRoots, Pulses, Oilcrops, Sugar, FruitVeg, Monogastrics, Ruminants, Nonfood /;
Alias(i, plumi);

Parameter
alpha(plumi)
beta(plumi)
delta(plumi)
tau(plumi)
kappa
omega
;

alpha("CerealsStarchyRoots") = alphaP("3001");
alpha("Sugar") = alphaP("3002");
alpha("FruitVeg") = alphaP("3003");
alpha("Pulses") = alphaP("3005");
alpha("Oilcrops") = alphaP("3006");
alpha("Ruminants") = alphaP("3008");
alpha("Monogastrics") = alphaP("3010");
alpha("Nonfood") = alphaP("5000");

beta("CerealsStarchyRoots") = betaP("3001");
beta("Sugar") = betaP("3002");
beta("FruitVeg") = betaP("3003");
beta("Pulses") = betaP("3005");
beta("Oilcrops") = betaP("3006");
beta("Ruminants") = betaP("3008");
beta("Monogastrics") = betaP("3010");
beta("Nonfood") = betaP("5000");

delta("CerealsStarchyRoots") = deltaP("3001");
delta("Sugar") = deltaP("3002");
delta("FruitVeg") = deltaP("3003");
delta("Pulses") = deltaP("3005");
delta("Oilcrops") = deltaP("3006");
delta("Ruminants") = deltaP("3008");
delta("Monogastrics") = deltaP("3010");
delta("Nonfood") = deltaP("5000");

tau("CerealsStarchyRoots") = tauP("3001");
tau("Sugar") = tauP("3002");
tau("FruitVeg") = tauP("3003");
tau("Pulses") = tauP("3005");
tau("Oilcrops") = tauP("3006");
tau("Ruminants") = tauP("3008");
tau("Monogastrics") = tauP("3010");
tau("Nonfood") = tauP("5000");


omega = omegaP;
kappa = kappaP;

EXECUTE_UNLOAD "DemandParamConv" i, alpha, beta, delta, tau, omega, kappa;