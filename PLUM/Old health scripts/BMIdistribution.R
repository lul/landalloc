### Cross-Entropy estimation for log-normal distritbtion for the relationship between
### mean BMI and population distribution of BMI (to get % underweight, overweight, obesity)
## 21 May 2019 - author: FCossar
## following the method of Springmann et al. https://doi.org/10.1016/S0140-6736(15)01156-3
## WHO data on mean BMI, prevalence of overweight, obese, and underweight (all age-adjusted BMI measure)
## downloaded from WHO Global Health Observatory Database: https://www.who.int/gho/database/en/
###----------------------------------------------------------------------------###


library("tidyverse")
library("data.table")
library("doBy")
library("CEoptim")
library("LaplacesDemon")


WHObmi <- fread(file.path(r_script_root,"InputData","Health","WHOmeanBMI.csv"),header=TRUE)
WHObmi <- data.table(gather(WHObmi,key="Year",value="meanBMI",2:43))
WHObmi[, `Year`:=as.numeric(`Year`)]

WHOobesity <- fread(file.path(r_script_root,"InputData","Health","WHOobesity.csv"),header=TRUE)
WHOobesity <- data.table(gather(WHOobesity,key="Year",value="30",2:41))
WHOobesity[, `Year`:=as.numeric(`Year`)]

WHOover <- fread(file.path(r_script_root,"InputData","Health","WHOoverweight.csv"),header=TRUE)
WHOover <- data.table(gather(WHOover,key="Year",value="25",2:43))
WHOover[, `Year`:=as.numeric(`Year`)]

WHOunder <- fread(file.path(r_script_root,"InputData","Health","WHOunderweight.csv"),header=TRUE)
WHOunder <- data.table(gather(WHOunder,key="Year",value="18.5",2:43))
WHOunder[, `Year`:=as.numeric(`Year`)]

WHO <- merge(WHObmi,WHOobesity)
WHO <- merge(WHO,WHOover)
WHO <- merge(WHO,WHOunder)

WHO <- data.table(gather(WHO,key="gamma",value="prob", "30":"18.5"))

# need to estimate mu and sigma for each country, using 2010 observations, to describe a log-normal distrubution
# estimated mu and sigma: minimize the K-L divergence

results = NULL

for (G in unique(WHO$Country)){
  
WHOtest <- WHO[WHO$Country==G & WHO$Year=="2010",]
WHOtest$prob <- WHOtest$prob/100

  p1 <- WHOtest$prob[WHOtest$gamma=="18.5"]
  p2 <- (1-WHOtest$prob[WHOtest$gamma=="25"])-WHOtest$prob[WHOtest$gamma=="18.5"]
  p3 <- (1-WHOtest$prob[WHOtest$gamma=="30"])-(1-WHOtest$prob[WHOtest$gamma=="25"])
  p4 <- WHOtest$prob[WHOtest$gamma=="30"]
fun <- function(par){
  ( (p1*log(p1/(plnorm(18.5, meanlog = par[1], sdlog = par[2], log = FALSE)))+
     p2*log(p2/(plnorm(25, meanlog = par[1], sdlog = par[2], log = FALSE)-plnorm(18.5, meanlog = par[1], sdlog = par[2], log = FALSE)
             ))
  + p3*log(p3/(plnorm(30, meanlog = par[1], sdlog = par[2], log = FALSE)-plnorm(25, meanlog = par[1], sdlog = par[2], log = FALSE)))
  + p4*log(p4/(1-plnorm(30, meanlog = par[1], sdlog = par[2], log = FALSE)))
  + 0.5*log(log(unique(WHOtest$meanBMI))/par[1])
    )^2)^(1/2)
  }

res <- optim(par=c(3,.3),fn=fun)

x<-seq(0,50,.1)
#print(plot(dlnorm(x,meanlog=res$par[1],sdlog=res$par[2])))

#WHOtest$predProb[WHOtest$gamma=="30"] <- 1-plnorm(30,meanlog=res$par[1],sdlog=res$par[2])
#WHOtest$predProb[WHOtest$gamma=="25"] <-1-plnorm(25,meanlog=res$par[1],sdlog=res$par[2])
#WHOtest$predProb[WHOtest$gamma=="18.5"] <-plnorm(18.5,meanlog=res$par[1],sdlog=res$par[2])
#WHOtest$predMeanBMI <- exp(res$par[1])
R <- data.table(Country=G,mu=res$par[1],sigma=res$par[2])
#WHOtest$KLvalue <- res$value

 results <- rbind(results,R)
}

## output for PLUM purposes
# merging in PLUM country names
countrymap <- data.table(read_csv(file.path(r_script_root,"InputData","countryMapping.csv")))
names(results)[1]<-"whoCountry"
results <- merge(countrymap,results)

write.csv(results[is.na(Country)==FALSE,c("Country","mu","sigma")],file.path(r_script_root,"InputData","Health","BMIdistributions.csv"),row.names=FALSE)


### look at sigma over time, to check if reasonable to assume that sigma is constant
sigmaTrend = NULL

for (G in unique(WHO$Country)){
  for (Y in unique(WHO$Year)) {
  
  WHOtest <- WHO[WHO$Country==G & WHO$Year==Y,]
  WHOtest$prob <- WHOtest$prob/100
  
  p1 <- WHOtest$prob[WHOtest$gamma=="18.5"]
  p2 <- (1-WHOtest$prob[WHOtest$gamma=="25"])-WHOtest$prob[WHOtest$gamma=="18.5"]
  p3 <- (1-WHOtest$prob[WHOtest$gamma=="30"])-(1-WHOtest$prob[WHOtest$gamma=="25"])
  p4 <- WHOtest$prob[WHOtest$gamma=="30"]
  fun <- function(par){
    ( (p1*log(p1/(plnorm(18.5, meanlog = par[1], sdlog = par[2], log = FALSE)))+
         p2*log(p2/(plnorm(25, meanlog = par[1], sdlog = par[2], log = FALSE)-plnorm(18.5, meanlog = par[1], sdlog = par[2], log = FALSE)
         ))
       + p3*log(p3/(plnorm(30, meanlog = par[1], sdlog = par[2], log = FALSE)-plnorm(25, meanlog = par[1], sdlog = par[2], log = FALSE)))
       + p4*log(p4/(1-plnorm(30, meanlog = par[1], sdlog = par[2], log = FALSE)))
       + 0.5*log(log(unique(WHOtest$meanBMI))/par[1])
    )^2)^(1/2)
  }
  
  
  res <- optim(par=c(3,.3),fn=fun)
  
  x<-seq(0,50,.1)

  #WHOtest$predProb[WHOtest$gamma=="30"] <- 1-plnorm(30,meanlog=res$par[1],sdlog=res$par[2])
  #WHOtest$predProb[WHOtest$gamma=="25"] <-1-plnorm(25,meanlog=res$par[1],sdlog=res$par[2])
  #WHOtest$predProb[WHOtest$gamma=="18.5"] <-plnorm(18.5,meanlog=res$par[1],sdlog=res$par[2])
  #WHOtest$predMeanBMI <- exp(res$par[1])
  R <- data.table(Country=G, Year =Y, mu=res$par[1],sigma=res$par[2])
  #WHOtest$KLvalue <- res$value
  
  sigmaTrend <- rbind(sigmaTrend,R)
}
}