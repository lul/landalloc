# Read FAO data and do some analysis
#
# Author: peteralexander
###############################################################################

if(!require(data.table)){install.packages("data.table")}

data_dir_root = "~/Documents/LURG/Data/FAOStat-Dec2015"

getRegions = function() {
	c("East Asia & Pacific", "Europe & Central Asia", "Latin America & Caribbean", "Middle East & North Africa", "North America", "South Asia", "Sub-Saharan Africa")
}

getCountries = function(fao_table) {
	if ("Country Code" %in% names(fao_table))
		setnames(fao_table, "Country Code", "CountryCode")
		
	fao_table[,list(v=1), by=list(Country, CountryCode)][!like(Country,"Total") & !(like(Country, "China") & !Country == "China")]
}

getCountryCodes = function() {
	cc = fread(paste0(data_dir_root, "/../SSP/country_codes2.csv"), header=TRUE)
	cc[, SpecialNotes:=NULL]
	cc
}

#getUSCpi = function() {
#	fread(paste0(data_dir_root, "/US_CPI.csv"), header=TRUE)
#}

getFeedConvRatio  = function() {
	data.table(
			productionItem=c("Milk Total + (Total)", "Eggs Primary + (Total)", "Meat Poultry + (Total)", "Beef and Buffalo Meat + (Total)", "Sheep and Goat Meat + (Total)", "Meat pig", "Meat Other"),
			consumptionItem=c("Milk Whole", "Eggs", "Poultry Meat", "Bovine Meat", "Mutton & Goat Meat", "Pigmeat", "Meat Other"),
			fcr=c(0.6, 2, 4.5, 25, 12, 9, 12))
}

moving_avg = function(x, n=5){
	filter(x,rep(1/n,n), sides=1)
}

readCountryData = function() {
	########## Land
	area_data = fread(paste0(data_dir_root, "/Inputs_Land_E_All_Data_Norm.csv"))
	setnames(area_data, "Country Code", "CountryCode")
	setkey(area_data, Country, Year)
	countries = getCountries(area_data)
	
	cd = CJ(Country=countries$Country, Year=(1961:2011))
	cd = cd[area_data[Item=="Country area"],total:=Value]
	cd = cd[area_data[Item=="Land area"],land:=Value]
	cd = cd[area_data[Item=="Agricultural area"],agri:=Value]
	cd = cd[area_data[Item=="Agricultural area irrigated"],irrigated:=Value]
	cd = cd[area_data[Item=="Arable land"],arable:=Value]
	cd = cd[area_data[Item=="Fallow land"],fallow:=Value]
	cd = cd[area_data[Item=="Forest area"],forest:=Value]
	cd = cd[area_data[Item=="Permanent crops"],perm_crops:=Value]
	cd = cd[area_data[Item=="Permanent meadows and pastures"],perm_pasture:=Value]
	cd = cd[area_data[Item=="Temporary crops"],temp_crops:=Value]
	cd = cd[area_data[Item=="Temporary meadows and pastures"],temp_pasture:=Value]
	cd = cd[area_data[Item=="Arable land and Permanent crops"],arable_and_perm:=Value]
	cd = cd[area_data[Item=="Other land"],other:=Value]
	
	cd = cd[!is.na(total) | !is.na(land) | !is.na(agri) | !is.na(irrigated) | !is.na(arable) | !is.na(fallow) | !is.na(forest) | !is.na(perm_crops) | !is.na(perm_pasture) | !is.na(arable_and_perm)]
		
	########## Fertiliser
#	f1 = fread(paste0(data_dir_root, "/Resources_Fertilizers_E_All_Data.csv"))
#	f2 = fread(paste0(data_dir_root, "/Resources_FertilizersArchive_E_All_Data.csv"))
	
#	f1 = f1[Item=="Nitrogen Fertilizers (N total nutrients)" & Element == "Consumption in nutrients", list(Country, Year, Value)]
#	f2 = f2[Item=="Nitrogenous fertilizers" & Element == "Consumption", list(Country, Year, Value)]
	
#	f_all = rbind(f1, f2)
#	setkey(f_all, Country, Year)
#	cd = cd[f_all,fertiliser_n:=Value]
	
	########## Historical Population
	pop_data = fread(paste0(data_dir_root, "/Population_E_All_Data_Norm.csv"))
	setkey(pop_data, Country, Year)
	countries = getCountries(pop_data)
	
	pop =  CJ(Country=countries$Country, Year=(1961:2050))
	pop = pop[pop_data[Element =="Total Population - Both sexes"],total_pop:=Value]
	pop = pop[pop_data[Element =="Rural population"],rural:=Value]
	pop = pop[pop_data[Element =="Urban population"],urban:=Value]
	pop = pop[pop_data[Element =="Total economically active population"],econ_active:=Value]
	pop = pop[pop_data[Element =="Total economically active population in Agr"],agri_econ:=Value]
	
	cd[pop, population:=total_pop]
	
	########## Historical GDP
	wb_data = fread(paste0(data_dir_root, "/../WorldBank/ny.gdp.mktp.kd_Indicator_en_csv_v2.csv"), header=TRUE)
	setkey(wb_data, 'Country Code')
			
	wb_data[, 'Country Name':=NULL]
	country_codes = getCountryCodes()
	setkey(country_codes, CountryCode)
	wb_data=wb_data[country_codes,Country:=Country][!is.na(Country)]
	wb_data=wb_data[country_codes,Region:=Region]
	wb_data=wb_data[country_codes,IncomeGroup:=IncomeGroup]
	wb_data=wb_data[country_codes,CountryCode:=CountryCode]
	
	gdp = reshape(wb_data, varying = c("1960", "1961", "1962", "1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972", "1973", "1974", "1975", "1976", "1977", "1978", "1979",
					"1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", 
					"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013"), 
			v.names = "Value",
			timevar = "Year", 
			times = c(1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979,
					1980, 1981, 1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999,
					2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013), 
			direction = "long")
	wb_data = NULL
	
	gdp = gdp[, list (Country=Country, Year=Year, Region=Region, CountryCode=CountryCode, IncomeGroup=IncomeGroup, Unit='constant 2005 US$', Value=as.numeric(Value)/1000000)]
	setkey(gdp, Country, Year)
	setkey(country_codes, Country)
	
	cd[gdp, gdp:=Value]
	cd[country_codes, c('Region', 'CountryCode', 'IncomeGroup') := list(Region, CountryCode, IncomeGroup)]
	
	cd[, gdp_pc:= gdp/population*1000]
	cd[is.na(cd)] = 0
	
	# sudan - aggregate data in 2011 back to former Sudan
	cd = rbind(cd, cd[Year==2011 & (Country=="South Sudan" | Country=="Sudan"), 
					list(Country="Sudan (former)", Year=2011, 
							total=sum(total), land=237600, agri=sum(agri), irrigated=sum(irrigated), arable=sum(arable),
							fallow=sum(fallow), forest=69949.0, perm_crops=sum(perm_crops), perm_pasture=sum(perm_pasture),
							temp_crops=sum(temp_crops), temp_pasture=sum(temp_pasture), arable_and_perm=sum(arable_and_perm), other=31406.0,
							#fertiliser_n=sum(fertiliser_n), 
							population=sum(population),
							gdp_pc=sum(gdp)/sum(population)*1000, gdp=sum(gdp),
							Region="Sub-Saharan Africa", CountryCode="SDNF", IncomeGroup="Lower middle income")],
			use.names=TRUE)		
	cd = cd[!((Country=="South Sudan" | Country=="Sudan") & Year == 2011)]
	cd
}

########## Commodities balance
readCommoditiyBalance = function() {
	
	processCommoditiyBalance = function(fao_file) {
		fao_data =fread(fao_file)
		
		setkey(fao_data, Country, Item, Year)
		
		commodities = fao_data[,list(v=1), by=list(Item, get("Item Code"))][order(Item)]
		countries = getCountries(fao_data)
		
		result = CJ(Country=countries$Country, Item=commodities$Item, Year=(1961:2012))
		setkey(result, Country, Item, Year)
				
		result = result[fao_data[Element=="Domestic supply quantity"],supply:=Value]
		result = result[fao_data[Element=="Seed"],seed:=Value]
		result = result[fao_data[Element=="Waste"],waste:=Value]
		result = result[fao_data[Element=="Processing"],processing:=Value]
		result = result[fao_data[Element=="Food supply quantity (tonnes)"],food:=Value]
		result = result[fao_data[Element=="Feed"],feed:=Value]
		result = result[fao_data[Element=="Production"],prod:=Value]
		result = result[fao_data[Element=="Import Quantity"],import:=Value]
		result = result[fao_data[Element=="Stock Variation"],stockvar:=Value]
		result = result[fao_data[Element=="Export Quantity"],export:=Value]
		result = result[fao_data[Element=="Import Quantity"],import:=Value]
		result = result[fao_data[Element=="Other uses"],other:=Value]
		result = result[!is.na(supply) | !is.na(seed) | !is.na(waste) | !is.na(processing) | !is.na(food) | !is.na(prod) | !is.na(import) | !is.na(feed) | !is.na(export) | !is.na(other)] 
		result[is.na(result)] = 0
		result
	}
	
	cb = rbind(processCommoditiyBalance(paste0(data_dir_root, "/CommodityBalances_Crops_E_All_Data_Norm.csv")),
			processCommoditiyBalance(paste0(data_dir_root, "/CommodityBalances_LivestockFish_E_All_Data_Norm.csv")))
	setkey(cb, Country, Year, Item)
	cb
}

########## Crop production
readCropProd = function() {
	crop_prod_data = fread(paste0(data_dir_root, "/Production_Crops_E_All_Data_Norm.csv"))
	setkey(crop_prod_data, Country, Item, Year)
	
	commodities = crop_prod_data[,list(v=1), by=list(Item, get("Item Code"))][order(Item)]
	countries = getCountries(crop_prod_data)
	
	cp = CJ(Country=countries$Country, Item=commodities$Item, Year=(1961:2012))
	cp = cp[crop_prod_data[Element=="Area harvested"],area:=Value]
	cp = cp[crop_prod_data[Element=="Yield"],yield:=Value/10000]
	cp = cp[crop_prod_data[Element=="Production"],prod:=Value]
	cp = cp[!is.na(area) | !is.na(prod) | !is.na(yield)]
	cp[is.na(cp)] = 0
	cp
}

########## Animal production
readAnimalProd = function() {
	animal_prod_data = fread(paste0(data_dir_root, "/Production_LivestockPrimary_E_All_Data_Norm.csv"))
	setkey(animal_prod_data, Country, Item, Year)
	
	countries = getCountries(animal_prod_data)
	# These item names have changed, so code doesn't work any more
	commodities = data.table(Item=c("Milk Total + (Total)", "Eggs Primary + (Total)", "Meat Poultry + (Total)", "Beef and Buffalo Meat + (Total)", "Sheep and Goat Meat + (Total)", "Meat pig"))
	
	ap = CJ(Country=countries$Country, Item=commodities$Item, Year=(1961:2012))
	ap = ap[animal_prod_data[Element=="Yield/Carcass Weight" | Element=="Yield"],yield:=Value]
	ap = ap[animal_prod_data[Element=="Production"],prod:=Value]
	ap = ap[!is.na(yield) | !is.na(prod)]
	
	fcrDT = getFeedConvRatio()[, list(Item = productionItem, fcr)]
	ap = merge(ap, fcrDT, by="Item")[, list(Country, Item, Year, yield, prod, feedReq=fcr*prod)]
	setkey(ap, Country, Item, Year)
	ap
}

########## Prices
#prices = fread(paste0(data_dir_root, "/Prices_E_All_Data.csv"))[Element=="Producer Price (US $/tonne)"]
#setkey(prices, Country, Item, Year)


########## Prices
#trade = fread(paste0(data_dir_root, "/Trade_Crops_Livestock_E_All_Data.csv"))


########## # Belgium-Luxembourg
combineCountries = function(countriesToCombine, newCountry, newCountryCode) {
	assign('com_bal', rbind(com_bal[!Country %in% countriesToCombine],	
					com_bal[Country %in% countriesToCombine, 
							list(Country=newCountry, supply=sum(supply,na.rm=TRUE), seed=sum(seed,na.rm=TRUE), waste=sum(waste,na.rm=TRUE), 
									processing=sum(processing,na.rm=TRUE),food=sum(food,na.rm=TRUE), feed=sum(feed,na.rm=TRUE), 
									prod=sum(prod,na.rm=TRUE), import=sum(import,na.rm=TRUE), stockvar=sum(stockvar,na.rm=TRUE), 
									export=sum(export,na.rm=TRUE), other=sum(other,na.rm=TRUE)), by=list(Item, Year)], 
					use.names=TRUE), envir = .GlobalEnv)
	
	assign('crop_prod', rbind(crop_prod[!Country %in% countriesToCombine],
					crop_prod[Country %in% countriesToCombine & prod >0, 
							list(Country=newCountry, yield=sum(prod)/sum(area)*10000, prod=sum(prod), area=sum(area)), by=list(Item, Year)], 
					use.names=TRUE), envir = .GlobalEnv)
	
	assign('animal_prod', rbind(animal_prod[!Country %in% countriesToCombine],
					animal_prod[Country %in% countriesToCombine & prod >0, 
							list(Country=newCountry, yield=sum(yield*prod)/sum(prod), prod=sum(prod), feedReq=sum(feedReq)), by=list(Item, Year)], 
					use.names=TRUE), envir = .GlobalEnv)
	
	assign('country_data', rbind(country_data[!Country %in% countriesToCombine],
					country_data[Country %in% countriesToCombine, 
							list(Country=newCountry, CountryCode=newCountryCode, gdp_pc=sum(gdp,na.rm=TRUE)/sum(population,na.rm=TRUE)*1000, total=sum(total,na.rm=TRUE), 
									land=sum(land,na.rm=TRUE),agri=sum(agri,na.rm=TRUE), irrigated=sum(irrigated,na.rm=TRUE), arable=sum(arable,na.rm=TRUE), 
									fallow=sum(fallow,na.rm=TRUE), forest=sum(forest,na.rm=TRUE), perm_crops=sum(perm_crops,na.rm=TRUE), 
									perm_pasture=sum(perm_pasture,na.rm=TRUE), temp_crops=sum(temp_crops,na.rm=TRUE), temp_pasture=sum(temp_pasture,na.rm=TRUE), 
									arable_and_perm=sum(arable_and_perm,na.rm=TRUE), other=sum(other,na.rm=TRUE), 
									#fertiliser_n=sum(fertiliser_n,na.rm=TRUE),
									population=sum(population,na.rm=TRUE), gdp=sum(gdp,na.rm=TRUE)), by=list(Year, Region, IncomeGroup)], 
					use.names=TRUE), envir = .GlobalEnv)
}

country_data = readCountryData()
com_bal = readCommoditiyBalance()
crop_prod = readCropProd()
animal_prod = readAnimalProd()

combineCountries(c('Belgium-Luxembourg', 'Luxembourg', 'Belgium'), 'Belgium', 'BEL')
combineCountries(c('Serbia and Montenegro', 'Serbia', 'Montenegro'), 'Serbia', 'SRB')

########## # USSR - 15 of them and Ethiopia/Eritrea
splitting_states = data.table (oldC="USSR", newC = c("Armenia", "Azerbaijan", "Belarus", "Estonia", "Georgia", "Kazakhstan", "Kyrgyzstan", "Latvia", 
		"Lithuania", "Republic of Moldova", "Russian Federation", "Tajikistan", "Turkmenistan", "Ukraine", "Uzbekistan"), convYear = 1992)

splitting_states = rbind(splitting_states, data.table(oldC="Ethiopia PDR", newC = c("Ethiopia", "Eritrea"), convYear = 1993))

setkey(splitting_states, newC, convYear)
setkey(country_data, Country, Year)
splitting_states = country_data[splitting_states][, list(newC=Country, convYear=Year, pop_ratio=population/sum(population)), by=oldC]

# for some reason there are some 0 rows for this in the dataset
crop_prod = crop_prod[!((Country %in% splitting_states[oldC=="USSR"]$newC & Year < 1992) | (Country %in% splitting_states[oldC=="Ethiopia PDR"]$newC & Year < 1993))] 
animal_prod = animal_prod[!((Country %in% splitting_states[oldC=="USSR"]$newC & Year < 1992) | (Country %in% splitting_states[oldC=="Ethiopia PDR"]$newC & Year < 1993))] 

adjustValue = function (x, y) { x * y}
convert_com_bal = function (newC, factor, dt, colNames) {
	dt$Country = newC
	adjusted = dt[, lapply(.SD, adjustValue, y=factor), .SDcols = colNames]
	for (i in length(colNames):1) {
		dt[,colNames[i] := NULL]
	}
	cbind(dt, adjusted)
}

for (i in 1:length(splitting_states$newC)) {
	ss = splitting_states[i]
	print(ss$newC)
	com_bal = rbind(convert_com_bal(ss$newC, ss$pop_ratio, com_bal[Country == ss$oldC & Year <= ss$convYear], 4:14), com_bal, use.names=TRUE)
	crop_prod = rbind(convert_com_bal(ss$newC, ss$pop_ratio, crop_prod[Country == ss$oldC & Year <= ss$convYear], c("area", "prod")), crop_prod, use.names=TRUE)
	animal_prod = rbind(convert_com_bal(ss$newC, ss$pop_ratio, animal_prod[Country == ss$oldC & Year <= ss$convYear], c("prod", "feedReq")), animal_prod, use.names=TRUE)
	country_data <- rbind(convert_com_bal(ss$newC, ss$pop_ratio, country_data[Country == ss$oldC & Year <= ss$convYear], 
	                 c('agri','arable','forest','land', 'other', 'perm_crops', 'perm_pasture', 'temp_crops', 
	                         'temp_pasture', 'irrigated','population', 'gdp')), country_data, use.names=TRUE)
}

com_bal = com_bal[!((Country == "USSR" & Year < 1992) | (Country == "Ethiopia PDR" & Year < 1993))] 
crop_prod = crop_prod[!((Country == "USSR" & Year < 1992) | (Country == "Ethiopia PDR" & Year < 1993))] 
country_data = country_data[!((Country == "USSR" & Year < 1992) | (Country == "Ethiopia PDR" & Year < 1993))] 
animal_prod = animal_prod[!((Country == "USSR" & Year < 1992) | (Country == "Ethiopia PDR" & Year < 1993))]


com_bal_adj = copy(com_bal)
com_bal_adj[is.na(com_bal_adj)] = 0
com_bal_adj = com_bal_adj[, list(Country, Year, Item, 
				stockvar,
				supply = feed+food+waste+processing+other+seed, 
				net_imports=import-export, 
				seed_rate=seed/(feed+food+waste+processing+other+seed),
				feed_rate=feed/(feed+food+waste+processing+other+seed),
				food_rate=food/(feed+food+waste+processing+other+seed),
				processing_rate=processing/(feed+food+waste+processing+other+seed),
				waste_rate=waste/(feed+food+waste+processing+other+seed),
				other_rate=other/(feed+food+waste+processing+other+seed))]

year_import_bal = com_bal_adj[,list(net_imports=sum(net_imports)), list(Year, Item)]
setkey(year_import_bal, Year, Item)
year_import_bal = year_import_bal[com_bal_adj[net_imports>0,list(exports=sum(net_imports)), by=list(Year, Item)]]
year_import_bal[,exports_adj := 1-net_imports/exports]

setkey(com_bal_adj, Year, Item)
com_bal_adj = com_bal_adj[year_import_bal[, list(Year, Item, exports_adj)]][net_imports>0, net_imports := net_imports*exports_adj]
com_bal_adj[, prod := supply + net_imports]
com_bal_adj[is.na(com_bal_adj)] = 0


########## Try to match up production and consumption
consump = com_bal_adj[, list(Country, Item, Year, supply, seed=seed_rate*supply, waste=waste_rate*supply, processing=processing_rate*supply, 
				food=food_rate*supply, feed=feed_rate*supply, import=0, stockvar, export=0, other=other_rate*supply, net_imports)]
consump[net_imports>0, import:=net_imports]
consump[net_imports<0, export:=-net_imports]
consump[,net_imports:=NULL]

prod_c = copy(crop_prod) #[!Item %like% "(Total)"]
prod_c[Item=="Rice paddy"]$Item = "Rice (Paddy Equivalent)"
prod_c[Item=="Soybeans"]$Item = "Soyabeans"
prod_c[Item=="Coconuts"]$Item = "Coconuts - Incl Copra"
prod_c[Item=="Groundnuts with shell"]$Item = "Groundnuts (in Shell Eq)" 
prod_c[Item=="Rapeseed"]$Item = "Rape and Mustardseed" 
prod_c[Item=="Onions dry"]$Item = "Onions" 
prod_c[Item=="Peas dry"]$Item = "Peas" 
prod_c[Item=="Beans dry"]$Item = "Beans" 
prod_c[Item=="Lemons and limes"]$Item = "Lemons Limes and products" 
prod_c[Item=="Coffee green"]$Item = "Coffee" 
prod_c[Item=="Grapefruit (inc. pomelos)"]$Item = "Grapefruit" 
prod_c[Item=="Grapes"]$Item = "Grapes and products (excl wine)"
prod_c[Item=="Olives"]$Item = "Olives (including preserved)"
prod_c[Item=="Cotton lint"]$Item = "Cotton lint - don't map"

prod_c[tolower(paste(Item, "and products")) %in% tolower(consump$Item), Item := paste(Item, "and products")]

combineProduction = function(consumed, prodList) {
	dt = rbind(prod_c, prod_c[Item %in% prodList, list(Item=consumed, area=sum(area), yield=sum(area*yield)/sum(area), prod=sum(prod)), by=list(Country, Year)], use.names=TRUE)
	dt[!Item %in% prodList]
}

prod_c = combineProduction("Tea (including mate)",c("Tea", "Mat"))
prod_c = combineProduction("Oranges Mandarines",c("Oranges", "Tangerines mandarins clementines satsumas"))
prod_c = combineProduction("Fruits Other",c("Apricots", "Cherries", "Watermelons", "Mangoes mangosteens guavas", "Melons other (inc.cantaloupes)", "Fruit tropical fresh nes", 
				"Fruit fresh nes", "Pears", "Peaches and nectarines", "Persimmons", "Plums and sloes", "Strawberries", "Kiwi fruit", "Cherries sour", "Figs"))
prod_c = combineProduction("Citrus Other",c("Fruit citrus nes"))
prod_c = combineProduction("Vegetables Other", c("Artichokes", "Asparagus", "Avocados", "Beans green", "Broad beans horse beans dry", "Cabbages and other brassicas", 
				"Cauliflowers and broccoli", "Chillies and peppers green", "Chillies and peppers dry", "Cow peas dry", "Cucumbers and gherkins", "Eggplants (aubergines)", "Garlic", 
				"Vegetables leguminous nes", "Leeks other alliaceous vegetables", "Lettuce and chicory", "String beans", "Cashewapple", "Mushrooms and truffles", "Vetches", 
				"Okra", "Onions shallots green", "Peas green", "Papayas", "Pigeon peas", "Pumpkins squash and gourds", "Spinach", "Taro (cocoyam)", "Vegetables fresh nes",
				"Yautia (cocoyam)", "Pepper (piper spp.)", "Chicory roots", "Carrots and turnips","Roots and tubers nes"))
prod_c = combineProduction("Cereals Other",c("Cereals nes", "Grain mixed", "Maize green", "Triticale"))
prod_c = combineProduction("Pulses Other and products",c("Buckwheat", "Chick peas", "Lentils", "Pulses nes", "Carobs", "Lupins", "Fonio"))
prod_c = combineProduction("Spices Other",c("Spices nes","Ginger", "Anise badian fennel coriander"))
prod_c = combineProduction("Oil Crops Other",c("Linseed", "Castor oil seed", "Oilseeds nes", "Mustard seed", "Safflower seed", "Tallowtree seed"))
prod_c = combineProduction("Nuts and products",c("Nuts nes", "Almonds with shell", "Walnuts with shell", "Chestnut", "Cashew nuts with shell", "Hazelnuts with shell", "Areca nuts", "Pistachios", 
				"Karite nuts (sheanuts)", "Kola nuts", "Brazil nuts with shell", "Tung nuts"))

prod_c[Item=="Cereals Total + (Total)"]$Item = "Cereals - Excluding Beer + (Total)"
prod_c[Item=="Fruit excl Melons Total + (Total)"]$Item = "Fruits - Excluding Wine + (Total)"
prod_c[Item=="Pulses Total + (Total)"]$Item = "Pulses + (Total)"
prod_c[Item=="Roots and Tubers Total + (Total)"]$Item = "Starchy Roots + (Total)"
prod_c[Item=="Treenuts Total + (Total)"]$Item = "Treenuts + (Total)"
prod_c[Item=="Vegetables Primary + (Total)"]$Item = "Vegetables + (Total)"
prod_c[Item=="Oilcrops Primary + (Total)"]$Item = "Oilcrops + (Total)"

consump[,mItem:=gsub("\\s","", tolower(Item))]
prod_c[,mItem:=gsub("\\s","", tolower(Item))]

prod_m = merge(com_bal, getFeedConvRatio()[, list(Item=consumptionItem, fcr)], by="Item")[Year < 2012, list(prod=sum(prod*fcr)),by=list(Country, Year)]

setkey(prod_m, Country, Year)
prod_m[country_data, area := (perm_pasture+temp_pasture)*1000]

# Sort out palm oil and palm oil fruit
setkey(prod_c, Country, Year)
setkey(consump, Country, Year)
palm_p = prod_c[mItem == "oilpalm"][prod_c [mItem == "oilpalmfruit"], list(Country, Year, Item="Palm Oil", area=i.area, yield=prod/i.area, prod=prod, mItem="palmoil")]
prod_c = rbind(prod_c[!mItem == "oilpalm" & !mItem == "oilpalmfruit"], palm_p[!is.na(yield)], use.names=TRUE)

# other oil crops
handleOilCrop = function(inputDt, oilcrop, processed, removeProcessedRows = TRUE) {
	
	dt = inputDt[Item %in% processed | Item == oilcrop]
	dt[Item %in% processed, c('supply', 'processing') := list((import - export), -(supply - import - stockvar + export))] # need to remove domestic processing from combined row
	
	combinedRows = dt[, list(Item=oilcrop, supply=sum(supply), seed=sum(seed), waste=sum(waste),
					processing=sum(processing), food=sum(food), feed=sum(feed), import=sum(import), stockvar=sum(stockvar), export=sum(export), 
					other=sum(other), mItem=gsub("\\s","", tolower(oilcrop))), 
			by=list(Country, Year)]
	
	rbind(combinedRows, inputDt[!((removeProcessedRows & Item %in% processed) | Item == oilcrop)], use.names=TRUE)
}

consump = handleOilCrop(consump, "Oilcrops + (Total)", c("Rape and Mustard Oil", "Rape and Mustard Cake", "Soyabean Oil", 
				"Soyabean Cake", "Sunflowerseed Oil", "Sunflowerseed Cake"), FALSE)

consump = handleOilCrop(consump, "Rape and Mustardseed", c("Rape and Mustard Oil", "Rape and Mustard Cake"))
consump = handleOilCrop(consump, "Soyabeans", c("Soyabean Oil", "Soyabean Cake"))
consump = handleOilCrop(consump, "Sunflower seed", c("Sunflowerseed Oil", "Sunflowerseed Cake"))
consump[processing < 0, processing:=0]

# Join non-meat prod and consumption
allocateExports = function(dt, areas_exported) {
	dt[areas_exported, c('world_export_area', 'world_export') := list(world_export_area, world_export)]
	dt[net_import > 0, imported_area := net_import*world_export_area/world_export] #imports based on global average yield
	dt[net_import < 0, imported_area := (net_import/prod*area)] #exports based on that countries yield
	
	dt[net_import == 0, imported_area := 0]
	dt[is.na(dt)] = 0
	dt[, net_area := area+imported_area]
	
	dt[supply>0, c('feed_area', 'food_area', 'waste_area', 'other_area', 'seed_area') := 
					list(feed/(feed+food+waste+processing+other+seed)*net_area/1000000, 
							(processing+food)/(feed+food+waste+processing+other+seed)*net_area/1000000, 
							waste/(feed+food+waste+processing+other+seed)*net_area/1000000, 
							other/(feed+food+waste+processing+other+seed)*net_area/1000000, 
							seed/(feed+food+waste+processing+other+seed)*net_area/1000000)]
	
	dt[supply > 0, c('food_area', 'feed_area') := list(food_area + seed_area*(1-feed/supply), feed_area + seed_area*feed/supply)]
	dt$seed_area = NULL
	dt
}

getCropConProd = function(prod_c, consump) {
	setkey(prod_c, Country, Year, mItem)
	setkey(consump, Country, Year, mItem)
	dt = prod_c[consump]
	dt = dt[Country %in% getCountryCodes()$Country]
	dt[, net_import:=import-export]
	dt[,prod:=sum(supply, -import, export, na.rm=TRUE), by=list(Country, Year, mItem)]
	dt[area > 0 & prod==0, area:=0]
	setkey(dt, mItem, Year)
	
	areas_exported=dt[net_import < 0 & !is.na(area) & prod>0, list(world_export_area=-sum(area*net_import/prod), world_export=-sum(net_import)), by=list(mItem, Year)]
	dt = allocateExports(dt, areas_exported)
	dt
}
con_prod_c = getCropConProd(prod_c, consump)

getMeatConProd = function(prod_m, consump) {
	feedConvRatio = getFeedConvRatio()
	setkey(feedConvRatio, consumptionItem)
	setkey(consump, Item)
	con_m = feedConvRatio[consump][consumptionItem %in% feedConvRatio$consumptionItem, list(supply=sum(supply*fcr), seed=sum(seed*fcr), waste=sum(waste*fcr),
					processing=sum(processing*fcr), feed=sum(feed*fcr), food=sum(food*fcr), import=sum(import*fcr), export=sum(export*fcr), 
					stockvar=sum(stockvar*fcr), other=sum(other*fcr)), by=list(Country, Year)]

	setkey(con_m, Country, Year)
	cpm = con_m[prod_m]
	cpm = cpm[Country %in% getCountryCodes()$Country]
	cpm[is.na(supply), c('supply', 'food', 'import', 'export') := list(prod,prod,0,0)]
	cpm[, c('net_import', 'mItem') := list(import-export, "meatmilkeggs")]
	setkey(cpm, Country, Year)
	cpm[con_prod_c[!Item %like% "(Total)", list(feed_area=sum(feed_area, na.rm=TRUE)), by=list(Country,Year)], area := (area + feed_area*1000000)] # add pasuture to feed area
	cpm = cpm[area > 0 ]
	
	setkey(cpm, Year)
	areas_exported=cpm[net_import < 0 & !is.na(area) & prod>0, list(world_export_area=-sum(area*net_import/prod), world_export=-sum(net_import)), by=list(Year)]
	areas_exported=areas_exported[cpm[,list(import_export_factor=sum(export)/sum(import)),by=list(Year)]]
	allocateExports(cpm, areas_exported)
}
con_prod_m = getMeatConProd(prod_m, consump)



calcCropDrivers = function(inputDt, baselineYear) {
	dt = merge(inputDt, country_data, by=c('Country', 'Year'))
	dt = dt[, c('food_nm_a', 'con_pc', 'net_y', 'pop') := list(
					food_area,
					sum(processing,food,na.rm=TRUE)/population,
					sum(processing,food,na.rm=TRUE)/food_area/1000000,
					population), 
			by=list(Country, Year, mItem)]
	
	baseline_con = dt[Year == baselineYear, list(Country, mItem, base_con_pc=con_pc, base_net_y=net_y, base_pop=pop, base_food_nm_a=food_nm_a)]
	
	setkey(dt, Country, mItem)
	setkey(baseline_con, Country, mItem)
	dt[baseline_con, c('base_con_pc', 'base_net_y', 'base_pop', 'base_food_nm_a') := list(base_con_pc, base_net_y, base_pop, base_food_nm_a)]
	
	dt[food_nm_a!=base_food_nm_a, temp := (food_nm_a-base_food_nm_a)/(log(food_nm_a)-log(base_food_nm_a))]
	dt[!is.na(temp), c('con_change_area', 'y_change_area', 'pop_change_area') := 
					list(temp*log(con_pc/base_con_pc), temp*log(base_net_y/net_y), temp*log(pop/base_pop))]
	dt[, temp := NULL]
	dt[is.infinite(con_change_area)]$con_change_area = 0
	dt[is.infinite(y_change_area)]$y_change_area = 0
	dt[is.infinite(pop_change_area)]$pop_change_area = 0
	dt[is.na(dt)] = 0
	dt
}

getCountryConProd = function(baselineYear) {
	con_prod_c_d = calcCropDrivers(con_prod_c[!Item %like% "(Total)"], baselineYear)
	
	dt = con_prod_c_d[abs(con_change_area+ pop_change_area+ y_change_area-food_nm_a+base_food_nm_a)<0.01, list(
					net_area=sum(net_area), 
					con_pc = sum(con_pc),
					prod=sum(prod),
					area=sum(area), 
					feed_area=sum(feed_area), 
					waste_area=sum(waste_area),
					other_area=sum(other_area), 
					food_nm_a = sum(food_nm_a),
					base_food_nm_a = sum(base_food_nm_a),
					con_change_area_c=sum(con_change_area),
					y_change_area_c=sum(y_change_area),
					pop_change_area_c=sum(pop_change_area)
					), 
			by=list(Country,Year, Region, CountryCode, land, population)]
	
	
	# Consolidating meat and non-meat by country and year
	setkey(dt, Country, Year)
	con_prod_m_short = con_prod_m[,list(Country, Year, m_area=area, m_prod=prod, m_net_area=net_area, 
					meat_a=food_area+feed_area+other_area, m_waste_area=waste_area, meat_con=food+processing)]
	setkey(con_prod_m_short, Country, Year)
	dt = dt[con_prod_m_short]
		
	dt[is.na(dt)] = 0						
	dt[, c('waste_a', 'con_pc_m', 'net_y_m')  := list(waste_area+m_waste_area, (meat_con)/population, (meat_con)/(meat_a)/1000000)]
	dt[is.infinite(con_pc_m)]$con_pc_m = 0
	dt[is.infinite(net_y_m)]$net_y_m = 0
	baseline_con = dt[Year == baselineYear, list(Country, base_con_pc_m=con_pc_m, base_net_y_m=net_y_m, base_pop=population, base_meat_a=meat_a)]
	
	dt = merge(dt, baseline_con, by="Country")
	
	dt[meat_a!=base_meat_a, temp := (meat_a-base_meat_a)/(log(meat_a)-log(base_meat_a))]
	dt[!is.na(temp), c('con_change_area_m', 'y_change_area_m', 'pop_change_area_m') := 
					list(temp*log(con_pc_m/base_con_pc_m), temp*log(base_net_y_m/net_y_m), temp*log(population/base_pop))]
	dt[, temp := NULL]
	dt[is.na(dt)] = 0
	dt[is.infinite(con_change_area_m)]$con_change_area_m = 0
	dt[is.infinite(y_change_area_m)]$y_change_area_m = 0
	dt[is.infinite(pop_change_area_m)]$pop_change_area_m = 0
	
	# Sudan
	adjustValue = function (x, y) { x * y}
	convertSudan = function (dt, newC, newCC, factor) {
		ss = dt[Country == "Sudan (former)"]
		ss$Country = newC
		ss$CountryCode = newCC
		ss[, 5:7 := lapply(.SD, adjustValue, y=factor), .SDcols = 5:7, by = Year]
		ss[, 9:34 := lapply(.SD, adjustValue, y=factor), .SDcols = 9:34, by = Year]
	}
	dt = rbind(dt,
			convertSudan(dt, "South Sudan", "SSD", 10381/(10381+36431)), 
			convertSudan(dt, "Sudan", "SDN", 36431/(10381+36431)))[!Country == "Sudan (former)"]
	
	#Democratic Republic of the Congo
	dt = dt[!Country == "Democratic Republic of the Congo"] # no com_bal data for it, so best to remove
	dt
}

country_con_prod1961 = getCountryConProd(1961)
country_con_prod1994 = getCountryConProd(1994)
country_con_prod = country_con_prod1994
