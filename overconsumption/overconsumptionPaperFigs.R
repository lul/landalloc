#############################################################

library(data.table)
library(tidyr)
library(dplyr)

library(terra)
library(ggplot2)
library(grid)
library(gridExtra)

##recommended Calorie consumption per capita per day per country
##need to think about demography, gender/age

rasterDataDir=file.path("C:\\Users\\rosly\\eclipse-workspace\\plumv2\\data\\halfdeg")
countryWregions = fread(file.path(plum_data_dir,"countriesWRegions.csv"))
cBoundaries <- fread(file.path(rasterDataDir,"country_boundaries.csv")) #singapore is missing, no substantial land area as it's urban
countries = fread(file.path(plum_data_dir,"countries.csv"))

c = merge(cBoundaries,countries, by=c("M49"), all.x=T)
cpg = unique(countryWregions[,list(Region,PlumGroup)])

cb = merge(c[,list(Lon,Lat,PlumGroup)],cpg, by="PlumGroup", all.x=T)

dt=fread('C:\\Users\\rosly\\Desktop\\overconsumption\\v3\\country_opt_concat.txt')
setDT(dt)[, c('Ensemble','Rep') := tstrsplit(Scenario, "_s")]

price=fread('C:\\Users\\rosly\\Desktop\\overconsumption\\v3\\price_concat.txt')
setDT(price)[, c('Ensemble','Rep') := tstrsplit(Scenario, "_s")]


####calorie difference between the two scenarios Fig 1(a)

##take year of interest
ha=dt[Year==2050, list(Ensemble,commodity,rebasedKcal,population,Rep, Country)]


# Group by 'Rep' and 'Ensemble', then summarize the sum of 'rebasedKcal'
calories <- data.table(ha %>%
                         group_by(Rep, Ensemble, Country) %>%
                         summarize(total_rebasedKcal = sum(rebasedKcal, na.rm = TRUE),
                                   population = mean(population, na.rm=TRUE)))

##calculate total calories across commodity groups
calories_data <- dcast(
  calories, 
  Country + Rep ~ Ensemble, 
  value.var = "total_rebasedKcal"
)

##calculate the % difference between the two scenarios in 2050 for each paired replicate
calories_data[,diff := (SSP2 - SSP2_nooverconsumption)/SSP2*100] 

##calculate summary statistics for each country
calories_dat <- data.table(calories_data %>%
                             group_by(Country) %>%
                             summarise(
                               mean_upper_lim = median(SSP2_nooverconsumption),
                               mean_cal_change = median(diff, na.rm = TRUE),
                               sd_cal_change = sd(diff, na.rm = TRUE)
                             ))


calories_dat_r = merge(calories_dat, countryWregions[,list(Country=Area,Region)], by='Country')
setorder(calories_dat_r, -mean_cal_change)


#Create a maps of land footprints in 2050
world_map = map_data("world")
# Filter world map data to remove Antarctica
world_map_no_antarctica <- world_map %>% filter(lat > -60)

##Adjust country names for merge
calories_dat_r_for_Map = calories_dat_r %>% 
  mutate(Country = case_when(Country == "Bolivia (Plurinational State of)" ~ "Bolivia",
                             Country == "Brunei Darussalam" ~ "Brunei",
                             Country == "Democratic People's Republic of Korea" ~ "North Korea",
                             Country == "Republic of Korea" ~ "South Korea",
                             Country == "Republic of Moldova" ~ "Moldova",
                             Country == "United States of America" ~ "USA",
                             Country == "United Kingdom" ~ "UK",
                             Country == "Syrian Arab Republic" ~ "Syria",
                             Country == "Venezuela (Bolivarian Republic of)" ~ "Venezuela",
                             Country == "Turkiye" ~ "Turkey",
                             Country == "Russian Federation" ~ "Russia",
                             Country == "Viet Nam" ~ "Vietnam",
                             Country == "Cabo Verde" ~ "Cape Verde",
                             Country == "Congo" ~ "Republic of Congo",
                             Country == "Cote d'Ivoire" ~ "Ivory Coast",
                             Country == "Czechia" ~ "Czech Republic",
                             Country == "Eswatini" ~ "Swaziland",
                             Country == "Iran (Islamic Republic of)" ~ "Iran",
                             Country == "Lao People's Democratic Republic" ~ "Laos",
                             Country == "Saint Vincent and the Grenadines" ~ "Saint Vincent",
                             Country == "Trinidad and Tobago" ~ "Trinidad",
                             Country == "United Republic of Tanzania" ~ "Tanzania",
                             TRUE ~ Country))

merged_data <- merge(calories_dat_r_for_Map, world_map_no_antarctica, by.y = "region", by.x = "Country", all.x = TRUE)


pcCalPlot= ggplot() +
  geom_map(
    data = merged_data, map = world_map,
    aes(long, lat, map_id = Country, fill = -mean_cal_change)) +
  scale_fill_gradient2(
    low = "red", mid = "white", high = "darkblue",
    midpoint = 0, space = "Lab",
    name = "% change (kcal/cap/yr)"
  ) +
  geom_polygon(
    data = world_map_no_antarctica, aes(x = long, y = lat, group = group),
    fill = NA, color = "black", size = 0.3
  ) +
  theme_light() +  # Removes axis and gridlines
  labs(
    title = expression(paste("(a) Mean ", Delta, " calorie intake between the No overconsumption and Reference scenarios (2050)")),
    fill = "% change (kcal/cap/yr)"
  ) +
  theme(
    plot.title = element_text(hjust = 0.5, size = 20),
    text = element_text(size = 20),
    panel.grid = element_blank(), 
    panel.border = element_blank(),  
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    axis.ticks = element_blank(),
    axis.title = element_blank(),
    axis.text = element_blank(),
    legend.text = element_text(size = 20),
    legend.title = element_text(size = 20, hjust=0.5),
    legend.key.size = unit(1.5, 'cm'),
    legend.key.height = unit(1.5, "cm"),  # Adjust legend key height
  )



# Group by 'Rep' and 'Ensemble', then summarize the sum of 'rebasedKcal'
all_calories <- data.table(ha %>%
                         group_by(Rep, Ensemble, Country) %>%
                         summarize(total_rebasedKcal = sum(rebasedKcal, na.rm = TRUE),
                                   population = mean(population, na.rm=TRUE)))

all_calories[,totalCaloriesConsumed := total_rebasedKcal*population]

all_calories_data <- dcast(
  all_calories, 
  Country + Rep ~ Ensemble, 
  value.var = "totalCaloriesConsumed"
)

all_calories_dat_r = merge(all_calories_data, countryWregions[,list(Country=Area,Region)], by='Country', all.x=TRUE)


# Summing values by Region and Rep
summed_all_calories_data_r <- all_calories_dat_r[, .(
  sum_SSP2 = sum(SSP2, na.rm = TRUE),
  sum_SSP2_noOC = sum(SSP2_nooverconsumption, na.rm = TRUE)
), by = .(Region, Rep)]

summed_all_calories_data_r[,diff := (sum_SSP2 - sum_SSP2_noOC)/sum_SSP2*100] 

summed_all_calories_data_r_av <- data.table(summed_all_calories_data_r %>%
                                          group_by(Region) %>%
                                          summarise(
                                            mean_cal_change = median(diff, na.rm = TRUE),
                                            sd_cal_change = sd(diff, na.rm = TRUE)
                                          ))


summed_all_calories_data_r_av <- summed_all_calories_data_r_av[order(-mean_cal_change)]

region_mapping <- c(
  "South Asia" = "SA",
  "Europe & Central Asia" = "ECA",
  "Middle East & North Africa" = "MENA",
  "Sub-Saharan Africa" = "SSA",
  "Latin America & Caribbean" = "LAC",
  "East Asia & Pacific" = "EAP",
  "North America" = "NA"
)

# Add abbreviated region to the data
summed_all_calories_data_r_av <- summed_all_calories_data_r_av %>%
  mutate(Abbrev_Region = region_mapping[Region])


# Create the bar chart
allCaloriePlot <- ggplot(summed_all_calories_data_r_av, aes(x = reorder(Abbrev_Region, -mean_cal_change), y = mean_cal_change)) +
  geom_bar(stat = "identity", fill="white", color = "black") +
  geom_errorbar(aes(ymin = mean_cal_change - sd_cal_change, ymax = mean_cal_change + sd_cal_change), 
                width = 0.3) +
  labs(
    x = "Region",
    y = "Total calorie reduction (%)"
  ) +
  theme_minimal() +
  theme(
    legend.title = element_text(size = 15),
    legend.text = element_text(size = 15),
    legend.position = "bottom",  # Move legend to below the plot
    axis.title = element_text(size = 15),
    axis.text = element_text(size = 15),
    plot.title = element_blank(),
    plot.margin = unit(c(0, 0, 0, 0), "cm"),
    axis.text.x = element_text(angle = 45, hjust = 1)
  )



####Plot natural land change (Fig 1 (b))

###process each replicate
landCovers = NULL
for(r in 1:30){
  
  landCover = fread(file.path("C:\\Users\\rosly\\Desktop\\overconsumption\\v3\\LU_folders","SSP2",paste0("s",r),"2050","LandCover.txt"))
  landCoverOC = fread(file.path("C:\\Users\\rosly\\Desktop\\overconsumption\\v3\\LU_folders","SSP2_nooverconsumption",paste0("s",r),"2050","LandCover.txt"))
  tog = merge(landCover[,list(Lat,Lon,Protection,naturalLand = UnmanagedForest+OtherNatural)],landCoverOC[,list(Lat,Lon,Protection,naturalLandOC = UnmanagedForest+OtherNatural)], by=c('Lat','Lon','Protection'))
  
  
  # Sum the values by Lat and Lon
  summarized_data <- tog[, .(
    total_naturalLand = sum(naturalLand, na.rm = TRUE),
    total_naturalLandOC = sum(naturalLandOC, na.rm = TRUE)
  ), by = .(Lat, Lon)]
  
  
  summarized_data[,diff := total_naturalLandOC - total_naturalLand]
  print(file.path("C:\\Users\\rosly\\Desktop\\overconsumption\\v3\\LU_folders",paste0("s",r),"2050","LandCover.txt"))
  
  togcbr <- summarized_data[, .(rep= r,Lat,Lon,diff)]
  
  landCovers=rbind(landCovers,togcbr)
  
}


ex=merge(landCovers,cb[,list(Lat,Lon,Region, PlumGroup)],by=c('Lat','Lon'), all.x=TRUE)
togcbr <- ex[, .(totalDiff = sum(diff, na.rm = TRUE)), by = c("rep","Region")]

togcbr[, totalRepDiff := sum(totalDiff), by = rep]
togcbr[, proportion := totalDiff / totalRepDiff *100]

nat_dat <- data.table(togcbr %>%
                                  group_by(Region) %>%
                                  summarise(
                                    mean_ag_change = median(proportion, na.rm = TRUE),
                                    sd_ag_change = sd(proportion, na.rm = TRUE)
                                  ))

countryDiff <- ex[, .(totalCountryDiff = sum(diff, na.rm = TRUE)), by = c("rep","PlumGroup")]

nat_country <- data.table(countryDiff %>%
                        group_by(PlumGroup) %>%
                        summarise(
                          mean_ag_change = median(totalCountryDiff, na.rm = TRUE),
                          sd_ag_change = sd(totalCountryDiff, na.rm = TRUE)
                        ))

setorder(nat_country, mean_ag_change)



mean_diff <- landCovers[, .(mean_diff = mean(diff)), by = .(Lat, Lon)]


togcbr <- ex[, .(Ensemble = dir, Rep= r, totalAgArea = sum(agriculture, na.rm = TRUE)), by = Region]


landUse = fread('C:\\Users\\rosly\\Desktop\\overconsumption\\v3\\lc_concat.txt')
setDT(landUse)[, c('Ensemble','Rep') := tstrsplit(Scenario, "_s")]

h_melt = data.table(melt(landUse[, list(Year,Ensemble,Rep,natural=OtherNatural+UnmanagedForest, cropland=Cropland,timber=TimberForest, pasture=Pasture, fert=FertCrop, irrig=IrrigCrop)], id.vars=c('Year','Ensemble','Rep'), variable.name='variable', value.name="Value"))

h_avg = h_melt[variable=="natural", list(sdev = sd(Value), avg = median(Value)), by=list(Year,Ensemble)]

# Rename scenarios
h_avg[, Ensemble := factor(Ensemble, 
                          levels = c("SSP2", "SSP2_nooverconsumption"),
                          labels = c("Reference", "No overconsumption"))]

# Create the inset plot
h_avg_plot <- ggplot(h_avg, aes(x = Year, y = avg, color = Ensemble, fill = Ensemble)) +
  geom_line(size = 1) +  # Line for average values
  geom_ribbon(aes(ymin = avg - sdev, ymax = avg + sdev), alpha = 0.2, color = NA) +  # Error ribbons
  theme_minimal() +
  theme(
    legend.title = element_text(size = 15),
    legend.text = element_text(size = 15),
    legend.position = "bottom",  # Move legend to below the plot
    axis.title = element_text(size = 15),
    axis.text = element_text(size = 15),
    plot.title = element_blank(),
    plot.margin = unit(c(0, 0, 0, 0), "cm")
  ) +
  scale_color_manual(
    values = c("Reference" = "darkorchid", "No overconsumption" = "mediumaquamarine")
  ) +
  scale_fill_manual(
    values = c("Reference" = "darkorchid", "No overconsumption" = "mediumaquamarine")
  ) +
  labs(
    title = "",
    x = "Year",
    y = "Global natural land \n (Mha)",
    color = "Scenario",  # Updated legend title
    fill = "Scenario"    # Updated legend title
  )



# Create the main map plot without Antarctica
natLandPlot <- ggplot() +
  geom_raster(data = mean_diff[, list(Lon, Lat, mean_diff)], aes(x = Lon, y = Lat, fill = mean_diff)) + 
  coord_quickmap() +
  theme_bw() +
  theme(
    panel.grid.major = element_blank(), 
    panel.grid.minor = element_blank(),
    legend.text = element_text(size = 20),
    legend.title = element_text(size = 20),
    legend.title.align = 0.5,
    legend.key.size = unit(1.5, 'cm'),
    legend.key.height = unit(1.5,'cm'),
    panel.border = element_blank(),
    axis.title = element_blank(),
    axis.text = element_blank(),
    axis.ticks = element_blank(),
    plot.title = element_text(hjust = 0.5, size = 20),
    plot.margin = unit(c(0, 0, 0, 0), "cm")
  ) +
  scale_fill_gradient2(
    low = "red", mid = "white", high = "darkblue",
    midpoint = 0, space = "Lab",
    name = "Difference (Mha)"
  ) +
  geom_polygon(
    data = world_map_no_antarctica, aes(x = long, y = lat, group = group),
    fill = NA, color = "black", size = 0.3
  ) +
  geom_hline(yintercept = c(23.5, -23.5), linetype = "dashed", color = "darkgrey", size = 0.7) +
  labs(
    title = expression(paste("(b) Mean ",Delta, " natural land between the No overconsumption and Reference scenarios (2050)")),
    fill = "Change (Mha)"
  ) 

##inset graph  on natural land map
naturalPlot = natLandPlot + annotation_custom(
  grob = ggplotGrob(h_avg_plot), 
  xmin = -200, xmax = -100,  # Adjust these to enlarge the inset
  ymin = -85, ymax = -25
)

##inset graph  on natural land map
consPlot = pcCalPlot + annotation_custom(
  grob = ggplotGrob(allCaloriePlot), 
  xmin = -180, xmax = -100,  # Adjust these to enlarge the inset
  ymin = -65, ymax = -20
)


consPlot <- consPlot + coord_fixed(ratio = 1)
naturalPlot <- naturalPlot + coord_fixed(ratio = 1)

# Add the inset plot to the main map with a larger size
tiff(width=20,height=15, units='in', res=200, file = file.path("C:\\Users\\rosly\\Desktop\\overconsumption\\v3", 'Figure1.tif'))
grid.arrange(consPlot, naturalPlot, nrow = 2)

dev.off()


###Figure 2 


lesTab = merge(dt[Year==2020, list(commodity,Country,Scenario,Ensemble,Rep,basePrice=price, baseQuantity=rebasedKcal/2000)],
               dt[, list(commodity,Year,Country,Scenario,Ensemble,Rep,population,price=price, quantity=rebasedKcal/2000)],
               by=c('commodity','Country','Ensemble','Scenario','Rep'))

lesperesTab = lesTab[,list(LaspIndex = sum(price*baseQuantity)/sum(basePrice*baseQuantity),population=population), 
                     by =c('Country','Ensemble','Scenario','Year','Rep')]

lesRe=merge(lesperesTab,countryWregions[,list(Country=Area,Region)],by='Country')

##change between income group or region on line below 
lesRegTab = lesRe[,list(LaspIndex=sum(LaspIndex*population)/sum(population)), by=c('Ensemble','Scenario','Year','Region','Rep')]

reshapedLes <- dcast(
  lesRegTab,
  Region + Year + Rep ~ Ensemble,
  value.var = "LaspIndex"
)

reshapedLes[,diff := (SSP2_nooverconsumption - SSP2)/SSP2 *100]

les_stats <- reshapedLes[, .(
  MedianDiff = median(diff, na.rm = TRUE),
  SDDiff = sd(diff, na.rm = TRUE)
), by = .(Region, Year)]

lesPlot=  ggplot(les_stats[Year < 2051], aes(x = Year, y = MedianDiff, color = Region, fill = Region)) +
  geom_line(size = 1.2) +  # Line for MedianDiff
  geom_ribbon(aes(ymin = MedianDiff - SDDiff, ymax = MedianDiff + SDDiff), 
              alpha = 0.2, color = NA) +  # Ribbon for SDDiff
  scale_y_continuous(limits = c(-5, 2.5)) +  # Set y-axis limits
  geom_vline(xintercept = 2025, linetype = "dashed", color = "grey", size = 0.8) +
  labs(
    title = "",
    x = "Year",
    y = "Change in the Lespeyres price index (%)",
    
    color = "Region",
    fill = "Region"
  ) +
  theme_minimal() + 
  theme(
    legend.position = "bottom"  
  
  )



##average weighted calorie proportional of each commodity in 2050 

slim = dt[,list(Country,Year,population,commodity, rebasedKcal,Ensemble,Rep)]
slim = merge(slim, countryWregions[,list(Country=Area,Region)], by='Country')

region_mapping <- c(
  "South Asia" = "SA",
  "Europe & Central Asia" = "ECA",
  "Middle East & North Africa" = "MENA",
  "Sub-Saharan Africa" = "SSA",
  "Latin America & Caribbean" = "LAC",
  "East Asia & Pacific" = "EAP"
)

# Add abbreviated region to the data
slim <- slim %>%
  mutate(Abbrev_Region = region_mapping[Region])

# Calculate weighted kcal consumption for each commodity across Ensemble and Rep
weighted_avg <- slim[, .(
  weighted_kcal = sum(rebasedKcal * population) / sum(population)
), by = .(commodity, Ensemble, Rep, Year, Region)]


# Calculate proportions
weighted_avg[, proportion := weighted_kcal / sum(weighted_kcal), by = .(Region, Year, Ensemble, Rep)]


# Reshape data to wide format to separate the weighted_proportion by Ensemble
reshaped_data <- dcast(
  weighted_avg,
  Year + Rep + Region + commodity ~ Ensemble,
  value.var = "proportion"
)

# Calculate the difference between the two Ensembles
reshaped_data[, difference := (SSP2_nooverconsumption - SSP2) * 100]


summary_stats <- reshaped_data[, .(
  MedianDiff = median(difference, na.rm = TRUE),
  SDDiff = sd(difference, na.rm = TRUE)
), by = .(Region, Year, commodity)]

summary_stats[Year==2050]

summary_stats[commodity =='FruitVeg', commodity := 'Fruit & Veg']
summary_stats[commodity =='CerealsStarchyRoots', commodity := 'Cereals & Starchy roots']


# Create the bar chart

commodity_colors <- c(
  "Fruit & Veg" = "#E69F00",
  "Sugar" = "#56B4E9",
  "Pulses" = "#009E73",
  "Oilcrops" = "#F0E442",
  "Monogastrics" = "#D55E00",
  "Ruminants" = "#CC79A7",
  "Cereals & Starchy roots" = "#999999"
)

# Create the bar chart
propPlot <- ggplot(summary_stats[Year == 2050], aes(x = commodity, y = MedianDiff, fill = commodity)) +
  geom_bar(stat = "identity", position = position_dodge(width = 0.8), color = "black") +  # Bars
  geom_errorbar(aes(ymin = MedianDiff - SDDiff, ymax = MedianDiff + SDDiff), 
                width = 0.2, position = position_dodge(width = 0.8)) +  # Error bars
  facet_wrap(~ Region, scales = "free_x", ncol = 3) +  # Separate plots for each region
  theme_minimal() +  # Clean theme
  theme(
    axis.text.x = element_blank(),  # Rotate x-axis labels
    legend.position = "bottom"
  ) +
  labs(
    title = "",
    x = "",
    y = "Change in the % of calories consumed in 2050",
    fill = "Commodity"
  ) +
  scale_fill_manual(values = commodity_colors)  # Use the custom color palette



###############prices

#why when prices go up, consumption goes up. regional prices? Plot traditional price difference map  and regionally 


# Reshape data to wide format to separate the weighted_proportion by Ensemble
reshaped_price<- dcast(
  price,
  Year + Crop + Rep ~ Ensemble,
  value.var = "Price"
)

# Calculate the difference between the two Ensembles
reshaped_price[, difference := (SSP2_nooverconsumption-SSP2)/SSP2 * 100]


price_summary <- reshaped_price[, .(
  medianPriceChange = median(difference, na.rm = TRUE),
  sdPriceChange = sd(difference, na.rm = TRUE)
), by = .(Crop, Year)]


price_summary[, Crop := factor(Crop,
                               levels = c("fruitveg", "Maize", "monogastrics", 
                                          "oilcropsNFix", "oilcropsOther", "pulses", 
                                          "rice", "ruminants", "starchyRoots", 
                                          "sugar", "wheat"),
                               labels = c("Fruit & Veg", "Maize", "Monogastrics", 
                                          "N. fixing oilcrops", "Other oilcrops", 
                                          "Pulses", "Rice", "Ruminants", 
                                          "Starchy roots", "Sugar", "Wheat"))]
price_summary <- na.omit(price_summary)
price_summary$Crop <- factor(price_summary$Crop, levels = names(crop_colors))
price_summary$Crop <- trimws(price_summary$Crop)


# Define a color-blind-friendly palette
crop_colors <- c(
  "Fruit & Veg" = "#E69F00",        # Orange
  "Sugar" = "#56B4E9",             # Sky Blue
  "Pulses" = "#009E73",            # Bluish Green
  "N. fixing oilcrops" = "#F0E442",# Yellow
  "Monogastrics" = "#D55E00",      # Vermilion
  "Ruminants" = "#CC79A7",         # Reddish Purple
  "Wheat" = "#999999",             # Grey
  "Maize" = "#0072B2",             # Blue
  "Other oilcrops" = "#A65628",    # Brown (substitute color not in Okabe-Ito)
  "Rice" = "#F781BF",              # Pink (substitute color not in Okabe-Ito)
  "Starchy roots" = "#E41A1C"      # Red (substitute color not in Okabe-Ito)
)

pricePlot = ggplot(data = price_summary, 
           aes(x = Year, y = medianPriceChange, color = Crop, fill = Crop)) +
  geom_line(size = 1) +
  geom_ribbon(aes(ymin = medianPriceChange - sdPriceChange, ymax = medianPriceChange + sdPriceChange), alpha = 0.2) +
  labs(
    x = "Year",
    y = "Change in global export price (%)",
    color = "Crop",
    fill = "Crop"
  ) +
  geom_hline(yintercept = 0, linetype = "dashed", color = "grey", size = 0.8)+
  theme_minimal() +
  theme(
    strip.text = element_text(size = 10, face = "bold"),
    legend.position = "bottom"
  ) +
  scale_color_manual(values = crop_colors) +  # Apply crop_colors for line colors
  scale_fill_manual(values = crop_colors)    # Apply crop_colors for ribbon fills

library(cowplot)
# Add labels to each plot
lesPlot_labeled <- plot_grid(lesPlot, labels = "(a)", label_size = 10, label_x = 0, label_y = 1.01, hjust = -0.5, vjust = 1.5)
propPlot_labeled <- plot_grid(propPlot, labels = "(c)", label_size = 10, label_x = 0, label_y = 1, hjust = -0.5, vjust = 1.5)
pricePlot_labeled <- plot_grid(pricePlot, labels = "(b)", label_size = 10, label_x = 0, label_y = 1.01, hjust = -0.5, vjust = 1.5)

# Custom layout matrix
layout_matrix <- rbind(
  c(1, 2),  # lesPlot (left) and pricePlot (right)
  c(3, 2)   # lesPlot (left) and propPlot (right)
)


# Add the inset plot to the main map with a larger size
tiff(width=20,height=10, units='in', res=200, file = file.path("C:\\Users\\rosly\\Desktop\\overconsumption\\v3", 'Figure2.tif'))
grid.arrange(
  grobs = list(lesPlot_labeled, propPlot_labeled, pricePlot_labeled),
  layout_matrix = layout_matrix
)
dev.off()

