library(raster)
library(ncdf4) # 'ncdf4' package must be downloaded and installed manually
library(data.table)
library(reshape2)

data_dir_root = "/Users/peteralexander/Documents/LURG/Model Comp/Phase1/LUC4C MIP/"
		
covertMagpieNetCDFs <- function() {
	#define lc types as in netCDF files
	lc_types <- c('Land Cover|Cropland','Land Cover|Pasture','Land Cover|Forest','Land Cover|Other Natural Land','Land Cover|Built-up Area')
	lc_types_short <- c('c','p','f','u','b')
	magpieRootDir = file.path(data_dir_root, "MAgPIE")
	
	#find all *LandCoverAll.nc files 
	magpie_data <- list.files(magpieRootDir, pattern = '\\LandCoverAll.nc$', full.names=TRUE)
	
	#convert all the nc files found into ascii rasters held in separate directories
	for (nc_file in magpie_data) {
		scenarioName = gsub('.*/MAgPIE/(.*)_LandCoverAll.nc', '\\1', nc_file)
		processedDir <- file.path(magpieRootDir, "processed", scenarioName)
		if (!file.exists(processedDir)) dir.create(processedDir, recursive = TRUE)
		
		#loop lc types and read to list as raster stacks, then write individual layers to new ascii files (one ascii per time step and lc_type)
		for (i in 1:length(lc_types)) {
			scenarioStack <- stack(nc_file, varname=lc_types[i])
			writeRaster(scenarioStack, file.path(processedDir,lc_types_short[i]), format='ascii', bylayer=TRUE, suffix=names(scenarioStack))
		}	
	}
}

#do the MAgPIE conversion to ascii
covertMagpieNetCDFs()


#function to read ascii for a MAgPIE scenario to raster stack 
readMagpieAscii <- function(scenario) {
	magpieFractionStack <- stack(list.files(file.path(data_dir_root, "MAgPIE/processed", scenario), full.names=TRUE))
	crs(magpieFractionStack) <- '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs'

	# convert from cell fraction to areas in Mha
	areaConversion <- area(magpieFractionStack) / 10000  
	magpieAreaStack <- magpieFractionStack * areaConversion
	names(magpieAreaStack) <- names(magpieFractionStack)
	
	magpieAreaStack
}

#get global magpie areas from ascii files 
getSummaryMagpieAreas <- function(scenario) {
	magpieAreas <- readMagpieAscii(scenario) 
	magpieEuAreas = magpieAreas * eu27
	names(magpieEuAreas) = names(magpieAreas)
	
	gdt = data.table(as.data.frame(magpieAreas, na.rm=TRUE))	
	summaryAreas = melt( gdt[, lapply(.SD, sum, na.rm=TRUE)], variable.name = "measure", value.name = "value", measure.vars=names(gdt))[, location := "Global"]
	
	edt = data.table(as.data.frame(magpieEuAreas, na.rm=TRUE))	
	summaryAreas = rbind(summaryAreas, melt( edt[, lapply(.SD, sum, na.rm=TRUE)], variable.name = "measure", value.name = "value", measure.vars=names(edt))[, location := "EU27"])
	
	summaryAreas[, list(unit = "Mha", 
					year =as.numeric(gsub(".*_X([0-9]+)","\\1", measure)),
					location,
					land_type = gsub("(.*)_X[0-9]+","\\1", measure),
					value, model = "MAgPIE", scenario = scenario)
			]
}


magpieAreaSummary = NULL
for (scenario in c("AFF", "AFF+BECCS", "BAU", "BECCS")) {
	print (paste("MAgPIE:", scenario))
	magpieAreaSummary = rbind(getSummaryMagpieAreas(scenario), magpieAreaSummary)
}
write.table(magpieAreaSummary, file.path(data_dir_root, "MAgPIE/MAgPIEsummary.csv"), row.names=FALSE)


