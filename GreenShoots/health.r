
source(file.path(r_script_root, "GreenShoots", "GSdata.R"))

directory = file.path(r_script_root, "GreenShoots")

calculateAgeGroupDeaths = function(){
	
	popData = fread(file.path(directory,"PopData.csv"), header=TRUE)			
	
	popSizes=merge(cbind(popData[SeriesName %like% '% of female population', list(SeriesName,percPop=a2013)],popData[SeriesName=='Population, female',list(femaleTot=a2013)])[,list(SeriesName=gsub("(.*),.*", "\\1",SeriesName), popSizeFemale=as.numeric(percPop)/100*as.numeric(femaleTot))],
			cbind(popData[SeriesName %like% '% of male population', list(SeriesName,percPop=a2013)],popData[SeriesName=='Population, male',list(maleTot=a2013)])[,list(SeriesName=gsub("(.*),.*", "\\1",SeriesName), popSizeMale=as.numeric(percPop)/100*as.numeric(maleTot))],
			by='SeriesName')[,list(SeriesName,popSize=popSizeFemale +popSizeMale)]
	
	popSizes[, "SeriesName" := gsub("-", " to ",SeriesName)]
	popSizes[, "SeriesName" := gsub("Population ages ", "",SeriesName)]
	popSizes[, "SeriesName" := gsub("80 and above", "80 plus",SeriesName)]
	
	deathData = fread(file.path(directory,"IHME-GBD_2017_DATA-2148069e-1.csv"),header=TRUE)
#No under 20s as these diseases are not associated with youth. See Springmann supp
#death rate per 100000 persons 
#use only mean value (val) and convert from long to wide format
	deathData = data.table::dcast(deathData[,list(location,age,cause,val)], age ~ cause, value.var="val")
	setnames(deathData, gsub(' ', '', (names(deathData))))  
	
	ageDeaths = merge(popSizes[,list(age=SeriesName,popSize)], deathData, by='age')
	
	ageDeathsTab = ageDeaths[,list(age, CHD_deaths = popSize*Ischemicheartdisease/100000, CAN_deaths=popSize*Neoplasms/100000, STK_deaths=popSize*Stroke/100000, OTH_deaths=popSize*AllOtherCauses/100000)]
	
	ageDeathsTab
}

calculateDietPAF = function(scnPlantFractProteinScen, refPlantFractProtein, refServings, rrRmChd = 1.25, rrRmStk = 1.1, rrRmCan =1.01,
		rrFvChd=0.96, rrFvStk=0.95, rrFvCan=0.93, redMeat){
	
	PAF_OTH=0.0
	
	if(redMeat == TRUE){
		scnServings = refServings*(1-scnPlantFractProteinScen)/(1-refPlantFractProtein)
		PAF_CHD = 1- (rrRmChd^scnServings)/(rrRmChd^refServings)
		PAF_STK = 1- (rrRmStk^scnServings)/(rrRmStk^refServings)
		PAF_CAN = 1- (rrRmCan^scnServings)/(rrRmCan^refServings)
		
		lowMeatThreshold = refServings/4
		if (scnServings < lowMeatThreshold) { # if less than 0.5 server of red meat potentially hard to achieve nutritional requirements
			veganDeathRate = (1 + 1 +  .85)/3  - (0.84 + .93 + .91)/3
			lowMeatAdjRate = (1 - scnServings/lowMeatThreshold) * veganDeathRate # up to 10% of other deaths
			PAF_OTH = -lowMeatAdjRate
			PAF_CHD = PAF_CHD - lowMeatAdjRate
			PAF_STK = PAF_STK - lowMeatAdjRate
			PAF_CAN = PAF_CAN - lowMeatAdjRate
		}
		riskFact='redMeat'
	}
	else {
		scnServings = refServings*scnPlantFractProteinScen/refPlantFractProtein
		PAF_CHD = 1- (rrFvChd^scnServings)/(rrFvChd^refServings)
		PAF_STK = 1- (rrFvStk^scnServings)/(rrFvStk^refServings)
		PAF_CAN = 1- (rrFvCan^scnServings)/(rrFvCan^refServings)
		riskFact='fruitVeg'
	}
	data.table(riskFactor=riskFact, PAF_CHD, PAF_STK, PAF_CAN, PAF_OTH)
}

calculateWeightPAF=function(refCalorieIntake, scnCalorieIntake){
	ref = calculateWeightRR(refCalorieIntake)
	scn = calculateWeightRR(scnCalorieIntake)
	dt = merge(ref, scn, by="type", suffixes=c('.ref', '.scn'))
	dcast(dt[, list(riskFactor="weight", type, value=1-rr.scn/rr.ref)], riskFactor ~ type)
}

calculateWeightRR=function(calorieIntake, rrOvwChd=1.31, rrUnChd=0.67, rrObChd=1.78, rrOvwStk=1.07, rrUnStk=1.03, rrObStk=1.55, rrOvwCan=1.1, rrObCan=1.4, rrUnCan=1.11, rrOvwOth=0.96, rrObOth=1.33, rrUnOth=1.85){
		
	weights = nutritionEstimatesWorld[, list(meanBMI = intercept + countryCoef + (calorieIntake)*
					totalKcalpc_cf + (calorieIntake)^2*totalKcalpc2_cf + 
					(percAP)*shareanimal_cf + (percAP)^2*shareanmial2_cf)]
		
	weights[,propObes := 1-plnorm(30,meanlog=(log(meanBMI)-((sigma)^2)/2),sdlog=sigma)]
	weights[,propOver := (1-plnorm(25,meanlog=(log(meanBMI)-((sigma)^2)/2),sdlog=sigma)) - (1-plnorm(30,meanlog=(log(meanBMI)-((sigma)^2)/2),sdlog=sigma))]
	weights[,propUnder := plnorm(18.5,meanlog=(log(meanBMI)-((sigma)^2)/2),sdlog=sigma)]
	weights[,propNorm := 1-sum(propObes, propOver,propUnder)]
		
	rbind(data.table(type="PAF_CHD", rr=weights[, propOver*rrOvwChd + propObes*rrObChd + propUnder*rrUnChd + propNorm]),
			data.table(type="PAF_STK", rr=weights[, propOver*rrOvwStk + propObes*rrObStk + propUnder*rrUnStk + propNorm]),
			data.table(type="PAF_CAN", rr=weights[, propOver*rrOvwCan + propObes*rrObCan + propUnder*rrUnCan + propNorm]),
			data.table(type="PAF_OTH", rr=weights[, propOver*rrOvwOth + propObes*rrObOth + propUnder*rrUnOth + propNorm]))
}

calculateAvoidedDeaths = function(scnPlantFractProteinScen,refPlantFractProtein, refServingsMeat, refServingsFruitVeg, scnConsumpEff, refConsumpEff){
	calorieGuideline=2100
	
	dietMeatPAF=calculateDietPAF(scnPlantFractProteinScen=scnPlantFractProteinScen, refPlantFractProtein=refPlantFractProtein, refServings=refServingsMeat, redMeat=TRUE)
	dietFruitVegPAF=calculateDietPAF(scnPlantFractProteinScen=scnPlantFractProteinScen, refPlantFractProtein=refPlantFractProtein, refServings=refServingsFruitVeg, redMeat=FALSE)
	weightPAF=calculateWeightPAF(refCalorieIntake=calorieGuideline/refConsumpEff * 0.8, scnCalorieIntake=calorieGuideline/scnConsumpEff * 0.8) # 0.8 accounts for losses at retailer and consumer (prior to consumption)
	
	pafTab=data.table::dcast(melt(rbind(dietMeatPAF,dietFruitVegPAF,weightPAF), id='riskFactor'), variable ~ riskFactor)
	pafTab[,total := 1 - (1-weight)*(1-fruitVeg)*(1-redMeat)]
	
	ageGroupDeaths[,sum(pafTab[variable=='PAF_CHD', total]*CHD_deaths +  
							pafTab[variable=='PAF_STK', total]*STK_deaths + 
							pafTab[variable=='PAF_CAN', total]*CAN_deaths +
							pafTab[variable=='PAF_OTH', total]*OTH_deaths )]
	
}

fs1=merge(food_supply[Year==2013],fread(file.path(directory, "healthItems.csv")), by='Item' )[,list(food_t=sum(food_t)), by=c('Country','plumDemandItem')]
fs_cd=merge(fs1, country_data[Year==2013, list(Country,population=population)], by='Country')
fs_cd[,g_pc_daily := food_t/population/365]

fs_cd[plumDemandItem=='Ruminants',no_servings := g_pc_daily/100]
fs_cd[plumDemandItem=='FruitVeg',no_servings := g_pc_daily/80]

fs_cd[Country=='World']

#setup some required data
ageGroupDeaths =calculateAgeGroupDeaths()
sigma = fread(file.path(r_script_root, "InputData", "Health", "BMIdistributions.csv"))[Country=='World',sigma]
percAP =kcalByType[Country=='World', animalKcal/(animalKcal+plantKcal)*100]
nutritionEstimates=fread(file.path(plum_data_dir, "nutritionestimates.csv"))
nutritionEstimatesWorld=nutritionEstimates[, lapply(.SD, function(x) mean(x,na.rm=TRUE)), 
		.SDcols=c('intercept', 'totalKcalpc_cf', 'totalKcalpc2_cf', 'shareanimal_cf', 'shareanmial2_cf', 'countryCoef')]

tab=NULL

for(fract in seq(0.30,1.0,0.1)){
#for(fract in seq(0.3, 1.0, 0.035)){
	print (paste0("Doing fract=", fract))
#	for(eff in seq(0.4,1.10,0.005)){
	for(eff in seq(0.4,1.0,0.15)){
		avDeaths=calculateAvoidedDeaths(scnPlantFractProteinScen=fract, refPlantFractProtein=adj$worldFract,
				refServingsMeat=fs_cd[Country=='World' & plumDemandItem=='Ruminants',no_servings], 
				refServingsFruitVeg=fs_cd[Country=='World' & plumDemandItem=='FruitVeg',no_servings], 
				scnConsumpEff=eff, refConsumpEff=adj$worldEff)

		newRow=data.table(fract,eff,avDeaths)
		tab=rbind(tab,newRow)
	}
}


minAvDeaths=min(tab$avDeaths)
maxAvDeaths=max(tab$avDeaths)

# scale so that to greenest is up to 1% of min deaths and 0 (white) 25%
#endOfGreenest=0.01
#midOfWhite=0.20
#A = 1/(midOfWhite-endOfGreenest)
#B = (1-endOfGreenest)*A - 1
#sumHealth = tab[, list(eff, fract, diff=(A*(avDeaths-minAvDeaths)/(maxAvDeaths-minAvDeaths)-B)*100)]


neutralDeathsRate = maxAvDeaths / 2
sumHealth = tab[, list(eff, fract, diff=(avDeaths-neutralDeathsRate)/(maxAvDeaths-neutralDeathsRate)*100)]

write.table(data.table::dcast(sumHealth, fract ~ eff, value.var="diff")[order(fract)], file.path(gsPaperDir, "health.csv"), row.names=FALSE, sep=",")


plotConsumptionSurface(sumHealth[, list(eff, fract, changeRate=diff/100)], 
		"Avoided deaths from dietary change", "heath-global.pdf")