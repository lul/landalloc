
calcLUforCountryDiet = function(cp, aCountry, prodYear, popYear, dietYear, aProdCountry = "World", scaleToWorld=TRUE) {
	aDiet = cp[Country == aCountry & Year == dietYear, list (Item, quantity_person_year = food/population)]
	cbind(Country=aCountry, Year=dietYear, calcLUforDiet(cp, aDiet, prodYear, popYear, aProdCountry, scaleToWorld))
}

calcLUforDiet = function(cp, aDiet, prodYear=2011, popYear=2011, aProdCountry="World", scaleToWorld=TRUE) {
	avg_land_and_nutrients = cp[Country == aProdCountry & Year == prodYear, list (Item, base_energy_kcal_pc=energy_kcal_pc, base_protein_g_pc=protein_g_pc,
					base_food_area=food_area, base_animal_a_feed=animal_a_feed*food/used, base_animal_a_pasture=animal_a_pasture*food/used,
					crop_food_req=food_area/food, feed_req=animal_a_feed/used, pasture_req=animal_a_pasture/used, 
					mj_per_kg, protein_g_per_kg)]
	
	dt = merge(avg_land_and_nutrients, aDiet, by='Item')
	pop_req = if(scaleToWorld) country_data[Country == aProdCountry & Year == popYear, population] else 1
	land_area_adj = if(scaleToWorld) country_data[Country == aProdCountry & Year == prodYear, land]/100 else 1 # includes 100 for % calc
	
	dt[, energy_kcal_pc:=quantity_person_year/365*mj_per_kg*1000*239.006]
	dt[, protein_g_pc:=quantity_person_year/365*protein_g_per_kg*1000]
	dt[, c('food_area', 'animal_a_feed', 'animal_a_pasture') := 
					list(quantity_person_year*pop_req*crop_food_req, quantity_person_year*pop_req*feed_req, quantity_person_year*pop_req*pasture_req)]
	
	summary = dt[, list(dummy = 1,
					food_crop_t = sum(food_area, na.rm=TRUE),
					feed_t = sum(animal_a_feed, na.rm=TRUE),
					pasture_t = sum(animal_a_pasture, na.rm=TRUE),
					food_crop_a_base=sum(base_food_area, na.rm=TRUE),
					feed_base=sum(base_animal_a_feed, na.rm=TRUE),
					pasture_base=sum(base_animal_a_pasture, na.rm=TRUE)
			)]
	
	energy_adj = dt[, sum(base_energy_kcal_pc, na.rm=TRUE)/sum(energy_kcal_pc, na.rm=TRUE)]
	protein_adj = dt[, sum(base_protein_g_pc, na.rm=TRUE)/sum(protein_g_pc, na.rm=TRUE)]
	
	adjsProfiles = data.table(type=c('Overall', "Profile", "Profile"), scenarioType=c("Both", "energy", "protein"), nutrient_adj=c(1, energy_adj, protein_adj), dummy = 1)
	adjsQuantities = data.table(type=c("Quantity", "Quantity"), scenarioType=c("energy", "protein"), nutrient_adj=c(energy_adj, protein_adj), dummy = 1)
	
	rbind(
			merge(summary, adjsProfiles, by='dummy')[, list(prodYear=prodYear, popYear=popYear, type, scenarioType,
							crop_index=nutrient_adj*(food_crop_t+feed_t)/land_area_adj,
							pasture_index=nutrient_adj*pasture_t/land_area_adj,
							agri_index=nutrient_adj*(food_crop_t+feed_t+pasture_t)/land_area_adj,
							#			food_crop_change = food_crop_t/food_crop_a_base*nutrient_adj-1, 
							crop_food_change = (food_crop_t)/(food_crop_a_base)*nutrient_adj-1, 
							crop_change = (food_crop_t+feed_t)/(food_crop_a_base+feed_base)*nutrient_adj-1, 
							animal_change=(pasture_t+feed_t)/(pasture_base+feed_base)*nutrient_adj-1, 
							agri_change = (food_crop_t+feed_t+pasture_t)/(food_crop_a_base+feed_base+pasture_base)*nutrient_adj-1)],
			
			merge(summary, adjsQuantities, by='dummy')[, list(prodYear=prodYear, popYear=popYear, type, scenarioType,
							crop_index=1/nutrient_adj*(food_crop_a_base+feed_base)/land_area_adj, 
							pasture_index=1/nutrient_adj*pasture_base/land_area_adj, 
							agri_index=1/nutrient_adj*(food_crop_a_base+feed_base+pasture_base)/land_area_adj,
							#			food_crop_change = 1/nutrient_adj-1,
							crop_food_change = 1/nutrient_adj-1,
							crop_change = 1/nutrient_adj-1,
							animal_change= 1/nutrient_adj-1, 
							agri_change=1/nutrient_adj-1)]
	)
}

calcHalfIndexesByTime = function(cp) {
	for (yr in seq(1961, 2011)) {
		for (cntry in c("United States of America", "United Kingdom", "India", "China", "Brazil", "Nigeria", "World")) {
			res = calcLUforCountryDiet(cp, cntry, 2011, 2011, yr)
			if (exists('allHalfs')) allHalfs = rbind(allHalfs, res) else allHalfs = res
			
			if (yr != 2011) {
				res2 = calcLUforCountryDiet(cp, cntry, yr, yr, yr)
				allHalfs = rbind(allHalfs, res2)
				
				if (cntry == "World") {
					res3 = calcLUforCountryDiet(cp, cntry, 2011, yr, 2011)
					allHalfs = rbind(allHalfs, res3)
					res3 = calcLUforCountryDiet(cp, cntry, yr, 2011, 2011)
					allHalfs = rbind(allHalfs, res3)
					res3 = calcLUforCountryDiet(cp, cntry, yr, yr, 2011)
					allHalfs = rbind(allHalfs, res3)
				}
			}
		}
	}
	allHalfs
}

calcHalfIndexesByCountry = function(cp, dietYear, countries = getCountryCodes()$Country, globalProduction=TRUE) {	
	for (cntry in countries) {
		if (!cntry %in% c("Hong Kong", "Kosovo", "Macau", "Monaco", "Pacific island small states", "Czechoslovakia", "Ethiopia PDR", "Sudan (former)", 
				"Belgium-Luxembourg", "Yugoslav SFR", "Serbia and Montenegro", "USSR")) {
			if (globalProduction) {
				res = calcLUforCountryDiet(cp, cntry, 2011, 2011, dietYear)
			} else {
				res = calcLUforCountryDiet(cp, cntry, 2011, 2011, dietYear, aProdCountry=cntry, scaleToWorld=FALSE)
			}
			if (exists('allHalfs')) allHalfs = rbind(allHalfs, res) else allHalfs = res
		}
	}
	allHalfs
}

calcFootprintByTime = function(cp) {
	for (yr in seq(1961, 2011)) {
		print (yr)
		for (cntry in getCountryCodes()$Country) {
			res = calcLUforCountryDiet(cp, cntry, yr, yr, yr, aProdCountry=cntry, scaleToWorld=FALSE)
			if (exists('footprintByTime')) footprintByTime = rbind(footprintByTime, res) else footprintByTime = res
		}
	}
	footprintByTime
}

#####################
# HALF PAPER 
halfIndexes2011ByCountry = calcHalfIndexesByCountry(con_prod, 2011)
landUseFootprint2011ByCountry = calcHalfIndexesByCountry(con_prod, 2011, globalProduction=FALSE)
halfIndexes1961ByCountry = calcHalfIndexesByCountry(con_prod, 1961)
halfIndexesByTime = calcHalfIndexesByTime(con_prod)
footprintByTime = calcFootprintByTime(con_prod)

# output tables for paper
write.table(halfIndexes2011ByCountry[agri_index > 0 & type == "Overall", list(Country, crop_index, pasture_index, agri_index)], 
		"/Users/peteralexander/Documents/LURG/Alt\ Scenarios/figures/country_half.csv", row.names=FALSE, sep=",")

write.table(con_prod[Year == 2011 & Country == "World", list(mass_g_pc=food/population*1000000/365, energy_kj_pc=energy_kcal_pc/239.006*1000, protein_g_pc, prod_area=sum(animal_a_feed, animal_a_pasture, food_area, na.rm=TRUE)), by=Item][order(energy_kj_pc, decreasing = TRUE)],
		"/Users/peteralexander/Documents/LURG/Alt\ Scenarios/figures/commodities.csv", row.names=FALSE, sep=",")

write.table(landUseFootprint2011ByCountry[!is.na(agri_change) & Country != "World" & type == "Overall", list(Country, Year, prodYear, popYear, type, scenarioType, crop=crop_index, pasture=pasture_index, agri=agri_index)], 
		"/Users/peteralexander/Downloads/landUseFootprint2011ByCountry.csv", row.names=FALSE, sep=",")


#####################


#####################
# MEAT SUBSTITUTE PAPER
getMeatSubs = function(cp, meat_replacement_rate, generateRandom=FALSE) {
	
	orig_ap = cp[Country == "World" & Year == 2011 & Item %in% getFeedConvRatio()$Item, list(dummy = 1,
					energy_giga_kcal=sum(energy_kcal_pc*population)*365/1000,
					protein_Mt=sum(protein_g_pc*population)*365/1000000,
					animal_a_feed_base=sum(animal_a_feed*food/used),
					animal_a_pasture_base=sum(animal_a_pasture*food/used),
					food_crop_a_base=cp[Country == "World" & Year == 2011, sum(food_area, na.rm=TRUE)],
					land_area = country_data[Country == "World" & Year == 2011, land],
					agri_area = country_data[Country == "World" & Year == 2011, agri])]
	
	avg_feed_dm_yield = cp[Country == "World" & Year == 2011 & is.na(fcr), sum(feed*dryMatter, na.rm=TRUE)/sum(feed_area, na.rm=TRUE)]
	soy_dm_yield = cp[Country == "World" & Year == 2011 & Item == 'Soyabeans', prod/prod_area* dryMatter]

	alt_ap_data = data.table(type = c("Mealworm", "Crickets", "Cultured meat", "Tilapia", "Carp", "Tofu"), 
			fcr = c(1.8, 2.1, 4.0, 4.59, 4.86, 0.29), 
			energy_kcal_per_kg = c(8.9*239.006, 5.9*239.006, 8.3*239.006, 960, 1270, 3.18*239.006), 
			protein_g_per_kg = c(179, 205, 190, 200.8, 178.3, 80.8),
			feed_yield_tdm_per_ha=c(rep(avg_feed_dm_yield, 5), soy_dm_yield),
			yield_range=0.1,
			fcr_range_lower=c(1.6, 1.9, 2.0, 3.7, 3.9, 0.27),
			fcr_range_upper=c(2.1, 2.4, 8.0, 5.5, 5.9, 0.35),
			nutrient_range=c(0.3, 0.3, 0.1, 0.1, 0.1, 0.1),
			pasture_a_per_t=0)
			
	if (generateRandom) {
		alt_ap_data[, feed_yield_tdm_per_ha := feed_yield_tdm_per_ha * (1 + runif(1, min=-yield_range, max=yield_range))]
		meat_replacement_rate = meat_replacement_rate * (1+runif(1, min=-0.01, max=0.01))
		alt_ap_data[, fcr := runif(6, min=fcr_range_lower, max=fcr_range_upper)]
		alt_ap_data[, energy_kcal_per_kg := energy_kcal_per_kg * (1+runif(3, min=-nutrient_range, max=nutrient_range))]
		alt_ap_data[, protein_g_per_kg := protein_g_per_kg * (1+runif(3, min=-nutrient_range, max=nutrient_range))]
	}
	
	alt_ap_data[, feed_a_per_t := fcr / feed_yield_tdm_per_ha]
	
	# Add conventional livestock
	alt_ap_data = rbind (alt_ap_data, cp[Country == "World" & Year == 2011 & Item %in% c("Bovine Meat", "Eggs", "Milk - Excluding Butter", "Pigmeat", "Poultry Meat", "Mutton & Goat Meat"),
					list(	type=Item,
							feed_a_per_t=animal_a_feed/used, 
							pasture_a_per_t=animal_a_pasture/used, 
							energy_kcal_per_kg=239.006*mj_per_kg, 
							protein_g_per_kg, fcr)], fill=TRUE)
	
	alt_ap_data[, dummy := 1]
		
	meat_subs = rbind(
			merge(orig_ap, alt_ap_data, by='dummy')[, list(type, scenarioType = "energy", dummy=1, feed_a_per_t, pasture_a_per_t, energy_kcal_per_kg, protein_g_per_kg,
							food_sub=meat_replacement_rate*energy_giga_kcal/energy_kcal_per_kg,
							animal_a_feed_sub=meat_replacement_rate*energy_giga_kcal/energy_kcal_per_kg*feed_a_per_t,
							animal_a_pasture_sub=meat_replacement_rate*energy_giga_kcal/energy_kcal_per_kg*pasture_a_per_t)]
			,
			merge(orig_ap, alt_ap_data, by='dummy')[, list(type, scenarioType = "protein", dummy=1, feed_a_per_t, pasture_a_per_t, energy_kcal_per_kg, protein_g_per_kg,
							food_sub=meat_replacement_rate*protein_Mt/protein_g_per_kg*1000,
							animal_a_feed_sub=meat_replacement_rate*protein_Mt/protein_g_per_kg*feed_a_per_t*1000,
							animal_a_pasture_sub=meat_replacement_rate*protein_Mt/protein_g_per_kg*pasture_a_per_t*1000)]
	)
	
	sub_result = merge(meat_subs, orig_ap, by='dummy')
	
	sub_result[, c('animal_a_feed_scenario', 'animal_a_pasture_scenario') := 
					list(animal_a_feed_base*(1-meat_replacement_rate)+animal_a_feed_sub, 
							animal_a_pasture_base*(1-meat_replacement_rate)+animal_a_pasture_sub)]
	
	sub_result2 = sub_result[, list(type, scenarioType, 
					feed_m2_per_mj=feed_a_per_t/energy_kcal_per_kg*239.006/1000*100*100, pasture_m2_per_mj=pasture_a_per_t/energy_kcal_per_kg*239.006/1000*100*100,
					feed_m2_per_protein_g=feed_a_per_t/protein_g_per_kg*100*100/1000, pasture_m2_per_protein_g=pasture_a_per_t/protein_g_per_kg*100*100/1000,
					crop_index=(food_crop_a_base+animal_a_feed_scenario)/land_area*100,
					pasture_index=animal_a_pasture_scenario/land_area*100, 
					agri_index=(food_crop_a_base+animal_a_feed_scenario+animal_a_pasture_scenario)/land_area*100,
					crop_change=(animal_a_feed_scenario+food_crop_a_base)/(animal_a_feed_base+food_crop_a_base)-1,
					animal_change=(animal_a_pasture_scenario+animal_a_feed_scenario)/(animal_a_pasture_base+animal_a_feed_base)-1, 
					agri_change=(animal_a_pasture_scenario+animal_a_feed_scenario-animal_a_pasture_base-animal_a_feed_base)/agri_area)]
	
	max_sub_res = merge(sub_result2, sub_result2[, list(agri_change=max(agri_change)), by=type], by=c('type', 'agri_change'))
	
	halfIndexesSample = calcHalfIndexesByCountry(cp, 2011, c('World', 'United States of America', 'India'))
	
	diet_res = halfIndexesSample[Country %in% c('United States of America', 'India')]
	max_diet_res = merge(diet_res, diet_res[, list(agri_change=max(agri_change)), by=list(type, Country)], by=c('type', 'Country', 'agri_change'))
	
	current_oversupply = cp[Country == "World" & Year == 2011, list(energy=sum(energy_kcal_pc, na.rm=TRUE)/239.006/9.8, protein=sum(protein_g_pc, na.rm=TRUE)/52)]	
	target_oversupply = 1 + (current_oversupply$energy-1)*.5
	if (generateRandom) target_oversupply =  1 + (current_oversupply$energy-1)*runif(1)
	
	baseDiet = cp[Country == "World" & Year == 2011, list(Item, quantity_person_year=food/population)]
	energy_waste = calcLUforDiet(cp, baseDiet[, list(Item, quantity_person_year=quantity_person_year*target_oversupply/current_oversupply$energy)])
#	protein_waste = calcLUforDiet(cp, baseDiet[, list(Item, quantity_person_year=quantity_person_year*target_oversupply/current_oversupply$protein)])
	# taking energy as more constraining than protein
	waste = energy_waste[type=="Overall", list(des="Waste", scenarioType, agri_change, crop_change, animal_change, agri_index, crop_index, pasture_index)]
	
	rbind(	waste, 
			max_diet_res[, list(des=paste(Country, type), scenarioType, agri_change, crop_change, animal_change, agri_index, crop_index, pasture_index)],
			max_sub_res[, list(des=type, scenarioType, energy_yield=1/(feed_m2_per_mj+pasture_m2_per_mj), protein_yield=1/(feed_m2_per_protein_g+pasture_m2_per_protein_g), 
							agri_change, crop_change, animal_change, agri_index, crop_index, pasture_index)],
			halfIndexesSample[Country == "World" & type == "Overall" & Year == 2011, list(des="Baseline 2011", scenarioType, agri_change, crop_change, animal_change, agri_index, crop_index, pasture_index)],
			fill=TRUE)	
}

calcMonteCarloSubs = function(meat_replacement_rate, num_iter=500) {
	monte_carlo_res = NULL
	
	for (i in 1:num_iter) {
		cp = calcAnimalStuff(con_prod_checkpoint, getFeedConvRatio(TRUE))
		
		cp = merge(cp, itemDetails[, list(Item, randNum=runif(91, min=-0.1, max=0.1))], by='Item')
		cp[, mj_per_kg := mj_per_kg * (1+randNum)]
		cp[, energy_kcal_pc := energy_kcal_pc * (1+randNum)]
		cp[, randNum:=NULL]	
		
		cp = merge(cp, itemDetails[, list(Item, randNum=runif(91, min=-0.1, max=0.1))], by='Item')
		cp[, protein_g_per_kg := protein_g_per_kg * (1+randNum)]
		cp[, protein_g_pc := protein_g_pc * (1+randNum)]
		cp[, randNum:=NULL]
		
		monte_carlo_res = rbind(monte_carlo_res, cbind(sim=i, getMeatSubs(cp, meat_replacement_rate=meat_replacement_rate, generateRandom=TRUE)))
	}
	
	cp = calcAnimalStuff(con_prod_checkpoint, getFeedConvRatio())
	base_res = getMeatSubs(cp, meat_replacement_rate=meat_replacement_rate, generateRandom=FALSE)
	merge(base_res, monte_carlo_res[, list(min_agri_index=min(agri_index), max_agri_index=max(agri_index), 
							min_energy_yield=min(energy_yield), max_energy_yield=max(energy_yield),
							min_protein_yield=min(protein_yield), max_protein_yield=max(protein_yield)),
					by=list(des)], by=c('des'))
}

meatSubs = calcMonteCarloSubs(meat_replacement_rate=0.5)

