
dt = copy(con_prod)
# factor of 10 is due to ha to m2 
dt[is.na(fcr), m2_per_protein_g:= 10 * net_area/used/protein_g_per_kg]
dt[!is.na(fcr), m2_per_protein_g:= 10 * (animal_a_feed+animal_a_pasture)/used/protein_g_per_kg]
dt[is.na(fcr), m2_per_mj:= 10 * net_area/used/mj_per_kg]
dt[!is.na(fcr), m2_per_mj:= 10 * (animal_a_feed+animal_a_pasture)/used/mj_per_kg]

tableToOutput = dt[used!=0, list(Item,Year,Country,mj_per_kg,protein_g_per_kg,dryMatter,m2_per_protein_g,m2_per_mj)]

dairyItems = data.table(outputItem = c('Milk Skimmed', 'Cream', 'Butter Ghee', 'Cheese', 'Whey', 'Infant food', 'Milk Whole'), 
		milkWeightRate = c(          0.634,             3.71,      7.43,         6.17,     0.03,    21.6,          1.0),
		mj_per_kg__n =    c(          1.42,              7.99,      30.0,         15.5,     1.13,    21.3,          2.405892),
		protein_g_per_kg__n =c(       33.7,              29.6,       8.5,        232.4,      8.5,    113,          34.12361))

# cartesian join
additionalDairy = dairyItems[,as.list(dt[Item == "Milk - Excluding Butter"]), by=list(outputItem, milkWeightRate, mj_per_kg__n, protein_g_per_kg__n)]
additionalDairy = additionalDairy[, list(Item=outputItem, Year, Country, mj_per_kg=mj_per_kg__n, protein_g_per_kg=protein_g_per_kg__n, dryMatter=NA, 
				m2_per_protein_g=m2_per_protein_g*milkWeightRate*protein_g_per_kg/protein_g_per_kg__n, m2_per_mj=m2_per_mj*milkWeightRate*mj_per_kg/mj_per_kg__n)]

tableToOutput = rbind(tableToOutput, additionalDairy)

write.table(tableToOutput,
		"/Users/peteralexander/Downloads/consumption_and_landuse.csv", row.names=FALSE, sep=",")
