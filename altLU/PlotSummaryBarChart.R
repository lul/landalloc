require(data.table)
require(ggplot2)
require(reshape2)
require(grid)


diet_txt= "Alternative diet and  \nwaste scenarios"

scenarioTitles = data.table(des = c('India Overall', 'United States of America Overall', 'India Profile', 'United States of America Profile', 'India Quantity', 'United States of America Quantity',
				'Crickets', 'Cultured meat', 'Mealworm', 'Mutton & Goat Meat', 'Bovine Meat', "Eggs", "Milk - Excluding Butter", "Pigmeat", "Poultry Meat", "Tofu", "Tilapia", "Carp", "Waste", "Baseline 2011"),
		ItemName = c('Indian\ndiet:\noverall', 'USA\ndiet:\noverall', 'Indian\ndiet:\nprofile', 'USA\ndiet:\nprofile', 'Indian\ndiet:\nquantity', 'USA\ndiet:\nquantity', 
				'Adult\ncrickets', 'Cultured\nmeat', 'Mealworm\nlarvae', 'Mutton &\ngoat meat', 'Beef', 'Eggs', 'Milk &\nproducts', 'Pork', 'Poultry\nmeat', 'Soy\nbean\ncurd', "Tilapia", "Carp", 'Consumer\nwaste', '2011\nbaseline'),
		halfTypes=c(rep(TRUE, 6), rep(FALSE, 13), TRUE),
		subsTypes=c(rep(FALSE, 6), rep(TRUE, 12), FALSE, FALSE),
		allTypes=c(rep(TRUE, 19), FALSE),
		scenarioType2=c(rep(diet_txt, 6), rep("Conventional animal product\nreplacement scenarios", 12), rep(diet_txt, 2)))



plotYieldBar = function(dtToPlot, yLabel, fill_colour="grey") {	
	ggplot() + 
			geom_bar(data = dtToPlot, aes(x=ItemName, y=value), stat="identity", fill=fill_colour) +
			labs(x="", y=yLabel) +
			theme_minimal() + 
			theme(text = element_text(size=11),
					legend.position="none",
					axis.text.x = element_text(size=10),
					panel.grid.minor.y = element_blank(), 
					panel.grid.major.x = element_blank(), 
					panel.grid.minor.x = element_blank(), 
					panel.background = element_rect(colour = "black", size =.4)) +
			geom_errorbar(data=dtToPlot, aes(x=ItemName, ymin=minVal, ymax=maxVal), width=.3, size=0.25)
	
}

dt = merge(scenarioTitles, meatSubs[protein_yield > 0], by = c('des'))

dt2 = dt[, list(ItemName, value=protein_yield, minVal=min_protein_yield, maxVal=max_protein_yield)]
dt2[, ItemName:=factor(ItemName, levels=dt2[order(-value)]$ItemName)]
ggp_protein = plotYieldBar(dt2, expression(paste("Protein yield (g protein/", m^2, "/yr)")), "#377eb8")

dt2 = dt[, list(ItemName, value=energy_yield, minVal=min_energy_yield, maxVal=max_energy_yield)]
dt2[, ItemName:=factor(ItemName, levels=dt2[order(-value)]$ItemName)]
ggp_energy = plotYieldBar(dt2, expression(paste("Calorific yield (MJ/", m^2, "/yr)")), "grey40")

pdf(width=11, height=6.5, file = "/Users/peteralexander/Documents/LURG/Alt\ Scenarios/figures/meat_sub_yields.pdf")
vp_t <- viewport(just=c("left", "top"), x=0., y=0.99, width=1, height=0.52)
vp_b <- viewport(just=c("left", "top"), x=0, y=0.5, width=1, height=0.52)

print(ggp_energy, vp=vp_t)
print(ggp_protein, vp=vp_b)
grid.text(gp=gpar(cex=1, font=4), x=unit(0.02, "npc"), y=unit(.99, "npc"), just=c("left", "top"), "a)")
grid.text(gp=gpar(cex=1, font=4), x=unit(0.02, "npc"), y=unit(.50, "npc"), just=c("left", "top"), "b)")
dev.off()






plotSummaryBarChart = function(combined_res, paper, fileName, saveToFile=TRUE, includeErrorBars=FALSE, w=12, h=6.5) {	
	
	#scenarioTitles[des %in% c('Mutton & Goat Meat', 'Bovine Meat'), Paper := "just-all"]
	
	dt = merge(scenarioTitles[(paper == "half" & halfTypes == TRUE) | (paper == "subs" & subsTypes == TRUE) | (paper == "all" & allTypes == TRUE)], combined_res, by = c('des'))
	dt[, ItemName:=factor(ItemName, levels=dt[order(agri_index)]$ItemName)]
	
	histPercentage = combined_res[des == "Baseline 2011"]
	
	gg1 = ggplot() + 
			geom_bar(data = melt( dt[, list(ItemName, crop_index, pasture_index, scenarioType2)], measure.vars = c('crop_index','pasture_index')),
					aes(x=ItemName, y=value, fill=variable, colour=scenarioType2), stat="identity") +
			geom_line(data = data.table(y=c(histPercentage$crop_index, histPercentage$crop_index), x=c(0.5, length(unique(dt$ItemName))+0.5)), 
					aes(x=x, y=y, linetype="Cropland area\nin 2011 ")) +
			geom_line(data = data.table(y=c(histPercentage$agri_index, histPercentage$agri_index), x=c(0.5, length(unique(dt$ItemName))+0.5)), 
					aes(x=x, y=y, linetype="Cropland and pasture  \narea in 2011 ")) +
			scale_linetype_manual(values = c("dashed", "dotted")) + 
			labs(x="", y="Human appropriation of land for food (HALF) index") +
			scale_fill_discrete(labels = c("Cropland  \narea  ", "Pasture \narea  ")) +
			scale_colour_manual(values = c("grey", "black"), guide = FALSE) + 
			theme_minimal() + 
			theme(text = element_text(size=11),
					legend.position="bottom",
					legend.direction = "horizontal", 
					legend.box = "horizontal",
					legend.title=element_blank(),
					legend.key.width=unit(2,"line"),
					axis.text.x = element_text(size=10),
					panel.grid.minor.y = element_blank(), 
					panel.grid.major.x = element_blank(), 
					panel.grid.minor.x = element_blank(), 
					panel.background = element_rect(colour = "black", size =.4)) +
			scale_y_continuous(breaks = seq(0, 300, 20)) 
		
	if (includeErrorBars) gg1 = gg1 + geom_errorbar(data=dt, aes(x=ItemName, ymin=min_agri_index, ymax=max_agri_index), width=.3, size=0.25)
			
	if (saveToFile) pdf(width=w, height=h, file = fileName)
	vp1 = viewport()
	print(gg1, vp=vp1)
	if (saveToFile) dev.off()
}

plotSummaryBarChart(meatSubs, "half", "/Users/peteralexander/Documents/LURG/Alt\ Scenarios/figures/half_summary_bar.pdf", w=9, h=5)
plotSummaryBarChart(meatSubs, "subs", "/Users/peteralexander/Documents/LURG/Alt\ Scenarios/figures/subs_summary_bar.pdf", includeErrorBars=TRUE)
plotSummaryBarChart(meatSubs, "all", "/Users/peteralexander/Documents/LURG/Alt\ Scenarios/figures/all_summary_bar.pdf", includeErrorBars=TRUE)
